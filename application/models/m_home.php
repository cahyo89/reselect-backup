<?php
class M_home extends CI_Model{

		public function __construct() {
	
			parent::__construct();
		}
		
		function coba() {
			return $_SESSION['ini'];
		}
		
		function getNilai($no,$id) {
			$this->db->select('*')->from('penilaian');
			$this->db->where('id_kab_kota', $id); 
			$this->db->where('no_notadinas', $no); 
			$this->db->order_by('total_score', 'desc'); 
			return $this->db->get();
		}
		
		function getFlagNilai($nip,$no,$id) {
			$this->db->select('flag_finish');
			$this->db->where('nip_calon', $nip); 
			$this->db->where('id_kab_kota', $id); 
			$this->db->where('no_notadinas', $no); 
			$q = $this->db->get('penilaian')->result_array();
			if(!isset($q[0]['flag_finish'])){
			$p=0;
			}else{
			$p=$q[0]['flag_finish'];
			}
			return $p;
		}
		function getFlagNilai3($no,$id) {
			$this->db->select('flag_finish');
			//$this->db->where('nip_calon', $nip); 
			$this->db->where('id_kab_kota', $id); 
			$this->db->where('no_notadinas', $no); 
			$q = $this->db->get('penilaian')->result_array();
			if(!isset($q[0]['flag_finish'])){
			$p=0;
			}else{
			$p=$q[0]['flag_finish'];
			}
			return $p;
		}
		function getFlagNilai2($no,$id) {
			$this->db->select('flag_finish')->from('penilaian');
			$this->db->where('id_kab_kota', $id); 
			$this->db->where('no_notadinas', $no); 
			$q = $this->db->get('penilaian')->result_array();
			return $this->db->get();
		}
		
		function getNilaiCalon($nip,$no,$id) {
			$this->db->select('*')->from('penilaian');
			$this->db->where('nip_calon', $nip); 
			$this->db->where('id_kab_kota', $id); 
			$this->db->where('no_notadinas', $no); 
			return $this->db->get();
		}
		
		function getCalonDetil($nip,$no,$id) {
			$this->db->select('*')->from('calon');
			$this->db->where('nip', $nip); 
			$this->db->where('id_kab_kota', $id); 
			$this->db->where('no_notadinas', $no); 
			return $this->db->get();
		}
		
		function update_calon($nip,$array_data,$id_kab_kota,$no_notadinas) {
			$this->db->where('nip', $nip);
			$this->db->where('id_kab_kota', $id_kab_kota);
			$this->db->where('no_notadinas', $no_notadinas);
			$this->db->limit(1);
			$this->db->update('calon', $array_data);
			return true;
		}
		
		function getAllKota() {
			$this->db->select('*')->from('kab_kota');
			$this->db->order_by('nm_kab_kota', 'asc'); 
			return $this->db->get();
		}
		
		function cekJmlCalon($nip,$nosurat) {
			$this->db->select('*')->from('calon');
			$this->db->where('nip', $nip); 
			$this->db->where('no_notadinas', $nosurat); 
			return $this->db->get();
		}
		
		function getCalonSkorTinggi($nosurat,$kota) {
			$p= $this->db->query('select * from (select  nip_calon, total_score from penilaian where id_kab_kota = '.$kota.' and no_notadinas ="'.$nosurat.'" order by total_score desc)b  where @rownum:=1 LIMIT 1');
			$data=$p->result_array();
			
			if(!isset($data[0]['nip_calon'])){
			$q=0;
			}else{
			$q=$data[0]['nip_calon'];
			}
			return $q;
		}
		
		function cekJmlCalon2($nosurat,$kota) {
			$this->db->select('*')->from('calon');
			$this->db->where('id_kab_kota', $kota); 
			$this->db->where('no_notadinas', $nosurat); 
			return $this->db->get();
		}
		function cekJmlCalon3($nosurat) {
			$this->db->select('*')->from('calon');
			$this->db->where('no_notadinas', $nosurat); 
			return $this->db->get();
		}

		function cekJmlNilai($nosurat,$kota) {
			$this->db->select('*')->from('penilaian');
			$this->db->where('id_kab_kota', $kota); 
			$this->db->where('no_notadinas', $nosurat); 
			return $this->db->get();
		}
		function cekJmlNilai2($nosurat) {
			$this->db->select('*')->from('penilaian');
			$this->db->where('no_notadinas', $nosurat); 
			return $this->db->get();
		}
		
		function delete_penilaian($nip,$no_notadinas,$id_kab_kota) {
			$this->db->where('nip_calon', $nip);
			$this->db->where('no_notadinas', $no_notadinas);
			$this->db->where('id_kab_kota', $id_kab_kota);
			$this->db->delete('penilaian');
			return true;
		}
		
		function getAllProv() {
			$this->db->select('*')->from('provinsi');
			$this->db->order_by('nm_provinsi', 'asc'); 
			return $this->db->get();
		}
		
		function getJmlAlasan($surat,$kota) {
			$this->db->select('*')->from('pengusulan');
			$this->db->where('no_surat',$surat);
			$this->db->where('id_kab_kota',$kota);
			return $this->db->get();
		}
		
		function getKabDetail($surat) {
			$this->db->select('a.nm_kab_kota')->from('daerah_undangan as b');
			$this->db->where('no_notadinas',$surat);
			$this->db->join('kab_kota as a', 'b.id_kab_kota=a.id_kab_kota');
			$this->db->order_by('a.nm_kab_kota', 'asc'); 
			return $this->db->get();
		}
		
		function getProvDetail($surat) {
			$this->db->select('c.nm_provinsi')->from('daerah_undangan as b');
			$this->db->where('b.no_notadinas',$surat);
			$this->db->join('kab_kota as a', 'b.id_kab_kota=a.id_kab_kota');
			$this->db->join('provinsi as c', 'a.id_provinsi=c.id_provinsi');
			$this->db->order_by('c.nm_provinsi', 'asc'); 
			return $this->db->get();
		}
		
		function getAllUndangan() {
			$this->db->select('a.id_notadinas,a.tgl_nota,a.tgl_rapat,a.jam_rapat,a.no_notadinas')->from('nd_und_sekda as a');
			//$this->db->join('provinsi as b', 'a.id_provinsi = b.id_provinsi');
			$this->db->order_by('id_notadinas', 'asc'); 
			return $this->db->get();
		}
		
		
		function getNamaProv($idkota) {
			$this->db->select('b.nm_provinsi,b.ibukota_prov')->from('kab_kota as a');
			$this->db->where('a.id_kab_kota', $idkota);
			$this->db->join('provinsi as b', 'a.id_provinsi=b.id_provinsi');
			return $this->db->get();
		}
		
		function cekSkor($nip) {
			$this->db->select('total_score');
			$this->db->where('nip_calon', $nip);
			$q = $this->db->get('penilaian');
			$data = $q->result_array();
			if(!isset($data[0]['total_score'])){
			$p=0;
			}else{
			$p=$data[0]['total_score'];
			}
			return $p;
			//echo($data[0]['age']);
		}
		
				
		function getNamaKota($id) {
			$this->db->select('nm_kab_kota');
			$this->db->where('id_kab_kota', $id);
			$q = $this->db->get('kab_kota')->result_array();
			return $q[0]['nm_kab_kota'];
		}
		
		function getSekor($no,$id,$nip) {
			$this->db->select('total_score');
			$this->db->where('id_kab_kota', $id); 
			$this->db->where('no_notadinas', $no); 
			$this->db->where('nip_calon', $nip); 
			$data= $this->db->get('penilaian')->result_array();
			if(!isset($data[0]['total_score'])){
			$p=0;
			}else{
			$p=$data[0]['total_score'];
			}
			return $p;
		}
		
		function getNama($id) {
			$this->db->select('nama');
			$this->db->where('nip', $id);
			$data = $this->db->get('calon')->result_array();
			if(!isset($data[0]['nama'])){
			$p="";
			}else{
			$p=$data[0]['nama'];
			}
			return $p;
		}
		
		function getPenjabat($jabatan) {
			$this->db->select('nama_petugas');
			$this->db->where('jabatan', $jabatan);
			$q = $this->db->get('petugas')->result_array();
			return $q[0]['nama_petugas'];
		}
		
		function getPenjabat2($id) {
			$this->db->select('nama_petugas');
			$this->db->where('id_petugas', $id);
			$q = $this->db->get('petugas')->result_array();
			return $q[0]['nama_petugas'];
		}
		function getPenjabat3($id) {
			$this->db->select('jabatan');
			$this->db->where('id_petugas', $id);
			$q = $this->db->get('petugas')->result_array();
			return $q[0]['jabatan'];
		}
		function getPenjabat4($id) {
			$this->db->select('title');
			$this->db->where('id_petugas', $id);
			$q = $this->db->get('petugas')->result_array();
			return $q[0]['title'];
		}
		
		function getUndangan($id) {
			$this->db->select('*');
			$this->db->where('id_notadinas', $id);
			return $this->db->get('nd_und_sekda');
			
		}
		
		function getDetilDaerahUndangan($id) {
			$this->db->select('no_notadinas,id_kab_kota')->from('daerah_undangan');
			$this->db->where('id_detail_daerah',$id);
			return $this->db->get();
		}
		
				
		function getDetailListSekda($no,$id) {
			$this->db->select('a.id_calon,a.nip,a.nama,a.no_notadinas,a.id_kab_kota,a.tgl_lahir,a.pangkat,a.tmt,a.pend_umum,a.pend_struktural,a.jabatan,b.nm_kab_kota')->from('calon as a');
			$this->db->where('a.no_notadinas', $no);
			$this->db->where('a.id_kab_kota', $id);
			$this->db->join('kab_kota as b', 'a.id_kab_kota = b.id_kab_kota');
			$this->db->order_by('a.id_calon', 'asc'); 
			return $this->db->get();
		}
		
		function getDetailListSekda2($nip) {
			$this->db->select('a.nip,a.nama,a.no_notadinas,a.id_kab_kota,a.tgl_lahir,a.pangkat,a.tmt,a.pend_umum,a.pend_struktural,a.jabatan,b.nm_kab_kota')->from('calon as a');
			$this->db->where('a.nip', $nip);
			$this->db->join('kab_kota as b', 'a.id_kab_kota = b.id_kab_kota');
			$this->db->order_by('a.id_calon', 'asc'); 
			return $this->db->get();
		}
		
		function getAllDapil() {
			$this->db->select('a.id_detail_daerah,a.no_notadinas,a.id_kab_kota,b.nm_kab_kota,c.id_provinsi,c.nm_provinsi,d.tgl_rapat ')->from('daerah_undangan as a');
			$this->db->join('kab_kota as b', 'a.id_kab_kota = b.id_kab_kota');
			$this->db->join('provinsi as c', 'c.id_provinsi = b.id_provinsi');
			$this->db->join('nd_und_sekda as d', 'a.no_notadinas = d.no_notadinas');
			$this->db->order_by('d.tgl_rapat ', 'asc'); 
			return $this->db->get();
		}
		
		function getAllDapil2($no) {
			$this->db->select('a.id_detail_daerah,a.no_notadinas,a.id_kab_kota,b.nm_kab_kota,c.id_provinsi,c.nm_provinsi,d.tgl_rapat ')->from('daerah_undangan as a');
			$this->db->where('a.no_notadinas', $no);
			$this->db->join('kab_kota as b', 'a.id_kab_kota = b.id_kab_kota');
			$this->db->join('provinsi as c', 'c.id_provinsi = b.id_provinsi');
			$this->db->join('nd_und_sekda as d', 'a.no_notadinas = d.no_notadinas');
			$this->db->order_by('d.tgl_rapat ', 'asc'); 
			return $this->db->get();
		}
		
		
		function getNosurat($id_daerah) {
			$this->db->select('no_notadinas,id_kab_kota')->from('daerah_undangan');
			$this->db->where('id_detail_daerah',$id_daerah);
			return $this->db->get();
		}
		
		function getKarir($nip) {
			$this->db->select('*')->from('karir');
			$this->db->where('nip_calon',$nip);
			$this->db->order_by('id_karir ', 'asc'); 
			return $this->db->get();
		}
		
		function getSuratGub($no,$id) {
			$this->db->select('no_surat_gub,tgl_surat_gub,alasan,perihal')->from('pengusulan');
			$this->db->where('id_kab_kota',$id);
			$this->db->where('no_surat',$no);
			return $this->db->get();
		}
		
		function getIdDapil($id_daerah) {
			$this->db->select('a.id_detail_daerah,a.no_notadinas,a.id_kab_kota,b.nm_kab_kota,c.id_provinsi,c.nm_provinsi,d.tgl_rapat ')->from('daerah_undangan as a');
			$this->db->where('a.id_detail_daerah',$id_daerah);
			$this->db->join('kab_kota as b', 'a.id_kab_kota = b.id_kab_kota');
			$this->db->join('provinsi as c', 'c.id_provinsi = b.id_provinsi');
			$this->db->join('nd_und_sekda as d', 'a.no_notadinas = d.no_notadinas');
			$this->db->order_by('d.tgl_rapat ', 'asc'); 
			return $this->db->get();
		}
		
		function getAllPetugas() {
			$this->db->select('*')->from('petugas');
			return $this->db->get();
		}
		
		function update_penjabat($array_data, $id) {
			$this->db->where('id_petugas', $id);
			$this->db->limit(1);
			$this->db->update('petugas', $array_data);
			return true;
		}
		
		 function cek_user_login($username, $password){
        $query = $this
            ->db
            ->where('email', $username) // kolom username
            ->where('pass', $password) // kolom password
            ->limit(1) // pembatasan jumlah select
            ->get('users'); //table name
 
        if ($query->num_rows() == 1) { // jika data = 1
                return $query->row_array(); // return data dan nilai TRUE
        }
        else
                {
                        return FALSE; // else mengembalikan FALSE
                }
        }
		
		public function add_undangan($array_data) {
			$opo=$this->db->insert('nd_und_sekda', $array_data);
			if ($opo){
				return true;
			}else{return false;}
		
		}
		
		public function add_detil_undangan($array_data) {
			$opo=$this->db->insert('daerah_undangan', $array_data);
			if ($opo){
				return true;
			}else{return false;}
		
		}
		
		public function add_alasan_usulan($array_data) {
			$opo=$this->db->insert('pengusulan', $array_data);
			if ($opo){
				return true;
			}else{return false;}
		
		}
		
		public function add_calon_sekda($array_data) {
			$opo=$this->db->insert('calon', $array_data);
			if ($opo){
				return true;
			}else{return false;}
		
		}
		
		public function add_karir_calon($array_data) {
			$opo=$this->db->insert('karir', $array_data);
			if ($opo){
				return true;
			}else{return false;}
		
		}
		
		public function add_penilaian($array_data) {
			$opo=$this->db->insert('penilaian', $array_data);
			if ($opo){
				return true;
			}else{return false;}
		}
		
		function update_penilaian($nip,$array_data,$id_kab_kota,$no_notadinas) {
			$this->db->where('nip_calon', $nip);
			$this->db->where('id_kab_kota', $id_kab_kota);
			$this->db->where('no_notadinas', $no_notadinas);
			$this->db->limit(1);
			$this->db->update('penilaian', $array_data);
			return true;
		}
		
		function getDetailUser($id) {
			$this->db->select('*')->from('users');
			$this->db->where('id_user',$id);
			return $this->db->get();
		}
		
		function update_akun($array_data, $id) {
			$this->db->where('id_user', $id);
			$this->db->limit(1);
			$this->db->update('users', $array_data);
			return true;
		}
		
		function update_provnya($array_data, $id) {
			$this->db->where('id_provinsi', $id);
			$this->db->limit(1);
			$this->db->update('provinsi', $array_data);
			return true;
		}
		
		public function add_provnya($array_data) {
			$opo=$this->db->insert('provinsi', $array_data);
			if ($opo){
				return true;
			}else{return false;}
		
		}
		
		function delete_provnya($id) {
			$this->db->where('id_provinsi', $id);
			$this->db->delete('provinsi');
			return true;
		}
		
		function getListKota() {
			$this->db->select('a.id_kab_kota,a.nm_kab_kota,a.id_provinsi,b.nm_provinsi, ')->from('kab_kota as a');
			$this->db->join('provinsi as b', 'a.id_provinsi = b.id_provinsi');
			$this->db->order_by('b.nm_provinsi ', 'asc'); 
			return $this->db->get();
		}
		
		function cekKabKota($nm) {
			$this->db->select('*')->from('kab_kota');
			$this->db->like('nm_kab_kota',$nm);
			return $this->db->get();
		}
		
		public function add_kab_kotanya($array_data) {
			$opo=$this->db->insert('kab_kota', $array_data);
			if ($opo){
				return true;
			}else{return false;}
		
		}
		
		function delete_kab_kotanya($id) {
			$this->db->where('id_kab_kota', $id);
			$this->db->delete('kab_kota');
			return true;
		}
		function delete_kab_kotanya2($id) {
			$this->db->where('id_provinsi', $id);
			$this->db->delete('kab_kota');
			return true;
		}
		
		function update_kab_kotanya($array_data, $id) {
			$this->db->where('id_kab_kota', $id);
			$this->db->limit(1);
			$this->db->update('kab_kota', $array_data);
			return true;
		}
		
		function delete_calonnya($id) {
			$this->db->where('id_calon', $id);
			$this->db->delete('calon');
			return true;
		}
		function delete_karirnya($id) {
			$this->db->where('nip_calon', $id);
			$this->db->delete('karir');
			return true;
		}
		
		public function add_log($array_data) {
			$opo=$this->db->insert('log', $array_data);
			if ($opo){
				return true;
			}else{return false;}
		
		}
		function getAllLog() {
			$this->db->select('*')->from('log');
			$this->db->order_by('tanggal', 'asc'); 
			return $this->db->get();
		}
}
?>