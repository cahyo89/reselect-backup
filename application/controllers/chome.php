<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Chome extends CI_Controller {

	 public function __construct()
	{
		session_start();
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('m_home');	
	}
	
	public function index()
	{
		$this->load->view('vlogin');
	}
	
	
	function isLogin(){
		if(!isset($_SESSION['username'])){
			redirect('chome/login');
		}
	}
	
	public function home()
	{
		$this->isLogin(); 
		$data['opo']=$this->m_home->getAllUndangan()->result();
		$this->load->view('template/headc');
		$this->load->view('vdashboard',$data);
		$this->load->view('template/footc');
		
	}
	
	
	public function login()
	{
		if ( isset($_SESSION['username']) ) { //cek apakah session ada
                 redirect('chome/home');//redirect controller c_home
              }
			  
              $this->form_validation->set_rules('email', 'Email', 'required'); //cek, validasi username
              $this->form_validation->set_rules('password', 'Password', 'required|min_length[4]'); //cek, validasi password
              if ( $this->form_validation->run() == TRUE ) { //apabila validasi true(benar semua)
               //  $this->load->model('m_home'); // load model m_user
                 $result = $this->m_home->cek_user_login( //jalankan fungsi cek_user_login dari model m_user
                             $this->input->post('email'),  //menangkap username dari form
                            md5($this->input->post('password'))//menangkap password dari form
                          );
                  
                             
                        if ($result == TRUE) { //apabila result = true(ada data)
                               $_SESSION['username'] = $result['nama'];
                               $_SESSION['id_user'] = $result['id_user'];
								$this->session->set_userdata('username', $result['nama']);
								$this->session->set_userdata('id_user',$result['id_user']);
								//create session
								
								$log = array("tanggal" => date("Y-m-d H:i:s"),
								"message" => "Login user ".$result['nama'],
								"akun" => $result['id_user']
								);
								$this->m_home->add_log($log);
                              redirect('chome/home'); // redirect controller c_home
							//print_r($result['id_petugas']);
                        }
              }
		$this->load->view('vlogin'); //apabila session kosong load login/v_form	
	}
	

	
	public function logout(){
	$log = array("tanggal" => date("Y-m-d H:i:s"),
		  			 "message" => "Logout System ".$_SESSION['nama'],
		 			 "akun" => $_SESSION['id_user']
					);
			$this->m_home->add_log($log);
		session_destroy();
		redirect("chome/login");
	}
	
	public function petunjuk()
	{
		$this->isLogin(); 
		$this->load->view('template/head');
		$this->load->view('vpetunjuk');
		$this->load->view('template/foot');
		
	}
	public function undangansekda()
	{
		$this->isLogin(); 
		$data['kota'] = $this->m_home->getAllKota()->result();
		$data['prov'] = $this->m_home->getAllProv()->result();
		$this->load->view('template/head');
		$this->load->view('v_nd_undangansekda',$data);
		$this->load->view('template/foot');
		
	}
	public function log()
	{
		$this->isLogin(); 
		$data['log'] = $this->m_home->getAllLog()->result();
		$this->load->view('template/head');
		$this->load->view('v_log',$data);
		$this->load->view('template/foot');
		
	}
	public function listpenilaian()
	{
		$this->isLogin(); 
		$data['dapilnya'] = $this->m_home->getAllDapil()->result();
		$this->load->view('template/head');
		$this->load->view('vlist_penilaian',$data);
		$this->load->view('template/foot');
	}
	
	public function penilaian($id){
		$this->isLogin();
		foreach($this->m_home->getNosurat($id)->result() as $a){
			$nosurat=$a->no_notadinas;
			$iddaerah=$a->id_kab_kota;
		}
		$data['nosurat']=$nosurat;
		$data['iddaerah']=$iddaerah;
		$data['datanya'] = $this->m_home->getDetailListSekda($nosurat,$iddaerah)->result();
		$data2['datao']=$this->m_home->getDetailListSekda($nosurat,$iddaerah)->result();
		$this->load->view('template/head');
		$this->load->view('vlist_nilai_calon',$data);
		$this->load->view('template/foot');
		
	}
	
	public function reviewpenilaian($id){
		$this->isLogin();
		foreach($this->m_home->getNosurat($id)->result() as $a){
			$nosurat=$a->no_notadinas;
			$iddaerah=$a->id_kab_kota;
		}
		$data['datanya'] = $this->m_home->getNilai($nosurat,$iddaerah)->result();
		$data['namakota'] = $this->m_home->getNamaKota($iddaerah);
		$data['nomersurat']=$nosurat;
		$this->load->view('template/headc');
		$this->load->view('vlist_review_nilai',$data);
		$this->load->view('template/footc');
	}
	
	public function finishpenilaian($id){
		$this->isLogin();
		$p=0;
		foreach($this->m_home->getNosurat($id)->result() as $a){
			$nosurat=$a->no_notadinas;
			$iddaerah=$a->id_kab_kota;
		
		}
		$data=array("flag_finish"=>1);
		$nilainya = $this->m_home->getNilai($nosurat,$iddaerah)->result();
		foreach($nilainya as $n){
			$saved[$p]=$this->m_home->update_penilaian($n->nip_calon,$data,$iddaerah,$nosurat);
				$p++;		
		}
		if($saved[0]&&$saved[1]&&$saved[2]){
						$msg = 'Penilaian Telah Selsai dilakuan.';
						$this->session->set_flashdata('success', $msg);
					}else{
						$msg = 'Error dalam penyelesaian penilaian';
						$this->session->set_flashdata('error', $msg);
					}
		$log = array("tanggal" => date("Y-m-d H:i:s"),
								"message" => $msg,
								"akun" => $_SESSION['id_user']
								);
								$this->m_home->add_log($log);
		redirect("chome/reviewpenilaian/".$id);
	}
	
	public function inputpenilaian($id,$nip){
		$this->isLogin();
		$inputs = $this->input->post(NULL, TRUE);
		$nip=str_replace("%20", " ", $nip);
		$data['datanya']=$this->m_home->getDetilDaerahUndangan($id)->result();
		if (!$inputs) {
			$data['nama']=$this->m_home->getNama($nip);
			$data['nip']=$nip;
			$this->load->view('template/head');
			$this->load->view('v',$data);
			$this->load->view('template/foot');
		}else{
			foreach($data['datanya'] as $a){
				$no_notadinas=$a->no_notadinas;
				$id_kab_kota=$a->id_kab_kota;
			}
		 $kepangkatan=$inputs['pangkat'] * 0.05;
		 $diklat_pim=$inputs['pim'] * 0.05;
		 $pendidikan=$inputs['pendidikan'] * 0.075;
		 $riwayat_jabatan=$inputs['jabatan'] * 0.05;
		 $diklat_teknis=$inputs['teknis'] * 0.05;
		 $diklat_fungsional=$inputs['fungsional'] * 0.05;
		 $duk=$inputs['duk'] * 0.025;
		 $dp3=$inputs['dp3'] * 0.025;
		 $disiplin=$inputs['disiplin'] * 0.025;
		 $t1=($kepangkatan + $diklat_pim + $pendidikan + $riwayat_jabatan + $diklat_teknis + $diklat_fungsional + $duk + $dp3 + $disiplin);
		 $karir=$inputs['karir'] * 0.1;
		 $keragaman_diklat=$inputs['diklat'] * 0.1;
		 $seminar=$inputs['seminar'] * 0.1;
		 $pokir_strategis=$inputs['pokir'] * 0.1;
		 $t2=($karir + $keragaman_diklat + $seminar + $pokir_strategis);
		 $sikap_nkri=$inputs['nkri'] * 0.05;
		 $pandangan_otda=$inputs['otda'] * 0.05;
		 $visi_misi=$inputs['visimisi'] * 0.05;
		 $koordinasi_komunikasi=$inputs['komunikasi'] * 0.05;
		 $t3=($sikap_nkri + $pandangan_otda + $visi_misi + $koordinasi_komunikasi);
		 $total_float=$t1+$t2+$t3;
					$data = array("nip_calon" => $nip,
					"kepangkatan" => $inputs['pangkat'],
					"diklat_pim" => $inputs['pim'],
					"pendidikan" => $inputs['pendidikan'],
					"riwayat_jabatan" => $inputs['jabatan'],
					"diklat_teknis" => $inputs['teknis'],
					"diklat_fungsional" => $inputs['fungsional'],
					"duk" => $inputs['duk'],
					"dp3" => $inputs['dp3'],
					"disiplin" => $inputs['disiplin'],
					"karir" => $inputs['karir'],
					"keragaman_diklat" => $inputs['diklat'],
					"seminar" => $inputs['seminar'],
					"pokir_strategis" => $inputs['pokir'],
					"sikap_nkri" => $inputs['nkri'],
					"pandangan_otda" => $inputs['otda'],
					"visi_misi" => $inputs['visimisi'],
					"koordinasi_komunikasi" => $inputs['komunikasi'],
					"total_score" => $inputs['totalSekor2'],
					"total_float" => $total_float,
					"id_kab_kota" => $id_kab_kota,
					"no_notadinas" => $no_notadinas);
					$saved=$this->m_home->add_penilaian($data);
					if($saved){
						$msg = 'Berhasil memasukkan penilaian.';
						$this->session->set_flashdata('success', $msg);
					}else{
						$msg = 'Gagal memasukkan data silahkan cek koneksi.';
						$this->session->set_flashdata('error', $msg);
					}
					
							$log = array("tanggal" => date("Y-m-d H:i:s"),
								"message" => $msg,
								"akun" => $_SESSION['id_user']
								);
								$this->m_home->add_log($log);
					redirect("chome/penilaian/".$id);
		}
	}
	
	public function editpenilaian($id,$nip){
		$this->isLogin();
		$inputs = $this->input->post(NULL, TRUE);
		$nip=str_replace("%20", " ", $nip);
		$data['datanya']=$this->m_home->getDetilDaerahUndangan($id)->result();
		foreach($data['datanya'] as $a){
				$no_notadinas=$a->no_notadinas;
				$id_kab_kota=$a->id_kab_kota;
			}
		if (!$inputs) {
			$data['nilainya']=$this->m_home->getNilaiCalon($nip,$no_notadinas,$id_kab_kota)->row();
			$data['nama']=$this->m_home->getNama($nip);
			$data['nip']=$nip;
			$this->load->view('template/head');
			$this->load->view('v_edit_nilai',$data);
			$this->load->view('template/foot');
		}else{
		$kepangkatan=$inputs['kepangkatan'] * 0.05;
		 $diklat_pim=$inputs['diklat_pim'] * 0.05;
		 $pendidikan=$inputs['pendidikan'] * 0.075;
		 $riwayat_jabatan=$inputs['riwayat_jabatan'] * 0.05;
		 $diklat_teknis=$inputs['diklat_teknis'] * 0.05;
		 $diklat_fungsional=$inputs['diklat_fungsional'] * 0.05;
		 $duk=$inputs['duk'] * 0.025;
		 $dp3=$inputs['dp3'] * 0.025;
		 $disiplin=$inputs['disiplin'] * 0.025;
		 $t1=($kepangkatan + $diklat_pim + $pendidikan + $riwayat_jabatan + $diklat_teknis + $diklat_fungsional + $duk + $dp3 + $disiplin);
		 $karir=$inputs['karir'] * 0.1;
		 $keragaman_diklat=$inputs['keragaman_diklat'] * 0.1;
		 $seminar=$inputs['seminar'] * 0.1;
		 $pokir_strategis=$inputs['pokir_strategis'] * 0.1;
		 $t2=($karir + $keragaman_diklat + $seminar + $pokir_strategis);
		 $sikap_nkri=$inputs['sikap_nkri'] * 0.05;
		 $pandangan_otda=$inputs['pandangan_otda'] * 0.05;
		 $visi_misi=$inputs['visi_misi'] * 0.05;
		 $koordinasi_komunikasi=$inputs['koordinasi_komunikasi'] * 0.05;
		 $t3=($sikap_nkri + $pandangan_otda + $visi_misi + $koordinasi_komunikasi);
		 $total_float=$t1+$t2+$t3;

			$data = array(
					"kepangkatan" => $inputs['pangkat'],
					"diklat_pim" => $inputs['pim'],
					"pendidikan" => $inputs['pendidikan'],
					"riwayat_jabatan" => $inputs['jabatan'],
					"diklat_teknis" => $inputs['teknis'],
					"diklat_fungsional" => $inputs['fungsional'],
					"duk" => $inputs['duk'],
					"dp3" => $inputs['dp3'],
					"disiplin" => $inputs['disiplin'],
					"karir" => $inputs['karir'],
					"keragaman_diklat" => $inputs['diklat'],
					"seminar" => $inputs['seminar'],
					"pokir_strategis" => $inputs['pokir'],
					"sikap_nkri" => $inputs['nkri'],
					"pandangan_otda" => $inputs['otda'],
					"visi_misi" => $inputs['visimisi'],
					"koordinasi_komunikasi" => $inputs['komunikasi'],
					"total_score" => $inputs['totalSekor2'],
					"total_float" => $total_float
					);
					$saved=$this->m_home->update_penilaian($nip,$data,$id_kab_kota,$no_notadinas);
					if($saved){
						$msg = 'Berhasil mengedit penilaian.';
						$this->session->set_flashdata('success', $msg);
					}else{
						$msg = 'Gagal memasukkan mengedit ';
						$this->session->set_flashdata('error', $msg);
					}
					
					$log = array("tanggal" => date("Y-m-d H:i:s"),
						"message" => $msg,
						"akun" => $_SESSION['id_user']
					);
								$this->m_home->add_log($log);
					redirect("chome/penilaian/".$id);
		}
	}
	
	public function deletepenilaian($id,$nip) {
		$this->isLogin();  
		$nip=str_replace("%20", " ", $nip);
		$data['datanya']=$this->m_home->getDetilDaerahUndangan($id)->result();
		foreach($data['datanya'] as $a){
				$no_notadinas=$a->no_notadinas;
				$id_kab_kota=$a->id_kab_kota;
			}
		if (isset($id)) {
			$saved=$this->m_home->delete_penilaian($nip,$no_notadinas,$id_kab_kota);
			if ($saved)
			{
				$msg = 'Berhasil menghapus data Penilaian.';
				$this->session->set_flashdata('success', $msg);
			}
			else
			{
				$msg = 'Gagal menghapus data Penilaian.';
				$this->session->set_flashdata('error', $msg);
			}
		}
		
		$log = array("tanggal" => date("Y-m-d H:i:s"),
		  			 "message" => $msg,
		 			 "akun" => $_SESSION['id_user']
					);
		$this->m_home->add_log($log);
		redirect("chome/penilaian/".$id);
	}
	
	public function listdapil()
	{
		$this->isLogin(); 
		$data['dapilnya'] = $this->m_home->getAllDapil()->result();
		$this->load->view('template/head');
		$this->load->view('vlist_dapil',$data);
		$this->load->view('template/foot');
	}
	
	public function add_undangansekda(){
		$inputs = $this->input->post(NULL, TRUE);
		if (!$inputs) {
			$msg = 'Maaf isi data dengan benar.';
			$this->session->set_flashdata('error', 	$msg);
			redirect("chome/undangansekda");
		} else {
				
				$data = array("tgl_nota" => $inputs['tglnota'],
					"tgl_rapat" => $inputs['tglrapat'],
					"jam_rapat" => $inputs['jamrapat'],
					"no_notadinas" => $inputs['nosurat']);
				$saved=$this->m_home->add_undangan($data);
				foreach($this->input->post('kota') as $k){
					$data = array("no_notadinas" => $inputs['nosurat'],
					"id_kab_kota" => $k);
					$saved2=$this->m_home->add_detil_undangan($data);
				}
				if ($saved)
				{
					$msg = 'Data undangan pemilihan SEKDA berhasil dibuat, silahkan menuju menu list undangan untuk men<i>download</i> undangan dalam format .pdf.';
					$this->session->set_flashdata('success', $msg);
				}
				else
				{
					$msg = 'Gagal membuat data undangan pemilihan silahkan cek kembali data yang anda isi.';
					$this->session->set_flashdata('error', 	$msg);
				}
				
			$log = array("tanggal" => date("Y-m-d H:i:s"),
		  			 "message" => $msg,
		 			 "akun" => $_SESSION['id_user']
					);
			$this->m_home->add_log($log);
			redirect("chome/undangansekda");
			
		}
	}
	
	
	public function listcalon($id){
		$this->isLogin();
		foreach($this->m_home->getNosurat($id)->result() as $a){
			$nosurat=$a->no_notadinas;
			$iddaerah=$a->id_kab_kota;
		}
		$data['datanya'] = $this->m_home->getDetailListSekda($nosurat,$iddaerah)->result();
		$this->load->view('template/head');
		$this->load->view('vlist_calon',$data);
		$this->load->view('template/foot');
	}
	
	
	public function calonsekda($id_detil_daerah){
		$this->isLogin();
		$data['datanya'] = $this->m_home->getIdDapil($id_detil_daerah)->result();
		$this->load->view('template/head');
		$this->load->view('vcalon',$data);
		$this->load->view('template/foot');
	}	
	
		public function add_calon($id){
		$inputs = $this->input->post(NULL, TRUE);
		
		if ((!$inputs)) {
			$msg = 'Maaf isi data dengan benar.';
			$this->session->set_flashdata('error', 	$msg);
			redirect("chome/calonsekda/".$id);
		} else {
				$cekcalon=$this->m_home->cekJmlCalon($inputs['nip'],$inputs['nosurat'])->num_rows();
				if($cekcalon<1){
					$data = array("nip" => $inputs['nip'],
						"nama" => $inputs['nama'],
						"tgl_lahir" => $inputs['tgllahir'],
						"pangkat" => $inputs['pangkat'],
						"tmt" => $inputs['tmt'],
						"pend_umum" => $inputs['pend_umum'],
						"pend_struktural" => $inputs['pend_struk'],
						"jabatan" => $inputs['jabatannya'],
						"id_kab_kota" => $inputs['kab_kota'],
						"no_notadinas" => $inputs['nosurat']);
					$saved=$this->m_home->add_calon_sekda($data);
					$i=0;
					foreach($inputs['jabatan'] as $j){
						$data2 = array("jabatan" => $j,
						"nip_calon" => $inputs['nip'],
						"eselon" => $inputs['eselon'][$i]);
						$saved=$this->m_home->add_karir_calon($data2);
						$i++;
					}
					
					if ($saved)
					{
						$msg = 'Data diri calon sekertaris daerah berhasil di input';
						$this->session->set_flashdata('success', $msg);
						$log = array("tanggal" => date("Y-m-d H:i:s"),
						 "message" => $msg,
						 "akun" => $_SESSION['id_user']
						);
			$this->m_home->add_log($log);
						redirect("chome/listdapil");
					}
					else
					{
						$msg = 'Gagal menginput data diri calon silahkan cek kembali data yang anda isi.';
						$this->session->set_flashdata('error', 	$msg);
						$log = array("tanggal" => date("Y-m-d H:i:s"),
								 "message" => $msg,
								 "akun" => $_SESSION['id_user']
								);
						$this->m_home->add_log($log);
						redirect("chome/calonsekda/".$id);
					}
				}else{
						$msg = 'Maaf Data Calon SEKDA denga NIP '.$inputs['nip'].' dengan surat dinas '.$inputs['nosurat'].' sudah ada sebelumnya';
						$this->session->set_flashdata('error', 	$msg);
						$log = array("tanggal" => date("Y-m-d H:i:s"),
								 "message" => $msg,
								 "akun" => $_SESSION['id_user']
								);
						$this->m_home->add_log($log);
						redirect("chome/calonsekda/".$id);
				}
			
			}
		}
	
	public function alasan($id_detil_daerah){
		$this->isLogin();
		$data['datanya'] = $this->m_home->getIdDapil($id_detil_daerah)->result();
		$this->load->view('template/head');
		$this->load->view('valasan',$data);
		$this->load->view('template/foot');
	}
	
	
		public function add_alasan($id){
		$inputs = $this->input->post(NULL, TRUE);
		
		if ((!$inputs) || (strlen($inputs['alasan'])<=2)) {
			$msg = 'Maaf isi data dengan benar.';
			$this->session->set_flashdata('error', 	$msg);
			redirect("chome/alasan/".$id);
		} else {
				$data = array("id_kab_kota" => $inputs['kota'],
					"alasan" => $inputs['alasan'],
					"perihal" => $inputs['perihal'],
					"no_surat" => $inputs['nosurat'],
					"no_surat_gub" => $inputs['no_surat_gub'],
					"tgl_surat_gub" => $inputs['tgl_surat_gub']);
				$saved=$this->m_home->add_alasan_usulan($data);
				
				if ($saved)
				{
					$msg = 'Data alasan pengusulan  dan data surat konsultasi gubernur telah berhasil diinput';
					$this->session->set_flashdata('success', $msg);
					$log = array("tanggal" => date("Y-m-d H:i:s"),
							 "message" => $msg,
							 "akun" => $_SESSION['id_user']
							);
					$this->m_home->add_log($log);
					redirect("chome/listdapil");
				}
				else
				{
					$msg = 'Gagal membuat data alasan pengusulan  dan data surat konsultasi gubernur silahkan cek kembali data yang anda isi.';
					$this->session->set_flashdata('error', 	$msg);
					$log = array("tanggal" => date("Y-m-d H:i:s"),
							 "message" => $msg,
							 "akun" => $_SESSION['id_user']
							);
					$this->m_home->add_log($log);
					redirect("chome/alasan/".$id);
				}
			
			}
		}
	
	
	public function deletecalon($iddaerah,$id,$nip) {
		$this->isLogin();  
		$nip=str_replace("%20", " ", $nip);
		if (isset($id)) {
			$saved=$this->m_home->delete_calonnya($id);
			$saved=$this->m_home->delete_karirnya($nip);
			if ($saved)
			{
				$msg = 'Berhasil menghapus data calon.';
				$this->session->set_flashdata('success', $msg);
			}
			else
			{
				$msg = 'Gagal menghapus data calon.';
				$this->session->set_flashdata('error', $msg);
			}
		}
		$log = array("tanggal" => date("Y-m-d H:i:s"),
		  			 "message" => $msg,
		 			 "akun" => $_SESSION['id_user']
					);
			$this->m_home->add_log($log);
		redirect("chome/listcalon/".$iddaerah);
	}
	
	
	public function downloadExcel($id) {

		$this->load->library('ExportToExcel');
		foreach($this->m_home->getNosurat($id)->result() as $a){
			$nosurat=$a->no_notadinas;
			$iddaerah=$a->id_kab_kota;
		}
		foreach($this->m_home->getSuratGub($nosurat,$iddaerah)->result() as $p){
			$alasan=$p->alasan;
		}
		$data['datanya'] = $this->m_home->getNilai($nosurat,$iddaerah)->result();
		$data['namakota'] = $this->m_home->getNamaKota($iddaerah);
		$data['nomersurat']=$nosurat;
		$data['alasan']=$alasan;
		$this->load->view('vexcel',$data);
		$stringData = $this->load->view('vexcel', $data, true);
		$excel=new ExportToExcel();
		$excel->exportWithPage("$stringData","Penilaian_tim.xls");
		
		
		
 
     

	}
	
	
	function pdfsuratmendagri($id){
		//$data['dapilnya'] = $this->m_home->getNosurat($iddaerah)->result();
		//$this->load->view('pdf_suratmendagri',$data);
		$this->load->library('pdf');
		$namafile="surat_mendagri";
		$data['dapilnya'] = $this->m_home->getNosurat($id)->result();
		$html = $this->load->view('pdf_suratmendagri', $data, true);
		$this->pdf->pdf_create($html, $namafile);
	}
	
	function pdfbapsekda($id)
	{
		$this->load->library('pdf');
		$namafile="Bap_sekda";
		$data['resume'] = $this->m_home->getUndangan($id)->result();
		$html = $this->load->view('pdf_bap_sekda', $data, true); 
		$this->pdf->pdf_create($html, $namafile);
		/*$data['resume'] = $this->m_home->getUndangan($id)->result();
		$this->load->view('pdf_bap_sekda', $data);*/
	}
	
	function pdfresume($id)
	{
		$this->load->library('pdf');
		$namafile="Resume_penilaian";
		$data['resume'] = $this->m_home->getUndangan($id)->result();
		$html = $this->load->view('pdf_resume', $data, true); 
		$this->pdf->pdf_create($html, $namafile);
		/*	$data['resume'] = $this->m_home->getUndangan($id)->result();
		$this->load->view('pdf_resume', $data);*/
	}
	function pdfbagan($id)
	{
		$this->load->library('pdf');
		$namafile="Bagan_pemilihan";
		$data['resume'] = $this->m_home->getUndangan($id)->result();
		$html = $this->load->view('pdf_bagan', $data, true); 
		$this->pdf->pdf_create($html, $namafile);
	}
	function pdfbap($id)
	{
		$this->load->library('pdf');
		
		$namafile="ND_BAP";
		$data['undangan'] = $this->m_home->getUndangan($id)->result();
		$html = $this->load->view('pdf_bap', $data, true); 
		
		$this->pdf->pdf_create($html, $namafile);
		/*	$data['undangan'] = $this->m_home->getUndangan($id)->result();
		$this->load->view('pdf_bap', $data);*/
	}
	
	function pdfundangan2($id){
		$data['undangan'] = $this->m_home->getUndangan($id)->result();
		$this->load->view('pdf_undangan', $data); 
	}
	
	function pdfundangan($id)
	{
		$this->load->library('pdf');
		$namafile="ND_undangan_sekda";
		$data['undangan'] = $this->m_home->getUndangan($id)->result();
		//$this->load->view('pdf_undangan', $data);
		$html = $this->load->view('pdf_undangan', $data, true); 
		$this->pdf->pdf_create($html, $namafile);
	}
	
	function pdf($i)
	{
		
		$data['undangan'] = $this->m_home->getUndangan($i)->result();
		$this->load->view('pdf_undangan', $data);
		
	}
	
	public function listundangan()
	{
		$this->isLogin(); 
		$data['undangan'] = $this->m_home->getAllUndangan()->result();
		$this->load->view('template/head');
		$this->load->view('vlistundangan',$data);
		$this->load->view('template/foot');
		
	}
	
	public function listbap()
	{
		$this->isLogin(); 
		$data['undangan'] = $this->m_home->getAllUndangan()->result();
		$this->load->view('template/head');
		$this->load->view('vlistbap',$data);
		$this->load->view('template/foot');
		
	}
	
	public function listhasilpenilaian()
	{
		$this->isLogin(); 
		$data['undangan'] = $this->m_home->getAllUndangan()->result();
		$this->load->view('template/head');
		$this->load->view('vlisthasilpenilaian',$data);
		$this->load->view('template/foot');
		
	}
	public function listsuratmendagri()
	{
		$this->isLogin(); 
		$data['dapilnya'] = $this->m_home->getAllDapil()->result();
		$this->load->view('template/head');
		$this->load->view('vlist_suratmendagri',$data);
		$this->load->view('template/foot');
		
	}
		
	function listpenjabat(){
		$this->isLogin(); 
		$data['petugas'] = $this->m_home->getAllPetugas()->result();
		$this->load->view('template/head');
		$this->load->view('vndeso',$data);
		$this->load->view('template/foot');
	}
	
	function edit_penjabat() {
		$inputs = $this->input->post(NULL, TRUE);
		 if ($inputs) {
			$data = array("nama_petugas" => $inputs['nama_penjabat'],"jabatan" => $inputs['jabatannya'], "title"=>strtoupper($inputs['title']));
			//print_r($data);
			//echo $inputs['id_jabatan'];
			$saved=$this->m_home->update_penjabat($data, $inputs['id_jabatan']);
			
			if ($saved)
			{
				$msg = 'Data Penjabat Berhasil Diedit';
				$this->session->set_flashdata('success', $msg);
			}
			else
			{
				$msg = 'Data penjabat Gagal Diedit';
				$this->session->set_flashdata('error', $msg);
			}
			$log = array("tanggal" => date("Y-m-d H:i:s"),
		  			 "message" => $msg,
		 			 "akun" => $_SESSION['id_user']
					);
			$this->m_home->add_log($log);
			redirect("chome/listpenjabat");
		}
	}

	
	public function change_user(){
		$inputs = $this->input->post(NULL, TRUE);
		if ($inputs) {
			if(($inputs['pass1']!="") && ($inputs['pass2']!="")){
				if($inputs['pass1']==$inputs['pass2']){
					$data = array(
								"nama" => $inputs['username'],
								"email" => $inputs['email'],
								"pass" => md5($inputs['pass1'])
								);
				}else{
					$msg = 'Password tidak sama !!!';
					$this->session->set_flashdata('error', $msg);
				}
			}else{
					$data = array(
								"nama" => $inputs['username'],
								"email" => $inputs['email']
								);
			}
			$saved=$this->m_home->update_akun($data, $_SESSION['id_user']);
				
			if ($saved)
			{
				$_SESSION['username']=$inputs['username'];
				$msg = 'Berhasil Update Akun';
				$this->session->set_flashdata('success', $msg);
			}
			else
			{
				$msg = 'Gagal Update Akun';
				$this->session->set_flashdata('error', $msg);
			}
			$log = array("tanggal" => date("Y-m-d H:i:s"),
		  			 "message" => $msg,
		 			 "akun" => $_SESSION['id_user']
					);
			$this->m_home->add_log($log);
			redirect("chome/settingUser");
			
		}
	}
	
	public function settingUser(){
		$this->isLogin();
		$data['datanya'] = $this->m_home->getDetailUser($_SESSION['id_user'])->result_array();
		$this->load->view('template/head');
		$this->load->view('vsettingUser',$data);
		$this->load->view('template/foot');
	}
	
	public function listProv(){
		$this->isLogin();
		$data['provnya'] = $this->m_home->getAllProv()->result();
		$this->load->view('template/head');
		$this->load->view('vlistProv',$data);
		$this->load->view('template/foot');
	}
	
	
	public function add_prov(){
		$inputs = $this->input->post(NULL, TRUE);
		if (!$inputs) {
			$msg = 'Maaf isi data dengan benar.';
			$this->session->set_flashdata('error', 	$msg);
			redirect("chome/listProv");
		} else {
				
				$data = array("nm_provinsi" =>strtoupper($inputs['nm_prov']),"ibukota_prov"=>ucwords(strtolower($inputs['ibukota_prov'])));
				$saved=$this->m_home->add_provnya($data);
				if ($saved)
				{
					$msg = 'Berhasil menambah data provinsi.';
					$this->session->set_flashdata('success', $msg);
				}
				else
				{
					$msg = 'Gagal menambah data provinsi.';
					$this->session->set_flashdata('error', 	$msg);
				}
				$log = array("tanggal" => date("Y-m-d H:i:s"),
		  			 "message" => $msg,
		 			 "akun" => $_SESSION['id_user']
					);
			$this->m_home->add_log($log);
			redirect("chome/listProv");
			
		}
	}
	
	public function delete_prov($id) {
	$this->isLogin();  
		if (isset($id)) {
			$saved=$this->m_home->delete_provnya($id);
			$saved=$this->m_home->delete_kab_kotanya2($id);
			if ($saved)
			{
				$msg = 'Berhasil menghapus data Provinsi.';
				$this->session->set_flashdata('success', $msg);
			}
			else
			{
				$msg = 'Gagal menghapus data Provinsi.';
				$this->session->set_flashdata('error', $msg);
			}
		}
		$log = array("tanggal" => date("Y-m-d H:i:s"),
		  			 "message" => $msg,
		 			 "akun" => $_SESSION['id_user']
					);
			$this->m_home->add_log($log);
		redirect("chome/listProv");
	}
	
	function edit_prov() {
		$inputs = $this->input->post(NULL, TRUE);
		 if ($inputs) {
			$data = array(
						"nm_provinsi" => $inputs['nm_prov'],"ibukota_prov"=>ucwords(strtolower($inputs['ibukota_prov']))
					);
			$saved=$this->m_home->update_provnya($data, $inputs['id_prov']);
			
			if ($saved)
			{
				$msg = 'Data Provinsi berhasil diedit';
				$this->session->set_flashdata('success', $msg);
			}
			else
			{
				$msg = 'Data Provinsi Gagal diedit';
				$this->session->set_flashdata('error', $msg);
			}
			$log = array("tanggal" => date("Y-m-d H:i:s"),
		  			 "message" => $msg,
		 			 "akun" => $_SESSION['id_user']
					);
			$this->m_home->add_log($log);
			redirect("chome/listProv");
		}
	}
	
	public function listKabKota(){
		$this->isLogin();
		$data['kotanya'] = $this->m_home->getListKota()->result();
		$data['provnya'] = $this->m_home->getAllProv()->result();
		$this->load->view('template/head');
		$this->load->view('vlistKota',$data);
		$this->load->view('template/foot');
	}
	
	public function add_kab_kota(){
		$inputs = $this->input->post(NULL, TRUE);
		if (!$inputs) {
			$msg = 'Maaf isi data dengan benar.';
			$this->session->set_flashdata('error', 	$msg);
		} else {
				
				$ceking=$this->m_home->cekKabKota($inputs['nm_kab_kota'])->num_rows();
				if($ceking < 1){
					$data = array(
								"id_provinsi" =>$inputs['provnya'],
								"nm_kab_kota" =>$inputs['nm_kab_kota']
							);
					$saved=$this->m_home->add_kab_kotanya($data);
					if ($saved)
					{
						$msg = 'Berhasil menambah data Kabupaten atau Kota.';
						$this->session->set_flashdata('success', $msg);
					}
					else
					{
						$msg = 'Gagal menambah data Kabupaten atau Kota.';
						$this->session->set_flashdata('error', 	$msg);
					}
				}else{
					$msg = 'Maaf nama kabupaten atau kota sudah ada sebelumnya.';
					$this->session->set_flashdata('error', 	$msg);
				}
			
		}
		$log = array("tanggal" => date("Y-m-d H:i:s"),
		  			 "message" => $msg,
		 			 "akun" => $_SESSION['id_user']
					);
			$this->m_home->add_log($log);
			redirect("chome/listKabKota");
	}
	
	public function delete_kab_kota($id) {
	$this->isLogin();  
		if (isset($id)) {
			$saved=$this->m_home->delete_kab_kotanya($id);
			if ($saved)
			{
				$msg = 'Berhasil menghapus data Kabupaten atau kota.';
				$this->session->set_flashdata('success', $msg);
			}
			else
			{
				$msg = 'Gagal menghapus data  Kabupaten atau kota.';
				$this->session->set_flashdata('error', $msg);
			}
		}
		$log = array("tanggal" => date("Y-m-d H:i:s"),
		  			 "message" => $msg,
		 			 "akun" => $_SESSION['id_user']
					);
			$this->m_home->add_log($log);
		redirect("chome/listKabKota");
	}
	
	function edit_kab_kota() {
		$inputs = $this->input->post(NULL, TRUE);
		 if ($inputs) {
			$data = array(
						"nm_kab_kota" => $inputs['nm_kab_kota'],
						"id_provinsi" => $inputs['provnya']
					);
			$saved=$this->m_home->update_kab_kotanya($data, $inputs['id_kab_kota']);
			
			if ($saved)
			{
				$msg = 'Data Kabupaten atau Kota berhasil diedit';
				$this->session->set_flashdata('success', $msg);
			}
			else
			{
				$msg = 'Data Kabupaten atau Kota Gagal diedit';
				$this->session->set_flashdata('error', $msg);
			}
			$log = array("tanggal" => date("Y-m-d H:i:s"),
		  			 "message" => $msg,
		 			 "akun" => $_SESSION['id_user']
					);
			$this->m_home->add_log($log);
			redirect("chome/listKabKota");
		}
	}
	
	public function edit_calon($id,$nip){
		$this->isLogin();
		$inputs = $this->input->post(NULL, TRUE);
		$nip=str_replace("%20", " ", $nip);
		$data['datanya']=$this->m_home->getDetilDaerahUndangan($id)->result();
		foreach($data['datanya'] as $a){
				$data['no_notadinas']=$no_notadinas=$a->no_notadinas;
				$data['id_kab_kota']=$id_kab_kota=$a->id_kab_kota;
		}
		if (!$inputs) {
				$data['calon'] = $this->m_home->getCalonDetil($nip,$no_notadinas,$id_kab_kota)->row();
				$data['karirnya'] = $this->m_home->getKarir($nip)->result();
				$this->load->view('template/head');
				$this->load->view('vcalon_edit',$data);
				$this->load->view('template/foot');
		}else{
		
					$array_data = array("nip" => $nip,
						"nama" => $inputs['nama'],
						"tgl_lahir" => $inputs['tgllahir'],
						"pangkat" => $inputs['pangkat'],
						"tmt" => $inputs['tmt'],
						"pend_umum" => $inputs['pend_umum'],
						"pend_struktural" => $inputs['pend_struk'],
						"jabatan" => $inputs['jabatannya']);
					$saved=$this->m_home->update_calon($nip,$array_data,$inputs['kab_kota'],$inputs['nosurat']);
					
					$this->m_home->delete_karirnya($nip);
					$i=0;
					foreach($inputs['jabatan'] as $j){
						$data2 = array("jabatan" => $j,
						"nip_calon" => $nip,
						"eselon" => $inputs['eselon'][$i]);
						$saved=$this->m_home->add_karir_calon($data2);
						$i++;
					}
					
					if ($saved)
					{
						$msg = 'Data diri calon sekertaris daerah berhasil di edit';
						$this->session->set_flashdata('success', $msg);
						$log = array("tanggal" => date("Y-m-d H:i:s"),
								 "message" => $msg,
								 "akun" => $_SESSION['id_user']
								);
						$this->m_home->add_log($log);
						redirect("chome/listdapil");
					}
					else
					{
						$msg = 'Gagal mengedit data diri calon silahkan cek kembali data yang anda isi.';
						$this->session->set_flashdata('error', 	$msg);
						$log = array("tanggal" => date("Y-m-d H:i:s"),
								 "message" => $msg,
								 "akun" => $_SESSION['id_user']
								);
						$this->m_home->add_log($log);
						redirect("chome/calonsekda/".$id);
					}
				
		}
	}
}