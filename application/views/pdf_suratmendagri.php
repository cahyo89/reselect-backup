  <script type="text/php">

        if ( isset($pdf) ) {

          $font = Font_Metrics::get_font("helvetica", "bold");
          $pdf->page_text(72, 18, "Header: {PAGE_NUM} of {PAGE_COUNT}", $font, 6, array(0,0,0));
		echo "<h1>BISAA</h1>";
        }else{
		echo "<h1>ndak</h1>";
		}
        </script>
<?php

function cariTanggal2($tglnya){
	$t=$tglnya;
	$tg=explode("-", $t);
	$a=$tg[1];

	switch ($a) {
		case 1:
			$bulan="Januari";
			break;
		case 2:
			$bulan="Februari";
			break;
		case 3:
			$bulan="Maret";
			break;
		case 4:
			$bulan="April";
			break;
		case 5:
			$bulan="Mei";
			break;
		case 6:
			$bulan="Juni";
			break;
		case 7:
			$bulan="Juli";
			break;
		case 8:
			$bulan="Agustus";
			break; 
		case 9:
			$bulan="September";
			break;
		case 10:
			$bulan="Oktober";
			break;
		case 11:
			$bulan="November";
			break;
		case 12:
			$bulan="Desember";
			break;
	}
	
	$tanggal2=number_format($tg[0])." ".$bulan." ".$tg[2];
	return $tanggal2;
}
foreach($dapilnya as $u){
	$id_kab_kota=$u->id_kab_kota;
	$no_notadinas=$u->no_notadinas;
}

foreach($this->m_home->getNamaProv($id_kab_kota)->result() as $r ){
	$nama_prov=$r->nm_provinsi;
	$ibukota_prov=$r->ibukota_prov;
}

foreach($this->m_home->getSuratGub($no_notadinas,$id_kab_kota)->result() as $r ){
	$no_surat_gub=$r->no_surat_gub;
	$tgl_surat_gub=cariTanggal2($r->tgl_surat_gub);
	$alasan=$r->alasan;
	$perihal=$r->perihal;
}
?>

<html>
	<head>
		<style>
			body{
				font-family:Tahoma, Geneva, sans-serif; 
				font-size:13pt; 
				/*margin: 3em;*/
				margin:5px;
			}
			div {
			/* background: #EDEDED;
			   border: solid 1px #CCC;
			  width: 650px;*/
			  width: 612px;
			
			}

			.break-word {
			  word-wrap: break-word;
			}
			
		</style>
	</head>
	<body>
			<table width="100%" style="margin-bottom:50px;">
			<tr>
				<td style="font-size:11px;  text-align: right;">&nbsp;</td>
			</tr>
			
			<tr>
				<td style="font-size:10pt;  text-align: center;"><b>&nbsp;</b></td>
			</tr><tr>
				<td style="font-size:10pt;  text-align: center;"><b>&nbsp;</b></td>
			</tr><tr>
				<td style="font-size:10pt;  text-align: center;"><b>&nbsp;</b></td>
			</tr><tr>
				<td style="font-size:10pt;  text-align: center;"><b>&nbsp;</b></td>
			</tr>
			<tr>
				<td style="font-size:10pt;  text-align: center;"><b>&nbsp;</b></td>
			</tr>
			
		</table>
		<table width="100%"   style="font-size:12pt; margin-top:20px;">
			
			<tr>
				<td width="50">&nbsp;</td>
				<td width="5">&nbsp;</td>
				<td width="190">&nbsp;</td>
				<td><span style="margin-right:150px">&nbsp;</span>Jakarta,</td>
			</tr>
			<tr>
				<td >&nbsp;</td>
				<td >&nbsp;</td>
				<td >&nbsp;</td>
				<td><span style="margin-right:150px">&nbsp;</span>Kepada</td>
			</tr>
			<tr>
				<td valign="top">Nomor</td>
				<td valign="top">:	&nbsp;</td>
				<td >&nbsp;</td>
				<td><span style="margin-right:120px">&nbsp;</span> Yth. Gubernur <?php echo ucwords(strtolower($nama_prov));?> </td>
			</tr>
			<tr>
				<td>Sifat</td>
				<td>:</td>
				<td>RAHASIA</td>
				<td><span style="margin-right:150px">&nbsp;</span>di</td>
			</tr>
			<tr>
				<td>Lampiran</td>
				<td>:&nbsp;</td>
				<td>&nbsp;</td>
				<td><span style="margin-right:160px">&nbsp;</span><b><?=$ibukota_prov?></b></td>
			</tr>
		
			<tr>
				<td valign="top">Perihal</td>
				<td valign="top">:</td>
				<td valign="top">Persetujuan Pengangkatan Calon Sekretaris Daerah <?php $p=$this->m_home->getNamaKota($id_kab_kota); echo str_replace("Kab.","Kabupaten",$p);?></td>
				<td>   &nbsp;</td>
			</tr>
			</tr>
	
		</table>
		<div class="break-word" style="text-align: justify; margin-top:20px; margin-left:87px" >
		<p><span style="margin-right:40px">&nbsp;</span>Sehubungan dengan Surat Saudara Nomor <?=$no_surat_gub?> tanggal <?=$tgl_surat_gub?> perihal <?=$perihal?>, dengan hormat disampaikan sebagai berikut : <br/>
			<ol style="margin-left:-14px">
				<li style="margin-bottom:15px">Berdasarkan ketentuan Pasal 122 ayat (1) dan (3) Undang-Undang Nomor 32 Tahun 2004 tentang Pemerintahan Daerah dan Peraturan Menteri Dalam Negeri Nomor 5 Tahun 2005 tentang  Pedoman Penilaian Calon Sekretaris Daerah Provinsi dan Kabupaten/Kota serta Pejabat Struktural Eselon II di Lingkungan Kabupaten/Kota, telah dilakukan penilaian terhadap Pegawai Negeri Sipil Calon Sekretaris Daerah <?=$this->m_home->getNamaKota($id_kab_kota);?> yang Saudara usulkan.<br/></li>
				<li style="margin-bottom:-20px">Dari hasil penilaian terhadap 3 (tiga) orang Calon Sekretaris Daerah <?=$this->m_home->getNamaKota($id_kab_kota);?>, 1 (satu) orang yang paling memenuhi persyaratan dan dapat disetujui oleh Menteri Dalam Negeri, dan  untuk selanjutnya sesuai Peraturan Pemerintah Nomor 23 Tahun 2011 tentang Perubahan Atas Peraturan Pemerintah Nomor 19 Tahun 2010 tentang Tata Cara Pelaksanaan Tugas dan Wewenang serta Kedudukan Keuangan Gubernur sebagai Wakil Pemerintah di Wilayah Provinsi, agar Saudara menetapkan Pengangkatan Sekretaris Daerah <?=$this->m_home->getNamaKota($id_kab_kota);?>, yaitu :
				<br/>
				<table style="margin-bottom:15px; font-size:12pt; font-weight:normal;" width="100%">
					<? $nipnya=$this->m_home->getCalonSkorTinggi($no_notadinas,$id_kab_kota); ?>
				<? foreach($this->m_home->getDetailListSekda2($nipnya)->result() as $m){?>
								<tr>
									<td width="170px">- Nama</td>
									<td width="5px">:</td>
									<td style="font-weight:bold;"><?=$m->nama?>  </td>
								</tr>
								<tr>
									<td>- NIP</td>
									<td width="5px">:</td>
									<td><?=$m->nip?> </td>
								</tr>
								<tr>
									<td>- Tanggal Lahir</td>
									<td width="5px">:</td>
									<td><?=$m->tgl_lahir?></td>
								</tr>
								<tr>
									<td>- Pangkat/Gol. Ruang</td>
									<td width="5px">:</td>
									<td><?=$m->pangkat?> </td>
								</tr>
								<tr>
									<td valign="top">- Jabatan</td>
									<td valign="top" width="5px">:</td>
									<td valign="top"><?=$m->jabatan?> </td>
								</tr>
								
							<?}?>
							</table>
				</li>
				<li>Selanjutnya hasil penilaian masing-masing calon Sekretaris Daerah <?=$this->m_home->getNamaKota($id_kab_kota);?>, sebagaimana terlampir.</li>
			</ol>
			<span style="margin-right:60px">&nbsp;</span>Demikian untuk maklum.
			<br/><br/><br/>
			<table align="right">
				<tr>
					<td style="text-align: center; font-size:12pt;"><b><?php echo strtoupper($this->m_home->getPenjabat3(3)); ?></b></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td style="text-align: center; font-size:12pt;"><b><?=$this->m_home->getPenjabat2(3)?></b></td>
				</tr>
			</table>
		</p>
		</div>
		
	</body>
</html>