<?php
	function cariTanggal($tglnya){
	$t=$tglnya;
	$tg=explode("-", $t);
	$a=$tg[1];

	switch ($a) {
		case 1:
			$bulan="JANUARI";
			break;
		case 2:
			$bulan="FEBRUARI";
			break;
		case 3:
			$bulan="MARET";
			break;
		case 4:
			$bulan="APRIL";
			break;
		case 5:
			$bulan="MEI";
			break;
		case 6:
			$bulan="JUNI";
			break;
		case 7:
			$bulan="JULI";
			break;
		case 8:
			$bulan="AGUSTUS";
			break; 
		case 9:
			$bulan="SEPTEMBER";
			break;
		case 10:
			$bulan="OKTOBER";
			break;
		case 11:
			$bulan="NOVEMBER";
			break;
		case 12:
			$bulan="DESEMBER";
			break;
	}
	
	$tanggal2=number_format($tg[0])." ".$bulan." ".$tg[2];
	return $tanggal2;
}

foreach($resume as $u){
	$tgl_rapat=$u->tgl_rapat;
	$no_notadinas=$u->no_notadinas;
	//$jum=$this->m_home->getKabDetail($u->no_notadinas)->num_rows();
	
}
?>

<html>
	<head>
		<style>
			.table { border:thin black solid; margin:2pt; border-width:thick;}
			body{
				font-family:Tahoma, Geneva, sans-serif; 
				font-size:12pt; 
				/*margin: 3em;*/
				margin:5px;
			}
			div {
			/* background: #EDEDED;
			   border: solid 1px #CCC;
			  width: 650px;*/
			  width: 690px;
			
			}
			.break-word {
			  word-wrap: break-word;
			}
			
			table.tabel2 {
				border: 1pt solid black;
				border-collapse: collapse;
			}
			table.tabel2 td,table.tabel2 th	 {
				border: 1pt solid black;
				}
			table.tabel2 th	{
			background-color:#E0E0E0;	
			}
		</style>
	</head>
	<body>
		<table width="100%" style="background-color:#E0E0E0;margin-bottom:55px; font-size:14pt; font-weight:bold; text-align: center;">	
			<tr>
				<td>RESUME</td>
			</tr>
			<tr>
				<td>PENILAIAN CALON SEKRETARIS DAERAH KABUPATEN/KOTA</td>
			</tr>
			<tr>
				<td>TANGGAL, <?=cariTanggal($tgl_rapat)?></td>
			</tr>
		</table>
		
			<ol>
		<?php foreach($this->m_home->getAllDapil2($no_notadinas)->result() as $p){?>
				<li style="font-weight:bold;margin-bottom:15px;">Calon  Sekretaris  Daerah <?=$p->nm_kab_kota?> Provinsi <?=ucwords(strtolower($p->nm_provinsi))?> <br/>
					<ol>
						<?php foreach($this->m_home->getDetailListSekda($no_notadinas,$p->id_kab_kota)->result() as $d){?>
						<li style="margin-bottom:35px; ">
							<table style="margin-bottom:15px; font-size:12pt; font-weight:normal;" width="100%">
								<tr>
									<td width="130px" >Nama</td>
									<td style="font-weight:bold;">: <?=$d->nama?></td>
								</tr>
								<tr>
									<td>NIP</td>
									<td>: <?=$d->nip?></td>
								</tr>
								<tr>
									<td>Tanggal Lahir</td>
									<td>: <?=$d->tgl_lahir?></td>
								</tr>
								<tr>
									<td>Pangkat/Gol. Ruang</td>
									<td>: <?=$d->pangkat?></td>
								</tr>
								<tr>
									<td>TMT</td>
									<td>: <?=$d->tmt?></td>
								</tr>
								<tr>
									<td>Pendidikan</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>- Umum</td>
									<td>: <?=$d->pend_umum?></td>
								</tr>
								<tr>
									<td>- Struktural</td>
									<td>: <?=$d->pend_struktural?></td>
								</tr>
								<tr>
									<td>Jabatan</td>
									<td>: <?=$d->jabatan?></td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td>: <?=$this->m_home->getSekor($no_notadinas,$p->id_kab_kota,$d->nip)?></td>
								</tr>
							</table>
							PERJALANAN KARIER<br/><br/>
							<table width="100%" class="tabel2" style=" font-size:12pt; ">
								<tr>
									<th width="50px" style="text-align: center;">No.</th>
									<th style="text-align: center;">Jabatan</th>
									<th width="80px" style="text-align: center;">Eselon</th>
								</tr>
								<?php $i=1; foreach($this->m_home->getKarir($d->nip)->result() as $k){?>
								<tr style="font-weight:normal;">
									<td style="text-align: center;"><?=$i?></td>
									<td><?=$k->jabatan?></td>
									<td style="text-align: center;"><?=$k->eselon?></td>
								</tr>
								<? $i++; } ?>
							</table>
						</li>
					<? } ?>
					<?
							foreach($this->m_home->getSuratGub($no_notadinas,$p->id_kab_kota)->result() as $g){
							$nosuratgub=$g->no_surat_gub;
							$alasan=$g->alasan;
							$perihal=$g->perihal;
							$tglsuratgub=cariTanggal($g->tgl_surat_gub);
								}
							?>
							<ol class="a" style="font-weight:bold; margin-bottom:40px;">
								<li style="margin-bottom:10px; ">Pejabat yang mengusulkan : <br/><span style="font-weight:normal;">Gubernur <?=ucwords(strtolower($p->nm_provinsi))?> dengan Surat Nomor <?=$nosuratgub?> tanggal <?=ucwords(strtolower($tglsuratgub))?> perihal <?=$perihal?> <!--Penilaian Calon Sekda <$p->nm_kab_kota?-->.</span></li>
								<li style="margin-bottom:10px; ">Pertimbangan/alasan penggantian :  <br/><span style="font-weight:normal;"><?=$alasan?></span></li>
								<li style="margin-bottom:10px; ">Catatan  : <br/><span style="font-weight:normal;">
									
									<table style="font-size:12pt; ">
									<?php $o=1;foreach($this->m_home->getNilai($u->no_notadinas,$p->id_kab_kota)->result() as $c){ ?>
									<tr>
										<td width="20"><?=$o?>)</td>
										<td width="220"><b><?=$this->m_home->getNama($c->nip_calon)?></b></td>
										<td>Rangking ke <?if($o==1){echo "I";}else if($o==2){echo "II";}else{echo "III";}?></td>
										<td><?if($o==1){echo "(satu)";}else if($o==2){echo "(dua)";}else{echo "(tiga)";}?></td>
									</tr>
									<?$o++;}?>
									</table>
								</span></li>
								<li style="margin-bottom:10px; ">Keterangan :<br/><span style="font-weight:normal;">Urut Ranking I (satu) penilaian Calon Sekretaris Daerah <?=$p->nm_kab_kota?>, sesuai dengan usul dari Gubernur <?=ucwords(strtolower($p->nm_provinsi))?>.</span></li>
							</ol>
					</ol>
				</li>
		<?}?>
			</ol>
	</body>
</html>