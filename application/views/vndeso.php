<header>
	<h2>List Penjabat Kementrian Dalam Negeri</h2>
	</header>
		<section class="tab-content">
		
			<? if($this->session->flashdata('success')){ ?>
				<div class="alert alert-success">
					<button class="close" data-dismiss="alert">&times;</button >
					<strong>Sukses!</strong> <?=$this->session->flashdata('success')?>.
				</div>		
			<? } else if ($this->session->flashdata('error')){?>
				<div class="alert alert-error">
					<button class="close" data-dismiss="alert">&times;</button >
					<strong>Eror!</strong> <?=$this->session->flashdata('error')?>.
				</div>
			<?}?>
		<!-- Tab #basic -->
			<div class="tab-pane active" id="basic" style="min-height:500px">
				<table class="datatable table table-striped table-bordered table-hover" id="example">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama Penjabat</th>
							<th>Title</th>
							<th>Jabatan</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php $i=1; foreach($petugas as $p){
							
						?>
						<tr class="gradeA">
							<td><?=$i?></td>
							<td><?=$p->nama_petugas;?></td>
							<td><?=$p->title;?></td>
							<td><?=$p->jabatan;?></td>
							<td width="100"> <a class="btn btn-primary" data-toggle="modal" href="#demoModal" onclick='document.jab.jabatan.value="<?=$p->jabatan?>";document.jab.nama_penjabat.value="<?=$p->nama_petugas?>";document.jab.id_jabatan.value=<?=$p->id_petugas?>' rel="<?=$p->title?>">Ganti Penjabat</a></td>
						</tr>
						<?php $i++;} ?>
					</tbody>
				</table>
			</div>
		</section>
	<script src="<?php echo base_url();?>asset/js/plugins/dataTables/jquery.datatables.min.js"></script>
	<script>
			/* Default class modification */
			$.extend( $.fn.dataTableExt.oStdClasses, {
				"sWrapper": "dataTables_wrapper form-inline"
			} );
			
			/* API method to get paging information */
			$.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
			{
				return {
					"iStart":         oSettings._iDisplayStart,
					"iEnd":           oSettings.fnDisplayEnd(),
					"iLength":        oSettings._iDisplayLength,
					"iTotal":         oSettings.fnRecordsTotal(),
					"iFilteredTotal": oSettings.fnRecordsDisplay(),
					"iPage":          Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
					"iTotalPages":    Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
				};
			}
			
			/* Bootstrap style pagination control */
			$.extend( $.fn.dataTableExt.oPagination, {
				"bootstrap": {
					"fnInit": function( oSettings, nPaging, fnDraw ) {
						var oLang = oSettings.oLanguage.oPaginate;
						var fnClickHandler = function ( e ) {
							e.preventDefault();
							if ( oSettings.oApi._fnPageChange(oSettings, e.data.action) ) {
								fnDraw( oSettings );
							}
						};
						
						$(nPaging).addClass('pagination').append(
							'<ul>'+
								'<li class="prev disabled"><a href="#">&larr; '+oLang.sPrevious+'</a></li>'+
								'<li class="next disabled"><a href="#">'+oLang.sNext+' &rarr; </a></li>'+
							'</ul>'
						);
						var els = $('a', nPaging);
						$(els[0]).bind( 'click.DT', { action: "previous" }, fnClickHandler );
						$(els[1]).bind( 'click.DT', { action: "next" }, fnClickHandler );
					},
					
					"fnUpdate": function ( oSettings, fnDraw ) {
						var iListLength = 5;
						var oPaging = oSettings.oInstance.fnPagingInfo();
						var an = oSettings.aanFeatures.p;
						var i, j, sClass, iStart, iEnd, iHalf=Math.floor(iListLength/2);
						
						if ( oPaging.iTotalPages < iListLength) {
							iStart = 1;
							iEnd = oPaging.iTotalPages;
						}
						else if ( oPaging.iPage <= iHalf ) {
							iStart = 1;
							iEnd = iListLength;
						} else if ( oPaging.iPage >= (oPaging.iTotalPages-iHalf) ) {
							iStart = oPaging.iTotalPages - iListLength + 1;
							iEnd = oPaging.iTotalPages;
						} else {
							iStart = oPaging.iPage - iHalf + 1;
							iEnd = iStart + iListLength - 1;
						}
						
						for ( i=0, iLen=an.length ; i<iLen ; i++ ) {
							// Remove the middle elements
							$('li:gt(0)', an[i]).filter(':not(:last)').remove();
							
							// Add the new list items and their event handlers
							for ( j=iStart ; j<=iEnd ; j++ ) {
								sClass = (j==oPaging.iPage+1) ? 'class="active"' : '';
								$('<li '+sClass+'><a href="#">'+j+'</a></li>')
									.insertBefore( $('li:last', an[i])[0] )
									.bind('click', function (e) {
										e.preventDefault();
										oSettings._iDisplayStart = (parseInt($('a', this).text(),10)-1) * oPaging.iLength;
										fnDraw( oSettings );
									} );
							}
							
							// Add / remove disabled classes from the static elements
							if ( oPaging.iPage === 0 ) {
								$('li:first', an[i]).addClass('disabled');
							} else {
								$('li:first', an[i]).removeClass('disabled');
							}
							
							if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {
								$('li:last', an[i]).addClass('disabled');
							} else {
								$('li:last', an[i]).removeClass('disabled');
							}
						}
					}
				}
			});
			
			/* Show/hide table column */
			function dtShowHideCol( iCol ) {
				var oTable = $('#example-2').dataTable();
				var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
				oTable.fnSetColumnVis( iCol, bVis ? false : true );
			};
			
			/* Table #example */
			$(document).ready(function() {
				$('.datatable').dataTable( {
					"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
					"sPaginationType": "bootstrap",
					"oLanguage": {
						"sLengthMenu": "_MENU_ records per page"
					}
				});
				$('.datatable-controls').on('click','li input',function(){
					dtShowHideCol( $(this).val() );
				})
			});
			
			$(document).ready(function(){
						$('a[href="#demoModal"]').on( 'click', function(){
							var id = $(this).attr('rel');
							$('#select option[label="' + id + '"]').prop( 'selected', 'selected' );
						});
					});
		</script>

								<div class="modal fade hide" id="demoModal">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h3>Edit Data Penjabat</h3>
									</div>
							<form action="<?php echo site_url("chome/edit_penjabat");?>" method="POST" id="jab" name="jab">
									<div class="modal-body">
										<fieldset>
											<div class="control-group">
												<label class="control-label" for="disabledInput">Jabatan</label>
												<div class="controls">
													<input id="jabatan" class="input-xlarge" name="jabatannya" type="text" value="">
													<input id="id_jabatan" class="input-xlarge" name="id_jabatan" type="hidden" value="">
												</div>
											</div>
											<div class="control-group">
												<label class="control-label" for="input">Nama Penjabat</label>
												<div class="controls">
													<input id="nama_penjabat" name="nama_penjabat" class="input-xlarge" type="text">
												</div>
											</div>
											<div class="control-group">
												<label class="control-label" for="input">Title</label>
												<div class="controls">
													<select name="title" id="select">
														<option label="IBU" value="IBU">IBU</option>
														<option label="BAPAK" value="BAPAK">BAPAK</option>
														<option label="PLT BAPAK" value="PLT BAPAK">PLT BAPAK</option>
														<option label="PLT IBU" value="PLT IBU">PLT IBU</option>
													</select>
												</div>
											</div>
										</fieldset>
									</div>
									<div class="modal-footer">
										<a href="#" class="btn " data-dismiss="modal">Cancle</a>
										<button class="btn btn-large btn-primary" type="submit">Save changes</button>
									</div>
										</form>
								</div>
							