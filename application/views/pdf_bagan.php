<?php
function cariTanggal($tglnya){
	$t=$tglnya;
	$tg=explode("-", $t);
	$a=$tg[1];

	switch ($a) {
		case 1:
			$bulan="JANUARI";
			break;
		case 2:
			$bulan="FEBRUARI";
			break;
		case 3:
			$bulan="MARET";
			break;
		case 4:
			$bulan="APRIL";
			break;
		case 5:
			$bulan="MEI";
			break;
		case 6:
			$bulan="JUNI";
			break;
		case 7:
			$bulan="JULI";
			break;
		case 8:
			$bulan="AGUSTUS";
			break; 
		case 9:
			$bulan="SEPTEMBER";
			break;
		case 10:
			$bulan="OKTOBER";
			break;
		case 11:
			$bulan="NOVEMBER";
			break;
		case 12:
			$bulan="DESEMBER";
			break;
	}
	
	$tanggal2=$tg[0]." ".$bulan." ".$tg[2];
	return $tanggal2;
}

foreach($resume as $u){
	$tgl_rapat=$u->tgl_rapat;
	$no_notadinas=$u->no_notadinas;
	//$jum=$this->m_home->getKabDetail($u->no_notadinas)->num_rows();
	
}

?>

<html>
	<head>
		<style>
			.table { border:thin black solid; margin:2pt; border-width:thick;}
			body{
				font-family:Tahoma, Geneva, sans-serif; 
				font-size:12pt; 
				/*margin: 3em;*/
				margin:5px;
			}
			div {
			/* background: #EDEDED;
			   border: solid 1px #CCC;
			  width: 650px;*/
			  width: 690px;
			
			}

			.break-word {
			  word-wrap: break-word;
			}
		</style>
	</head>
	<body>
		<table width="100%" style="margin-bottom:25px; font-size:16pt; font-weight:bold; text-align: center;">	
			<tr>
				<td>REKAPITULASI</td>
			</tr>
			<tr>
				<td>PENILAIAN CALON SEKRETARIS DAERAH</td>
			</tr>
			<tr>	
				<td>KABUPATEN/KOTA</td>
			</tr>
			<tr>
				<td><?=cariTanggal($tgl_rapat)?></td>
			</tr>
		</table>
		<table width="100%" class="table">
			<tr>
				<th style="font-size:14pt; font-weight:bold;" width="50px">NO</th>
				<th style="font-size:14pt; font-weight:bold;">KABUPATEN/KOTA</th>
				<th style="font-size:14pt; font-weight:bold;" width="80px">SCORE</th>
			</tr>
			<tr>
				<th>1</th>
				<th>2</th>
				<th>3</th>
			</tr>
			<?php $i=1;	
				foreach($this->m_home->getAllDapil2($u->no_notadinas)->result() as $p){
			?>
			<tr>
				<td style=" text-align: center;"><?=$i?></td>
				<td>Calon Sekretaris Daerah Kabupaten <?=$p->nm_kab_kota?></td>
				<td>&nbsp;</td>
			</tr>
				<?php $o=1;foreach($this->m_home->getNilai($u->no_notadinas,$p->id_kab_kota)->result() as $c){ 

				?>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;&nbsp;<b><? echo $o.". ";?> <?=$this->m_home->getNama($c->nip_calon)?></b></td>
						<td style="text-align: left;"><?=$c->total_float?></td>
					</tr>
					
				<? $o++;} ?>
				<tr>
				<td colspan=3>&nbsp;</td>
				</tr>
				
			<? $i++;} ?>
			<?  ?>
		</table>
	</body>
</html>