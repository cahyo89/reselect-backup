<script src="<?php echo base_url();?>asset/js/jquery18.min"></script>
	<header>
	<h2>List Penilaian Sekertaris Daerah</h2>
	</header>
		<section class="tab-content">
							
		<!-- Tab #basic -->
			<div class="tab-pane active" id="basic" style="min-height:500px">
				<table class="datatable table table-striped table-bordered table-hover" id="example">
					<thead>
						<tr>
							<th>No</th>
							<th>Tgl Rapat</th>
							<th>No Surat Dinas</th>
							<th>Provinsi</th>
							<th>Kab / Kota</th>
							<th>Status</th>
							<th>Ket</th>
						</tr>
					</thead>
					<tbody>
						<?php $i=1; foreach($dapilnya as $d){
							$jmlAlasan=$this->m_home->getJmlAlasan($d->no_notadinas,$d->id_kab_kota)->num_rows();
							$jmlCalon=$this->m_home->cekJmlCalon2($d->no_notadinas,$d->id_kab_kota)->num_rows();
							$jmlInputNilai=$this->m_home->cekJmlNilai($d->no_notadinas,$d->id_kab_kota)->num_rows();
							$flag=$this->m_home->getFlagNilai3($d->no_notadinas,$d->id_kab_kota);
						?>
						<tr class="gradeA">
							<td><?=$i?></td>
							<td><?=$d->tgl_rapat;?></td>
							<td><?=$d->no_notadinas;?></td>
							<td><?=$d->nm_provinsi;?></td>
							<td><?=$d->nm_kab_kota;?></td>
							<td><?php if ($jmlCalon < 3){echo "<span class='label label-important'>Not Ready</span>";}else if($jmlInputNilai >=3){echo "<span class='label label-success'>Complete</span>";}else{echo "<span class='label label-success'>Ready</span>";}?></td>
							<td><?php if ($jmlCalon < 2){ echo "Silahkan input alasan pergantian <br/>dan biodata diri calon Sekda";}else if($jmlInputNilai >=3 && $flag==1){?> <a href="<?php echo base_url();?>index.php/chome/reviewpenilaian/<?=$d->id_detail_daerah;?>"><span class="btn btn-info"><i class="awe-check"></i> Review Penilaian</span></a> <?}else{?><a href="<?php echo base_url();?>index.php/chome/penilaian/<?=$d->id_detail_daerah;?>"><span class="btn"><i class="awe-edit"></i> Input Penilaian</span></a><?}?></td>
						</tr>
						<?php $i++;} ?>
					</tbody>
				</table>
			</div>
		</section>
	<script src="<?php echo base_url();?>asset/js/plugins/dataTables/jquery.datatables.min.js"></script>
	<script>
			/* Default class modification */
			$.extend( $.fn.dataTableExt.oStdClasses, {
				"sWrapper": "dataTables_wrapper form-inline"
			} );
			
			/* API method to get paging information */
			$.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
			{
				return {
					"iStart":         oSettings._iDisplayStart,
					"iEnd":           oSettings.fnDisplayEnd(),
					"iLength":        oSettings._iDisplayLength,
					"iTotal":         oSettings.fnRecordsTotal(),
					"iFilteredTotal": oSettings.fnRecordsDisplay(),
					"iPage":          Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
					"iTotalPages":    Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
				};
			}
			
			/* Bootstrap style pagination control */
			$.extend( $.fn.dataTableExt.oPagination, {
				"bootstrap": {
					"fnInit": function( oSettings, nPaging, fnDraw ) {
						var oLang = oSettings.oLanguage.oPaginate;
						var fnClickHandler = function ( e ) {
							e.preventDefault();
							if ( oSettings.oApi._fnPageChange(oSettings, e.data.action) ) {
								fnDraw( oSettings );
							}
						};
						
						$(nPaging).addClass('pagination').append(
							'<ul>'+
								'<li class="prev disabled"><a href="#">&larr; '+oLang.sPrevious+'</a></li>'+
								'<li class="next disabled"><a href="#">'+oLang.sNext+' &rarr; </a></li>'+
							'</ul>'
						);
						var els = $('a', nPaging);
						$(els[0]).bind( 'click.DT', { action: "previous" }, fnClickHandler );
						$(els[1]).bind( 'click.DT', { action: "next" }, fnClickHandler );
					},
					
					"fnUpdate": function ( oSettings, fnDraw ) {
						var iListLength = 5;
						var oPaging = oSettings.oInstance.fnPagingInfo();
						var an = oSettings.aanFeatures.p;
						var i, j, sClass, iStart, iEnd, iHalf=Math.floor(iListLength/2);
						
						if ( oPaging.iTotalPages < iListLength) {
							iStart = 1;
							iEnd = oPaging.iTotalPages;
						}
						else if ( oPaging.iPage <= iHalf ) {
							iStart = 1;
							iEnd = iListLength;
						} else if ( oPaging.iPage >= (oPaging.iTotalPages-iHalf) ) {
							iStart = oPaging.iTotalPages - iListLength + 1;
							iEnd = oPaging.iTotalPages;
						} else {
							iStart = oPaging.iPage - iHalf + 1;
							iEnd = iStart + iListLength - 1;
						}
						
						for ( i=0, iLen=an.length ; i<iLen ; i++ ) {
							// Remove the middle elements
							$('li:gt(0)', an[i]).filter(':not(:last)').remove();
							
							// Add the new list items and their event handlers
							for ( j=iStart ; j<=iEnd ; j++ ) {
								sClass = (j==oPaging.iPage+1) ? 'class="active"' : '';
								$('<li '+sClass+'><a href="#">'+j+'</a></li>')
									.insertBefore( $('li:last', an[i])[0] )
									.bind('click', function (e) {
										e.preventDefault();
										oSettings._iDisplayStart = (parseInt($('a', this).text(),10)-1) * oPaging.iLength;
										fnDraw( oSettings );
									} );
							}
							
							// Add / remove disabled classes from the static elements
							if ( oPaging.iPage === 0 ) {
								$('li:first', an[i]).addClass('disabled');
							} else {
								$('li:first', an[i]).removeClass('disabled');
							}
							
							if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {
								$('li:last', an[i]).addClass('disabled');
							} else {
								$('li:last', an[i]).removeClass('disabled');
							}
						}
					}
				}
			});
			
			/* Show/hide table column */
			function dtShowHideCol( iCol ) {
				var oTable = $('#example-2').dataTable();
				var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
				oTable.fnSetColumnVis( iCol, bVis ? false : true );
			};
			
			/* Table #example */
			$(document).ready(function() {
				$('.datatable').dataTable( {
					"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
					"sPaginationType": "bootstrap",
					"oLanguage": {
						"sLengthMenu": "_MENU_ records per page"
					}
				});
				$('.datatable-controls').on('click','li input',function(){
					dtShowHideCol( $(this).val() );
				})
			});
		</script>