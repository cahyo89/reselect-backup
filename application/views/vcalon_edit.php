
							<header>
								<h2>Form Edit Calon SEKDA</h2>
							</header>
							<section class="tab-content">
							
								<!-- Tab #basic -->
								<div class="tab-pane active" id="basic">
								
									<!-- Example horizontal forms -->
									<div class="row-fluid">
										<div class="span4">
											<p>Silahkan mengisi form berikut untuk Edit data diri calon sekertaris daerah</p>
										</div>
										<div class="span8">
											<form class="form-horizontal" action="<?php echo site_url("chome/edit_calon/".$this->uri->segment(3)."/".$calon->nip);?>" method="POST">
												<fieldset>
													<?php if($this->session->flashdata('success')){?>
													<div class="alert alert-success">
														<button class="close" data-dismiss="alert" type="button">&times;</button>
														<strong>Well done!</strong> <?php echo $this->session->flashdata('success');?>.
													</div>
													<?php } else if($this->session->flashdata('error')){?>
													<div class="alert alert-block">
														<button class="close" data-dismiss="alert" type="button">&times;</button>
														<strong>Warning!</strong>
														<p><?php echo $this->session->flashdata('error');?></p>
													</div>
													<?php }?>
													
														<input type="hidden" name="kab_kota" value="<?=$id_kab_kota?>"/>
														<input type="hidden" name="nosurat" value="<?=$no_notadinas;?>"/>
												
													<div class="control-group">
														<label class="control-label" for="input">NIP</label>
														<div class="controls">
															<input type="text"  class="input-xlarge"  value="<?=$calon->nip?>" disabled name="nip">
														</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="input">Nama Lengkap</label>
														<div class="controls">
															<input type="text"  class="input-xlarge"  value="<?=$calon->nama?>" name="nama">
														</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="input">Tanggal Lahir</label>
														<div class="controls">
															<div class="input-append">
																<input class="datepicker input-small" type="text"  name="tgllahir" value="<?=$calon->tgl_lahir?>"><span class="add-on"><i class="awe-calendar"></i></span>
															</div>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="input">Pangkat Calon</label>
														<div class="controls">
															<select id="pangkat"  name="pangkat" >
																	<option value="Pembina (IV/a)" <?php if($calon->pangkat=="Pembina (IV/a)"){echo"selected";}?>>Pembina (IV/a)</option>
																	<option value="Pembina Tingkat I (IV/b)" <?php if($calon->pangkat=="Pembina Tingkat I (IV/b)"){echo"selected";}?>>Pembina Tingkat I (IV/b)</option>
																	<option value="Pembina Utama Muda (IV/c)" <?php if($calon->pangkat=="Pembina Utama Muda (IV/c)"){echo"selected";}?>>Pembina Utama Muda (IV/c)</option>
																	<option value="Pembina Utama Madya (IV/d)" <?php if($calon->pangkat=="Pembina Utama Madya (IV/d)"){echo"selected";}?>>Pembina Utama Madya (IV/d)</option>
																	<option value="Pembina Utama (IV/e)" <?php if($calon->pangkat=="Pembina Utama (IV/e)"){echo"selected";}?>>Pembina Utama (IV/e)</option>
																	
															</select>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="input">TMT</label>
														<div class="controls">
															<div class="input-append">
																<input class="input-small datepicker2"  type="text"  name="tmt" value="<?=$calon->tmt?>"><span class="add-on"><i class="awe-calendar"></i></span>
															</div>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="input">Pendidikan Umum</label>
															<div class="controls">
																<input type="text"  class="input-xlarge" id="input" name="pend_umum" value="<?=$calon->pend_umum?>"/>
																<p class="help-block">Contoh : Pasca Sarjana, Tahun 2001</p>
															</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="input">Pendidikan Struktural</label>
															<div class="controls">
																<input type="text"  class="input-xlarge" id="input" name="pend_struk" value="<?=$calon->pend_struktural?>"/>
																<p class="help-block">Contoh : Diklatpim Tk. III, Tahun 2006</p>
															</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="input">Jabatan</label>
															<div class="controls">
																<input type="text"  class="input-xlarge" id="input" name="jabatannya" value="<?=$calon->jabatan?>"/>
																<p class="help-block">Contoh : Staf Ahli Bupati Tambrauw Bidang Pengawasan Teknik</p>
															</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="input">Perjalanan Karir</label>
															<div class="controls">
																<a href="#" id="addNew"><span class="awe-plus-sign"></span> Add</a>
																<div id="addinput">
																<?php $p=1;
																	foreach($karirnya as $k){?>
																<p>
																<input type="text" id="p_new<?=$p?>" class="input-xlarge" name="jabatan[]" value="<?=$k->jabatan?>" placeholder="Jabatan" /> 
																<select id="p_new2<?=$p?>" style="width: 50px;" name="eselon[]" >
																	<option <?php if($k->eselon=="V.a"){echo"selected";}?> value="V.a">V.a</option>
																	<option <?php if($k->eselon=="IV.b"){echo"selected";}?> value="IV.b">IV.b</option>
																	<option <?php if($k->eselon=="IV.a"){echo"selected";}?> value="IV.a">IV.a</option>
																	<option <?php if($k->eselon=="III.b"){echo"selected";}?> value="III.b">III.b</option>
																	<option <?php if($k->eselon=="III.a"){echo"selected";}?> value="III.a">III.a</option>
																	<option <?php if($k->eselon=="II.b"){echo"selected";}?> value="II.b">II.b</option>
																	<option <?php if($k->eselon=="II.a"){echo"selected";}?> value="II.a">II.a</option>
																	<option <?php if($k->eselon=="I.b"){echo"selected";}?> value="I.b">I.b</option>
																	<option <?php if($k->eselon=="I.a"){echo"selected";}?> value="I.a">I.a</option>
																</select>
																 <a href="#" class="awe-remove" id="remNew"></a> 
																</p>
																<?php $p++;} ?>
																</div>
															</div>
													</div>
													
													<div class="form-actions">
														<button class="btn btn-primary btn-large" type="submit">Save changes</button>
														
													</div>
												</fieldset>
											</form>
										</div>
									</div>
									
								</div>
								
							</section>
							<footer class="info">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam blandit, dolor mollis adipiscing elementum, ipsum turpis euismod tellus, vitae mollis velit leo id nisi.</p>
							</footer>
						</div>
					</article>
<script src="<?php echo base_url();?>asset/js/jquery.min.js"></script>
<link href="<?php echo base_url();?>asset/css/multi-select.css" media="screen" rel="stylesheet" type="text/css">
<script src="<?php echo base_url();?>asset/js/jquery.multi-select.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>asset/js/jquery.quicksearch.js" type="text/javascript"></script>
	<script>
		$('#searchable').multiSelect({
	  selectableHeader: "<input type='text' id='search' autocomplete='off' class='input-medium' placeholder='try \"kota\"'>"
	});

	$('#search').quicksearch($('.ms-elem-selectable', '#ms-searchable' )).on('keydown', function(e){
	  if (e.keyCode == 40){
		$(this).trigger('focusout');
		$('#searchable').focus();
		return false;
	  }
	});
	</script>
		<script type="text/javascript">
			$(function() {
				var addDiv = $('#addinput');
				var i = $('#addinput p').size() + 1;

				$('#addNew').live('click', function() {
				$('<p><input type="text" id="p_new'+i+'"  class="input-xlarge" name="jabatan[]" value="" placeholder="Jabatan" /><select id="p_new2'+i+'" style="width: 50px;" name="eselon[]" ><option value="V.a">V.a</option><option value="IV.b">IV.b</option><option value="IV.a">IV.a</option><option value="III.b">III.b</option><option value="III.a">III.a</option><option value="II.b">II.b</option><option value="II.a">II.a</option><option value="I.b">I.b</option><option value="I.a">I.a</option></select> <a href="#" class="awe-remove" id="remNew"></a> </p>').fadeIn("slow").appendTo(addDiv);
				i++;

				return false;
				});

				$('#remNew').live('click', function() {
					if( i > 0 ) {
						$(this).parents('p').remove();
						i--;
					}
					return false;
				});
			});

		</script>