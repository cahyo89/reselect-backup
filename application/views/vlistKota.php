
							<header>
								<h2>Kelola Data Kab/Kota</h2>
								<ul class="data-header-actions">
													<li class="demoTabs active">
													<a href="#demoModal" class="btn btn-inverse" data-toggle="modal"><span class="awe-plus-sign"></span>Tambah Kab/Kota</a>
													</li>
													</ul>
							</header>
							<section class="tab-content">
							
								<!-- Tab #basic -->
								<div class="tab-pane active" id="basic">
								
									<!-- Example horizontal forms -->
									<div class="row-fluid">
										
										<div class="span12">
											
												<fieldset>
													<?php if($this->session->flashdata('success')){?>
													<div class="alert alert-success">
														<button class="close" data-dismiss="alert" type="button">&times;</button>
														<strong>Well done!</strong> <?php echo $this->session->flashdata('success');?>.
													</div>
													<?php } else if($this->session->flashdata('error')){?>
													<div class="alert alert-block">
														<button class="close" data-dismiss="alert" type="button">&times;</button>
														<strong>Warning!</strong>
														<p><?php echo $this->session->flashdata('error');?></p>
													</div>
													<?php }?>
													
														
									<div class="modal fade hide" id="demoModal">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">×</button>
										<h3>Tambah Kab/Kota Baru</h3>
									</div>
									<div class="modal-body">
										<form class="form-horizontal" action="<?php echo site_url("chome/add_kab_kota");?>" method="POST">
											<fieldset>
												<div class="control-group">
														<label class="control-label" for="input">Nama Kab/Kota</label>
															<div class="controls">
																<input type="text"  class="input-xlarge" id="input" name="nm_kab_kota" placeholder="Nama Provinsi ..."/>
																
															</div>
												</div>
												<div class="control-group">
														<label class="control-label" for="input"> Provinsi</label>
														<div class="controls">
															<select id="select" name="provnya">
																<option>Pilih Provinsi</option>
																<?php foreach($provnya as $p){?>
																	<option value="<?=$p->id_provinsi?>" ><?=$p->nm_provinsi?></option>
																<?php } ?>
															</select>
														</div>
									
												</div>
											</fieldset>
									</div>
									<div class="modal-footer">
										<a href="#" class="btn btn-alt" data-dismiss="modal">Cancle</a>
										<button class="btn btn-alt btn-large btn-primary" type="submit">Save changes</button>
									</div>
										</form>
								</div>
																										
													
															
									<h3>List Kab/Kota</h3>
									<table class="datatable table table-striped table-bordered table-hover" id="example">
										<thead>
											<tr>
												<th width="50px">No</th>
												<th>Nama Kab/Kota</th>
												<th>Nama Provinsi</th>
												<th width="100px">Aksi</th>
											</tr>
										</thead>
										<tbody>
										<?php $no=1; foreach ($kotanya as $k){?>
											<tr class="odd gradeX">
												<td><?=$no?></td>
												<td><?=$k->nm_kab_kota?></td>
												<td><?=$k->nm_provinsi?></td>
												<td>
													<div class="btn-group">
														<a class="btn" title="Edit <?=$k->nm_kab_kota?>"  href="#demoModal2" rel="<?=$k->nm_provinsi?>" data-toggle="modal"  onclick="document.kate.id_kab_kota.value='<?=$k->id_kab_kota?>';document.kate.nm_kab_kota.value='<?=$k->nm_kab_kota?>';"><span class="awe-wrench"></span></a>
														<a onclick='return window.confirm("Anda yakin menghapus data ini ?");' href="<?php echo base_url();?>index.php/chome/delete_kab_kota/<?=$k->id_kab_kota?>" class="btn" title="Hapus  <?=$k->nm_kab_kota?>"><span class="awe-remove" ></span></a>
													</div>
												</td>
											</tr>
										<?php 
											$no++;
										} ?>
										</tbody>
									</table>
													
													
													</div>
												</fieldset>
										</div>
									</div>
									
								</div>
								
							</section>
							
						</div>
					</article>
					
					
					
								<div class="modal fade hide" id="demoModal2">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h3>Edit Nama Kab/Kota</h3>
									</div>
									<div class="modal-body">
									<form action="<?php echo site_url("chome/edit_kab_kota");?>" method="POST" id="kate" name="kate">
										<fieldset>
											<div class="control-group">
														<label class="control-label" for="input">Nama Kab/Kota</label>
															<div class="controls">
															<input id="id_kab_kota" class="input-xlarge" name="id_kab_kota" type="hidden" value="">
																<input type="text"  class="input-xlarge" id="nm_kab_kota" name="nm_kab_kota" value=""/>
																
															</div>
												</div>
												<div class="control-group">
														<label class="control-label" for="input"> Provinsi</label>
														<div class="controls">
															<select id="select" name="provnya">
																<option>Pilih Provinsi</option>
																<?php foreach($provnya as $p){?>
																	<option label="<?=$p->nm_provinsi?>" value="<?=$p->id_provinsi?>" ><?=$p->nm_provinsi?></option>
																<?php } ?>
															</select>
														</div>
									
												</div>
										</fieldset>
									</div>
									<div class="modal-footer">
										<a href="#" class="btn " data-dismiss="modal">Cancle</a>
										<button onclick='return window.confirm("Anda yakin melakukan perubahan data ini ?");' class="btn btn-large btn-primary" type="submit">Save changes</button>
									</div>
								</form>
								</div>
							
<script src="<?php echo base_url();?>asset/js/jquery.min.js"></script>
<link href="<?php echo base_url();?>asset/css/multi-select.css" media="screen" rel="stylesheet" type="text/css">
<script src="<?php echo base_url();?>asset/js/jquery.multi-select.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>asset/js/jquery.quicksearch.js" type="text/javascript"></script>
	<script>
		$('#searchable').multiSelect({
	  selectableHeader: "<input type='text' id='search' autocomplete='off' class='input-medium' placeholder='try \"kota\"'>"
	});

	$('#search').quicksearch($('.ms-elem-selectable', '#ms-searchable' )).on('keydown', function(e){
	  if (e.keyCode == 40){
		$(this).trigger('focusout');
		$('#searchable').focus();
		return false;
	  }
	});
	</script>
	<script src="<?php echo base_url();?>asset/js/plugins/dataTables/jquery.datatables.min.js"></script>
	<script>
			/* Default class modification */
			$.extend( $.fn.dataTableExt.oStdClasses, {
				"sWrapper": "dataTables_wrapper form-inline"
			} );
			
			/* API method to get paging information */
			$.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
			{
				return {
					"iStart":         oSettings._iDisplayStart,
					"iEnd":           oSettings.fnDisplayEnd(),
					"iLength":        oSettings._iDisplayLength,
					"iTotal":         oSettings.fnRecordsTotal(),
					"iFilteredTotal": oSettings.fnRecordsDisplay(),
					"iPage":          Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
					"iTotalPages":    Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
				};
			}
			
			/* Bootstrap style pagination control */
			$.extend( $.fn.dataTableExt.oPagination, {
				"bootstrap": {
					"fnInit": function( oSettings, nPaging, fnDraw ) {
						var oLang = oSettings.oLanguage.oPaginate;
						var fnClickHandler = function ( e ) {
							e.preventDefault();
							if ( oSettings.oApi._fnPageChange(oSettings, e.data.action) ) {
								fnDraw( oSettings );
							}
						};
						
						$(nPaging).addClass('pagination').append(
							'<ul>'+
								'<li class="prev disabled"><a href="#">&larr; '+oLang.sPrevious+'</a></li>'+
								'<li class="next disabled"><a href="#">'+oLang.sNext+' &rarr; </a></li>'+
							'</ul>'
						);
						var els = $('a', nPaging);
						$(els[0]).bind( 'click.DT', { action: "previous" }, fnClickHandler );
						$(els[1]).bind( 'click.DT', { action: "next" }, fnClickHandler );
					},
					
					"fnUpdate": function ( oSettings, fnDraw ) {
						var iListLength = 5;
						var oPaging = oSettings.oInstance.fnPagingInfo();
						var an = oSettings.aanFeatures.p;
						var i, j, sClass, iStart, iEnd, iHalf=Math.floor(iListLength/2);
						
						if ( oPaging.iTotalPages < iListLength) {
							iStart = 1;
							iEnd = oPaging.iTotalPages;
						}
						else if ( oPaging.iPage <= iHalf ) {
							iStart = 1;
							iEnd = iListLength;
						} else if ( oPaging.iPage >= (oPaging.iTotalPages-iHalf) ) {
							iStart = oPaging.iTotalPages - iListLength + 1;
							iEnd = oPaging.iTotalPages;
						} else {
							iStart = oPaging.iPage - iHalf + 1;
							iEnd = iStart + iListLength - 1;
						}
						
						for ( i=0, iLen=an.length ; i<iLen ; i++ ) {
							// Remove the middle elements
							$('li:gt(0)', an[i]).filter(':not(:last)').remove();
							
							// Add the new list items and their event handlers
							for ( j=iStart ; j<=iEnd ; j++ ) {
								sClass = (j==oPaging.iPage+1) ? 'class="active"' : '';
								$('<li '+sClass+'><a href="#">'+j+'</a></li>')
									.insertBefore( $('li:last', an[i])[0] )
									.bind('click', function (e) {
										e.preventDefault();
										oSettings._iDisplayStart = (parseInt($('a', this).text(),10)-1) * oPaging.iLength;
										fnDraw( oSettings );
									} );
							}
							
							// Add / remove disabled classes from the static elements
							if ( oPaging.iPage === 0 ) {
								$('li:first', an[i]).addClass('disabled');
							} else {
								$('li:first', an[i]).removeClass('disabled');
							}
							
							if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {
								$('li:last', an[i]).addClass('disabled');
							} else {
								$('li:last', an[i]).removeClass('disabled');
							}
						}
					}
				}
			});
			
			/* Show/hide table column */
			function dtShowHideCol( iCol ) {
				var oTable = $('#example-2').dataTable();
				var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
				oTable.fnSetColumnVis( iCol, bVis ? false : true );
			};
			
			/* Table #example */
			$(document).ready(function() {
				$('.datatable').dataTable( {
					"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
					"sPaginationType": "bootstrap",
					"oLanguage": {
						"sLengthMenu": "_MENU_ records per page"
					}
				});
				$('.datatable-controls').on('click','li input',function(){
					dtShowHideCol( $(this).val() );
				})
			});
			
				$(document).ready(function(){
						$('a[href="#demoModal2"]').on( 'click', function(){
							var id = $(this).attr('rel');
							$('#select option[label="' + id + '"]').prop( 'selected', 'selected' );
						});
					});
		</script>
		<script type="text/javascript" src="<?php echo base_url();?>asset/js/plugins/jGrowl/jquery.jgrowl.js"></script>
		<?php if (($this->session->flashdata('success')) || ($this->session->flashdata('error'))){?>
		<script>
			$(document).ready(function(){
				
				// This value can be true, false or a function to be used as a callback when the closer is clciked
				
				<?php if($this->session->flashdata('success')){?>
				$.jGrowl("<?=$this->session->flashdata('success')?>", { 
					theme: 'success'
				});
				<?php }else if($this->session->flashdata('error')){?>
				$.jGrowl("<?=$this->session->flashdata('error')?>", {
					life: 2500,
					theme: 'danger'
				});
				<?php }?>
				});
				
			
		</script>
				<?php }?>
		