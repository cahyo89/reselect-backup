<?php
	function cariTanggal($tglnya){
	$t=$tglnya;
	$tg=explode("-", $t);
	$a=$tg[1];

	switch ($a) {
		case 1:
			$bulan="JANUARI";
			break;
		case 2:
			$bulan="FEBRUARI";
			break;
		case 3:
			$bulan="MARET";
			break;
		case 4:
			$bulan="APRIL";
			break;
		case 5:
			$bulan="MEI";
			break;
		case 6:
			$bulan="JUNI";
			break;
		case 7:
			$bulan="JULI";
			break;
		case 8:
			$bulan="AGUSTUS";
			break; 
		case 9:
			$bulan="SEPTEMBER";
			break;
		case 10:
			$bulan="OKTOBER";
			break;
		case 11:
			$bulan="NOVEMBER";
			break;
		case 12:
			$bulan="DESEMBER";
			break;
	}
	
	$tanggal2=number_format($tg[0])." ".$bulan." ".$tg[2];
	return $tanggal2;
}

foreach($resume as $u){
	$tgl_rapat=$u->tgl_rapat;
	$no_notadinas=$u->no_notadinas;
	//$jum=$this->m_home->getKabDetail($u->no_notadinas)->num_rows();
	
}
$jum=$this->m_home->getKabDetail($no_notadinas)->num_rows();
	$i=1;
	$kota="";
	foreach($this->m_home->getKabDetail($no_notadinas)->result() as $s){
		if($i<$jum){
		$kota=$kota."".$s->nm_kab_kota.', ';
		}else{
		$kota=$kota." dan ".$s->nm_kab_kota;
		}
		$i++;
	}
	
	function cariHari($tglnya){
	$t=$tglnya;
	$namahari = date('l', strtotime($t));
	
	if ($namahari == "Sunday") $namahari = "Minggu";
	else if ($namahari == "Monday") $namahari = "Senin";
	else if ($namahari == "Tuesday") $namahari = "Selasa";
	else if ($namahari == "Wednesday") $namahari = "Rabu";
	else if ($namahari == "Thursday") $namahari = "Kamis";
	else if ($namahari == "Friday") $namahari = "Jumat";
	else if ($namahari == "Saturday") $namahari = "Sabtu";
	return $namahari;
}
function Terbilang($satuan){  
$huruf = array ("", "satu", "dua", "tiga", "empat", "lima", "enam",   
"tujuh", "delapan", "sembilan", "sepuluh","sebelas");  
if ($satuan < 12)  
 return " ".$huruf[$satuan];  
elseif ($satuan < 20)  
 return Terbilang($satuan - 10)." belas";  
elseif ($satuan < 100)  
 return Terbilang($satuan / 10)." puluh".  
 Terbilang($satuan % 10);  
elseif ($satuan < 200)  
 return "seratus".Terbilang($satuan - 100);  
elseif ($satuan < 1000)  
 return Terbilang($satuan / 100)." ratus".  
 Terbilang($satuan % 100);  
elseif ($satuan < 2000)  
 return "seribu".Terbilang($satuan - 1000);   
elseif ($satuan < 1000000)  
 return Terbilang($satuan / 1000)." ribu".  
 Terbilang($satuan % 1000);   
elseif ($satuan < 1000000000)  
 return Terbilang($satuan / 1000000)." juta".  
 Terbilang($satuan % 1000000);   
elseif ($satuan >= 1000000000)  
 echo "Angka terlalu Besar";  
}  

	$t=$tgl_rapat;
	$tg=explode("-", $t);

	
	$a=Terbilang(number_format($tg[0]));
	$b=Terbilang(number_format($tg[1]));
	$c=Terbilang($tg[2]);
	
	$harian=cariHari($t)." tanggal ".$a." bulan ".$b." ".$c;
?>
<html>
	<head>
		<style>

			body{
				font-family:Tahoma, Geneva, sans-serif; 
				font-size:12pt; 
				/*margin: 3em;*/
				margin:5px;
			}
			div {
			/* background: #EDEDED;
			   border: solid 1px #CCC;
			  width: 650px;*/
			  width: 690px;
			
			}

			.break-word {
			  word-wrap: break-word;
			}
		</style>
	</head>
	<body>
		<div style=" text-align: right; float:left;">
			<div style="font-size:13px; margin:0px 0px 0px 625px; border:0.2px solid; padding:5px 15px 5px 20px; width:50px"><b>RAHASIA</b></div>
		</div>
		<table width="100%" style="margin-bottom:35px">
			<tr>
				<td style="font-size:11px;  text-align: right;">&nbsp;</td>
			</tr>
			<tr >
				<td style="font-size:14pt;  text-align: center;"><img src="<?php echo base_url();?>asset/img/garuda.png" width="80px"/></td>
			</tr>
			<tr>
				<td style="font-size:10pt;  text-align: center;"><b>KEMENTERIAN DALAM NEGERI</b></td>
			</tr>
			<tr>
				<td style="font-size:10pt;  text-align: center;"><b>REPUBLIK INDONESIA</b></td>
			</tr>
			<tr>
				<td style="font-size:10pt;  text-align: center;"><b>&nbsp;</b></td>
			</tr>
			<tr>
				<td style="font-size:10pt;  text-align: center;"><b>&nbsp;</b></td>
			</tr>
			<tr>
				<td style="font-size:12pt;  text-align: center;"><b>BERITA ACARA RAPAT</b></td>
			</tr>
			<tr>
				<td style="font-size:12pt;  text-align: center;"><b>PENILAIAN CALON SEKRETARIS DAERAH KABUPATEN/KOTA</b></td>
			</tr>
		</table>
		<div class="break-word" style="text-align: justify;">
			<p><span style="margin-right:60px">&nbsp;</span>Pada hari ini <?=$harian?> Tim Penilai Kementerian Dalam Negeri, sesuai surat undangan Sekretaris Jenderal Nomor <?=$no_notadinas?> tanggal <?=cariTanggal($tgl_rapat)?>, telah melaksanakan rapat penilaian persyaratan administrasi dan penilaian wawasan kebangsaan terhadap usulan calon Sekretaris Daerah Kabupaten/Kota, yaitu : <?=$kota?>.</p>
			<p><span style="margin-right:60px">&nbsp;</span>Sesuai Pasal 122 ayat (1), (2) dan (3) Undang-Undang Nomor 32 Tahun 2004 tentang Pemerintahan Daerah dan Peraturan Menteri Dalam Negeri Nomor 5 Tahun 2005 tentang Pedoman Penilaian Calon Sekretaris Daerah Provinsi dan Kabupaten/Kota serta Pejabat Struktural Eselon II di lingkungan Kabupaten/Kota, telah dilakukan penilaian yang obyektif terhadap Pegawai Negeri Sipil calon Sekretaris Daerah Kabupaten/Kota, meliputi : </p>
			<p>
				<ol>
		<?php foreach($this->m_home->getAllDapil2($no_notadinas)->result() as $p){?>
				<li style="font-weight:bold;margin-bottom:10px;">Calon  Sekretaris  Daerah <?=$p->nm_kab_kota?> Provinsi <?=ucwords(strtolower($p->nm_provinsi))?> <br/>
					<ol>
						<?php foreach($this->m_home->getDetailListSekda($no_notadinas,$p->id_kab_kota)->result() as $d){?>
						<li style="margin-bottom:10px; ">
							<table style="margin-bottom:1px; font-size:12pt; font-weight:normal;" width="100%">
								<tr>
									<td width="130px" >Nama</td>
									<td style="font-weight:bold;">: <?=$d->nama?></td>
								</tr>
								<tr>
									<td>NIP</td>
									<td>: <?=$d->nip?></td>
								</tr>
								<tr>
									<td>Tanggal Lahir</td>
									<td>: <?=$d->tgl_lahir?></td>
								</tr>
								<tr>
									<td>Pangkat/Gol. Ruang</td>
									<td>: <?=$d->pangkat?></td>
								</tr>
								<tr>
									<td>TMT</td>
									<td>: <?=$d->tmt?></td>
								</tr>
								<tr>
									<td>Pendidikan</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>- Umum</td>
									<td>: <?=$d->pend_umum?></td>
								</tr>
								<tr>
									<td>- Struktural</td>
									<td>: <?=$d->pend_struktural?></td>
								</tr>
								<tr>
									<td>Jabatan</td>
									<td>: <?=$d->jabatan?></td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td>: <?=$this->m_home->getSekor($no_notadinas,$p->id_kab_kota,$d->nip)?></td>
								</tr>
							</table>
						</li>
						<?}?>
						
					</ol>
				</li>
				<span style="margin-bottom:35px; ">
				Berdasarkan hasil penilaian tersebut, Tim Penilai memutuskan nilai yang diperoleh  dari Calon Sekretaris Daerah <?=$p->nm_kab_kota?>, dengan nilai tertinggi  adalah :
						<?$nipnya=$this->m_home->getCalonSkorTinggi($no_notadinas,$p->id_kab_kota);?>
						<? foreach($this->m_home->getDetailListSekda2($nipnya)->result() as $m){?>
							<table style="margin-bottom:15px; font-size:12pt; font-weight:normal;" width="100%">
								<tr>
									<td width="130px" >Nama</td>
									<td width="5px" >:</td>
									<td style="font-weight:bold;"> <?=$m->nama?></td>
								</tr>
								<tr>
									<td>NIP</td>
									<td>:</td>
									<td><?=$m->nip?></td>
								</tr>
								<tr>
									<td>Tanggal Lahir</td>
									<td>:</td>
									<td><?=$m->tgl_lahir?></td>
								</tr>
								<tr>
									<td>Pangkat/Gol. Ruang</td>
									<td>:</td>
									<td><?=$m->pangkat?></td>
								</tr>
								<tr>
									<td>TMT</td>
									<td>:</td>
									<td><?=$m->tmt?></td>
								</tr>
								<tr>
									<td>Pendidikan</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>- Umum</td>
									<td>:</td>
									<td><?=$m->pend_umum?></td>
								</tr>
								<tr>
									<td>- Struktural</td>
									<td>:</td>
									<td><?=$m->pend_struktural?></td>
								</tr>
								<tr>
									<td>Jabatan</td>
									<td>:</td>
									<td><?=$m->jabatan?></td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td>:</td>
									<td><?=$this->m_home->getSekor($no_notadinas,$p->id_kab_kota,$m->nip)?></td>
								</tr>
							</table>
						<?}?>
					</span>
			<?}?>
			</ol>
			</p>
			<p>Selanjutnya calon Sekretaris Daerah Kabupaten/Kota dengan nilai tertinggi dapat disetujui/direkomendasikan untuk dipertimbangkan penetapan pengangkatannya sebagai Sekretaris Daerah <?=$kota?>.</p>
			<p><span style="margin-right:60px">&nbsp;</span>Demikian Berita Acara Rapat Tim Penilai Calon Sekretaris Daerah Kabupaten/Kota.
			
			<br/><br/><br/>
			<table align="right">
				<tr>
					<td style="text-align: center; font-size:12pt;"><?php echo strtoupper($this->m_home->getPenjabat3(2)); ?></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td style="text-align: center; font-size:12pt;"><?=$this->m_home->getPenjabat2(2)?></td>
				</tr>
			</table>
			</p>
		</div>
	</body>
</html>