<div class="hero-unit">
	<h1>Selamat Datang</h1>
	<p>Aplikasi ini bernama "RESELECT" (Regional Secretary Election Apps) atau aplikasi pemilihan sekertaris daerah yang dimiliki oleh Kementrian Dalam Negeri Republik Indonesia. Untuk menggunakan aplikasi ini silahkan memahami panduan pengguanaan dengan cara meng-klik link dibawah ini.</p>
	<p><a class="btn btn-primary btn-large">Panduan Penggunaan</a></p>
</div>


<div class="row">
				
	<!-- Example jQuery FullCalendar -->
	<!-- Data block -->
	<article class="span12 data-block">
		<div class="data-container">
			<header>
				<h2>Kalendar Penilaian Sekertaris Daerah</h2>
			</header>
			<section class="tab-content">
				<div class="tab-pane active" id="basic">
					<div class='full-calendar full-calendar-demo'></div>
				</div>
			</section>
		</div>
	</article>
</div>

<script src="<?php echo base_url();?>asset/js/plugins/fullCalendar/jquery.fullcalendar.min.js"></script>

		<script>
			$(document).ready(function() {
			
				var date = new Date();
				var d = date.getDate();
				var m = date.getMonth();
				var y = date.getFullYear();
				
				$('.full-calendar-demo').fullCalendar({
					header: {
						left: 'title',
						center: '',
						right: 'today month,basicWeek prev,next'
					},
					buttonText: {
						prev: '<span class="awe-circle-arrow-left"></span>',
						next: '<span class="awe-circle-arrow-right"></span>'
					},
					editable: true,
					events: [
						
					<? $q=1;
						$jm=$this->m_home->getAllUndangan()->num_rows();
						foreach($opo as $p){
							$jum=$this->m_home->getKabDetail($p->no_notadinas)->num_rows();
							$i=1;
							$kota="";
							foreach($this->m_home->getKabDetail($p->no_notadinas)->result() as $s){
								if($i<$jum){
								$kota=$kota."".$s->nm_kab_kota.', ';
								}else{
								$kota=$kota." dan ".$s->nm_kab_kota;
								}
								$i++;
							}
						$a=explode("-", $p->tgl_rapat);
						
						if($q<$jm){
					?>
						{
							title: 'Penilaian Sekda <?=$kota?>, pukul <?=$p->jam_rapat?>',
							start: new Date(<?=$a[2]?>, <?=number_format($a[1])?>, <?=number_format($a[0])?>)
							
						},
						<?}else{?>
						{
							title: 'Penilaian Sekda <?=$kota?>, pukul <?=$p->jam_rapat?>',
							start: new Date(<?=$a[2]?>, <?=number_format($a[1])?>-1, <?=number_format($a[0])?>)
						}
					<? } $q++;}?>
					]
				});
				
				$('.full-calendar-gcal').fullCalendar({
					header: {
						left: 'title',
						center: '',
						right: 'today month,basicWeek prev,next'
					},
					buttonText: {
						prev: '<span class="awe-arrow-left"></span>',
						next: '<span class="awe-arrow-right"></span>'
					},
					events: {
						url: 'http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic',
						className: 'gcal-event', // an option!
						currentTimezone: 'America/Chicago' // an option!
					}
				});
				
			});
		</script>
		