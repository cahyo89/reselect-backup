
	

		<!-- Main page container -->
	
		
		
			
				<!-- Page header -->
				<article class="page-header">
					<h1>Review Hasil Penilaian Sekertaris daerah <?=$namakota?></h1>
				</article>
				<!-- /Page header -->
				
				<!-- Grid row -->
				<div class="row">
				
					<!-- Example jQuery Visualize -->
					<!-- Data block -->
					<article class="span12 data-block">
						<div class="data-container">
							<header>
								<h2>Statistik Penilaian Calon Sekertaris Daerah</h2>
								<ul class="data-header-actions">
									<li>
									<a class="btn btn-warning" href="<?php echo base_url();?>index.php/chome/downloadExcel/<?=$this->uri->segment(3);?>"> <span class="fam-page-excel"></span> Export to excel</a>
									</li>
								</ul>
							</header>
							<section>
								<table id="table" data-chart="line">
									<caption>Employee Sales by Department</caption>
									<thead>
										<tr>
											<th scope="col">A</th>
											<th scope="col">B</th>
											<th scope="col">C</th>
											<th scope="col">D</th>
											<th scope="col">E</th>
											<th scope="col">F</th>
											<th scope="col">G</th>
											<th scope="col">H</th>
											<th scope="col">I</th>
											<th scope="col">J</th>
											<th scope="col">K</th>
											<th scope="col">L</th>
											<th scope="col">M</th>
											<th scope="col">N</th>
											<th scope="col">O</th>
											<th scope="col">P</th>
											<th scope="col">Q</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($datanya as $d){ ?>
										<tr>
											<th scope="row"><?=$this->m_home->getNama($d->nip_calon);?></th>
											<td><?=$d->kepangkatan?></td>
											<td><?=$d->diklat_pim?></td>
											<td><?=$d->pendidikan?></td>
											<td><?=$d->riwayat_jabatan?></td>
											<td><?=$d->diklat_teknis?></td>
											<td><?=$d->diklat_fungsional?></td>
											<td><?=$d->duk?></td>
											<td><?=$d->dp3?></td>
											<td><?=$d->disiplin?></td>
											<td><?=$d->karir?></td>
											<td><?=$d->keragaman_diklat?></td>
											<td><?=$d->seminar?></td>
											<td><?=$d->pokir_strategis?></td>
											<td><?=$d->sikap_nkri?></td>
											<td><?=$d->pandangan_otda?></td>
											<td><?=$d->visi_misi?></td>
											<td><?=$d->koordinasi_komunikasi?></td>
										</tr>
										<? } ?>
									</tbody>
								</table>
								
							</section>
							<h3>note:</h3> A (Kepangkatan), B (Diklat Pim), C (Pendidikan), D (Riwayat Jabatan), E (Diklat Teknis), F (Diklat Fungsional), G (D U K), H (DP3), I (Disiplin), J (Perjalanan Karir), K (Keragaman Diklat), L (Seminar), M (Pokir Strategis), N (Penyikapan Teritorial & NKRI), O (Pandangan Otda), P (Visi & Misi), Q (Koordinasi & Komunikasi)<br/>
						</div>
					</article>
					<!-- /Data block -->
					
				</div>
				
				<div class="row">
				
				<?php foreach($datanya as $d){ ?>
					<article class="span4 data-block">
						<div class="data-container">
							<header>
								<h2><?=$this->m_home->getNama($d->nip_calon);?></h2>
							</header>
							<section>
								<table id="opo" class="table table-striped table-hover">
									<tr><td>Kepangkatan </td><td>: <?=$d->kepangkatan?></td></tr>
									<tr><td>Diklat Pim  </td><td>: <?=$d->diklat_pim?></td></tr>
									<tr><td>Pendidikan </td><td>: <?=$d->pendidikan?></td></tr>
									<tr><td>Riwayat Jabatan </td><td>: <?=$d->riwayat_jabatan?></td></tr>
									<tr><td>Diklat Teknis </td><td>: <?=$d->diklat_teknis?></td></tr>
									<tr><td>Diklat Fungsional </td><td>: <?=$d->diklat_fungsional?></td></tr>
									<tr><td>D U K </td><td>: <?=$d->duk?></td></tr>
									<tr><td>DP3 </td><td>: <?=$d->dp3?></td></tr>
									<tr><td>Disiplin </td><td>: <?=$d->disiplin?></td></tr>
									<tr><td>Karir </td><td>: <?=$d->karir?></td></tr>
									<tr><td>Keragaman Diklat </td><td>: <?=$d->keragaman_diklat?></td></tr>
									<tr><td>Seminar </td><td>: <?=$d->seminar?></td></tr>
									<tr><td>Prokir Strategis </td><td>: <?=$d->pokir_strategis?></td></tr>
									<tr><td>Penyikapan Teritorial & NKRI </td><td>: <?=$d->sikap_nkri?></td></tr>
									<tr><td>Pandangan Otda </td><td>: <?=$d->pandangan_otda?></td></tr>
									<tr><td>Visi & Misi </td><td>: <?=$d->visi_misi?></td></tr>
									<tr><td>Koordinasi & Komunikasi </td><td>: <?=$d->koordinasi_komunikasi?></td></tr>
									<tr><td><h2 style="color:#E74949;">Total Nilai</h2> </td><td><h2 style="color:#E74949;">: <?=$d->total_score?></td></h2></tr>
									
								</table>
							</section>
						</div>
					</article>
				<? } ?>
					
				</div>
			
		<!-- jQuery Visualize -->
		<!--[if lte IE 8]>
			<script language="javascript" type="text/javascript" src="js/plugins/visualize/excanvas.js"></script>
		<![endif]-->
		<script src="<?php echo base_url();?>asset/js/plugins/visualize/jquery.visualize.min.js"></script>
		<script src="<?php echo base_url();?>asset/js/plugins/visualize/jquery.visualize.tooltip.min.js"></script>
		
		<script>
			$(document).ready(function() {
			
				$('#table').each(function() {
					var chartType = ''; // Set chart type
					var chartWidth = $(this).parent().width()*0.9; // Set chart width to 90% of its parent
					
					if(chartWidth < 350) {
						var chartHeight = chartWidth;
					}else{
						var chartHeight = chartWidth*0.4;
					}
					
					if ($(this).attr('data-chart')) { // If exists chart-chart attribute
						chartType = $(this).attr('data-chart'); // Get chart type from data-chart attribute
					} else {
						chartType = 'area'; // If data-chart attribute is not set, use 'area' type as default. Options: 'bar', 'area', 'pie', 'line'
					}
					
					if(chartType == 'line' || chartType == 'pie') {
						$(this).hide().visualize({
							type: chartType,
							width: chartWidth,
							height: chartHeight,
							colors: ['#389abe','#fa9300','#6b9b20','#d43f3f','#8960a7','#33363b','#b29559','#6bd5b1','#66c9ee'],
							lineDots: 'double',
							interaction: true,
							multiHover: 5,
							tooltip: true,
							tooltiphtml: function(data) {
								var html ='';
								for(var i=0; i<data.point.length; i++){
									html += '<p class="tooltip chart_tooltip"><div class="tooltip-inner"><strong>'+data.point[i].value+'</strong> '+data.point[i].yLabels[0]+'</div></p>';
								}	
								return html;
							}
						});
					} else {
						$(this).hide().visualize({
							type: chartType,
							width: chartWidth,
							height: chartHeight,
							colors: ['#389abe','#fa9300','#6b9b20','#d43f3f','#8960a7','#33363b','#b29559','#6bd5b1','#66c9ee'],
						});
					}
				});
			
			});
		</script>
		
	