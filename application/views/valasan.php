
							<header>
								<h2>Form Alasan Pergantian SEKDA</h2>
							</header>
							<section class="tab-content">
							
								<!-- Tab #basic -->
								<div class="tab-pane active" id="basic">
								
									<!-- Example vertical forms -->
									<div class="row-fluid">
										<div class="span4">
											<h3>Alasan Penggantian</h3>
											<p>Silahkan masukkan alasan pergantian penjabat sekertaris daerah.</p>
										</div>
										<div class="span8">
											<form action="<?php echo site_url("chome/add_alasan/".$this->uri->segment(3));?>" method="POST">
												<?php foreach($datanya as $d){?>
												<fieldset>
													<?php if($this->session->flashdata('success')){?>
													<div class="alert alert-success">
														<button class="close" data-dismiss="alert" type="button">&times;</button>
														<strong>Well done!</strong> <?php echo $this->session->flashdata('success');?>.
													</div>
													<?php } else if($this->session->flashdata('error')){?>
													<div class="alert alert-block">
														<button class="close" data-dismiss="alert" type="button">&times;</button>
														<strong>Warning!</strong>
														<p><?php echo $this->session->flashdata('error');?></p>
													</div>
													<?php }?>
													
													<div class="control-group">
														<label class="control-label" for="input">No Surat Undangan Penilaian</label>
														<div class="controls">
															<input id="input" class="input-xlarge"  type="hidden" value="<?=$d->no_notadinas?>" name="nosurat">
															
															<span class="uneditable-input"><?=$d->no_notadinas;?></span>

														</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="input">Provinsi</label>
														<div class="controls">
															<input id="input" class="input-xlarge"  type="hidden" value="">
															<span class="uneditable-input"><?=$d->nm_provinsi;?></span>

														</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="input">Daerah Kab / Kota</label>
														<div class="controls">
															<input id="input" class="input-xlarge"  type="hidden" value="<?=$d->id_kab_kota?>" name="kota">
															<span class="uneditable-input"><?=$d->nm_kab_kota;?></span>

														</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="textarea">Alasan Pergantian Sekda</label>
														<div class="controls">
															<textarea name ="alasan" id="textarea" class="input-xlarge" rows="3"></textarea>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="textarea">Perihal</label>
														<div class="controls">
															<textarea name ="perihal" id="textarea" class="input-xlarge" rows="3"></textarea>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="textarea">No Surat Konsultasi Pengangkatan Sekda</label>
														<div class="controls">
															<input id="input" class="input-xlarge" name="no_surat_gub" type="text">
															<p class="help-block">Nomor surat ini adalah nomor surat Konsultasi Pengangkatan Sekretaris Daerah dari gubernur kepada Menteri Dalam Negeri </p>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="textarea">Tanggal Surat Konsultasi Pengangkatan Sekda</label>
														<div class="controls">
															<div class="input-append">
																<input class="datepicker input-small" type="text" value="<?php echo date("d-m-Y");?>" name="tgl_surat_gub">
																<span class="add-on"><i class="awe-calendar"></i></span>
															</div>
														</div>
													</div>
													<div class="form-actions">
														<button class="btn btn-primary btn-large" type="submit">Save changes</button>
													</div>
												</fieldset>
												<?php } ?>
											</form>
										</div>
									</div>	
								</div>
								
							</section>
							
						</div>
					</article>
<script src="<?php echo base_url();?>asset/js/jquery.min.js"></script>
<link href="<?php echo base_url();?>asset/css/multi-select.css" media="screen" rel="stylesheet" type="text/css">
<script src="<?php echo base_url();?>asset/js/jquery.multi-select.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>asset/js/jquery.quicksearch.js" type="text/javascript"></script>
	<script>
		$('#searchable').multiSelect({
	  selectableHeader: "<input type='text' id='search' autocomplete='off' class='input-medium' placeholder='try \"kota\"'>"
	});

	$('#search').quicksearch($('.ms-elem-selectable', '#ms-searchable' )).on('keydown', function(e){
	  if (e.keyCode == 40){
		$(this).trigger('focusout');
		$('#searchable').focus();
		return false;
	  }
	});
	</script>