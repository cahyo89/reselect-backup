
							<header>
								<h2>Form Input Calon SEKDA</h2>
							</header>
							<section class="tab-content">
							
								<!-- Tab #basic -->
								<div class="tab-pane active" id="basic">
								
									<!-- Example horizontal forms -->
									<div class="row-fluid">
										<div class="span4">
											<p>Silahkan mengisi form berikut untuk input data diri calon sekertaris daerah</p>
										</div>
										<div class="span8">
											<form class="form-horizontal" action="<?php echo site_url("chome/add_calon/".$this->uri->segment(3));?>" method="POST">
												<fieldset>
													<?php if($this->session->flashdata('success')){?>
													<div class="alert alert-success">
														<button class="close" data-dismiss="alert" type="button">&times;</button>
														<strong>Well done!</strong> <?php echo $this->session->flashdata('success');?>.
													</div>
													<?php } else if($this->session->flashdata('error')){?>
													<div class="alert alert-block">
														<button class="close" data-dismiss="alert" type="button">&times;</button>
														<strong>Warning!</strong>
														<p><?php echo $this->session->flashdata('error');?></p>
													</div>
													<?php }?>
													<?php foreach($datanya as $d){?>
														<input type="hidden" name="kab_kota" value="<?=$d->id_kab_kota?>"/>
														<input type="hidden" name="nosurat" value="<?=$d->no_notadinas;?>"/>
													<?php } ?>
													<div class="control-group">
														<label class="control-label" for="input">NIP</label>
														<div class="controls">
															<input type="text"  class="input-xlarge"  value="" name="nip">
														</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="input">Nama Lengkap</label>
														<div class="controls">
															<input type="text"  class="input-xlarge"  value="" name="nama">
														</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="input">Tanggal Lahir</label>
														<div class="controls">
															<div class="input-append">
																<input class="datepicker input-small" type="text"  name="tgllahir"><span class="add-on"><i class="awe-calendar"></i></span>
															</div>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="input">Pangkat Calon</label>
														<div class="controls">
															<select id="pangkat"  name="pangkat" >
																	<option value="Pembina (IV/a)">Pembina (IV/a)</option>
																	<option value="Pembina Tingkat I (IV/b)">Pembina Tingkat I (IV/b)</option>
																	<option value="Pembina Utama Muda (IV/c)">Pembina Utama Muda (IV/c)</option>
																	<option value="Pembina Utama Madya (IV/d)">Pembina Utama Madya (IV/d)</option>
																	<option value="Pembina Utama (IV/e)">Pembina Utama (IV/e)</option>
																	
															</select>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="input">TMT</label>
														<div class="controls">
															<div class="input-append">
																<input class="input-small datepicker2"  type="text"  name="tmt"><span class="add-on"><i class="awe-calendar"></i></span>
															</div>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="input">Pendidikan Umum</label>
															<div class="controls">
																<input type="text"  class="input-xlarge" id="input" name="pend_umum" />
																<p class="help-block">Contoh : Pasca Sarjana, Tahun 2001</p>
															</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="input">Pendidikan Struktural</label>
															<div class="controls">
																<input type="text"  class="input-xlarge" id="input" name="pend_struk" />
																<p class="help-block">Contoh : Diklatpim Tk. III, Tahun 2006</p>
															</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="input">Jabatan</label>
															<div class="controls">
																<input type="text"  class="input-xlarge" id="input" name="jabatannya" />
																<p class="help-block">Contoh : Staf Ahli Bupati Tambrauw Bidang Pengawasan Teknik</p>
															</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="input">Perjalanan Karir</label>
															<div class="controls">
																<a href="#" id="addNew"><span class="awe-plus-sign"></span> Add</a>
																<div id="addinput">
																<p>
																<input type="text" id="p_new" class="input-xlarge" name="jabatan[]" value="" placeholder="Jabatan" /> 
																<select id="p_new2" style="width: 60px;" name="eselon[]" >
																	<option value="V.a">V.a</option>
																	<option value="IV.b">IV.b</option>
																	<option value="IV.a">IV.a</option>
																	<option value="III.b">III.b</option>
																	<option value="III.a">III.a</option>
																	<option value="II.b">II.b</option>
																	<option value="II.a">II.a</option>
																	<option value="I.b">I.b</option>
																	<option value="I.a">I.a</option>
																</select>
																
																</p>
																</div>
															</div>
													</div>
													
													<div class="form-actions">
														<button class="btn btn-primary btn-large" type="submit">Save changes</button>
														
													</div>
												</fieldset>
											</form>
										</div>
									</div>
									
								</div>
								
							</section>
							<footer class="info">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam blandit, dolor mollis adipiscing elementum, ipsum turpis euismod tellus, vitae mollis velit leo id nisi.</p>
							</footer>
						</div>
					</article>
<script src="<?php echo base_url();?>asset/js/jquery.min.js"></script>
<link href="<?php echo base_url();?>asset/css/multi-select.css" media="screen" rel="stylesheet" type="text/css">
<script src="<?php echo base_url();?>asset/js/jquery.multi-select.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>asset/js/jquery.quicksearch.js" type="text/javascript"></script>
	<script>
		$('#searchable').multiSelect({
	  selectableHeader: "<input type='text' id='search' autocomplete='off' class='input-medium' placeholder='try \"kota\"'>"
	});

	$('#search').quicksearch($('.ms-elem-selectable', '#ms-searchable' )).on('keydown', function(e){
	  if (e.keyCode == 40){
		$(this).trigger('focusout');
		$('#searchable').focus();
		return false;
	  }
	});
	</script>
		<script type="text/javascript">
			$(function() {
				var addDiv = $('#addinput');
				var i = $('#addinput p').size() + 1;

				$('#addNew').live('click', function() {
				$('<p><input type="text" id="p_new'+i+'"  class="input-xlarge" name="jabatan[]" value="" placeholder="Jabatan" /><select id="p_new2'+i+'" style="width: 50px;" name="eselon[]" ><option value="V.a">V.a</option><option value="IV.b">IV.b</option><option value="IV.a">IV.a</option><option value="III.b">III.b</option><option value="III.a">III.a</option><option value="II.b">II.b</option><option value="II.a">II.a</option><option value="I.b">I.b</option><option value="I.a">I.a</option></select> <a href="#" class="awe-remove" id="remNew"></a> </p>').fadeIn("slow").appendTo(addDiv);
				i++;

				return false;
				});

				$('#remNew').live('click', function() {
					if( i > 2 ) {
						$(this).parents('p').remove();
						i--;
					}
					return false;
				});
			});

		</script>