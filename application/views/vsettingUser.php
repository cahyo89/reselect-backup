
							<header>
								<h2>Setting Akun</h2>
							</header>
							<section class="tab-content">
							
								<!-- Tab #basic -->
								<div class="tab-pane active" id="basic">
								
									<!-- Example horizontal forms -->
									<div class="row-fluid">
										<div class="span4">
											<p>Silahkan setting akun anda sesuai dengan kebutuhan.</p>
										</div>
										<div class="span8">
											<form class="form-horizontal" action="<?php echo site_url("chome/change_user");?>" method="POST">
												<fieldset>
													<?php if($this->session->flashdata('success')){?>
													<div class="alert alert-success">
														<button class="close" data-dismiss="alert" type="button">&times;</button>
														<strong>Well done!</strong> <?php echo $this->session->flashdata('success');?>.
													</div>
													<?php } else if($this->session->flashdata('error')){?>
													<div class="alert alert-block">
														<button class="close" data-dismiss="alert" type="button">&times;</button>
														<strong>Warning!</strong>
														<p><?php echo $this->session->flashdata('error');?></p>
													</div>
													<?php }?>
													
													<div class="control-group">
														<label class="control-label" for="input">Nama User</label>
														<div class="controls">
															<input type="text"  class="input-xlarge" value="<?=$datanya[0]['nama']?>" name="username">
														</div>
													</div>
													
													<div class="control-group">
														<label class="control-label" for="input">Email</label>
														<div class="controls">
															<input type="email"  class="input-xlarge" value="<?=$datanya[0]['email']?>" name="email">
															<p class="help-block">Email ini akan digunakan sebagai email login</p>
														</div>
													</div>
													
													<div class="control-group">
														<label class="control-label" for="input">New Password</label>
														<div class="controls">
															<input type="password"  class="input-xlarge" value="" name="pass1">
															<p class="help-block">silahkan isi field password ini jika ingin merubah password sebelumnya</p>
														</div>
													</div>
													
													<div class="control-group">
														<label class="control-label" for="input">Re-Type Password</label>
														<div class="controls">
															<input type="password"  class="input-xlarge" value="" name="pass2">
															<p class="help-block">silahkan ketil ulang password baru anda</p>
														</div>
													</div>
													
													
													
													<div class="form-actions">
														<button class="btn btn-primary btn-large" type="submit" onclick='return window.confirm("Anda yakin melakukan perubahan ini?");'>Save changes</button>
														
													</div>
												</fieldset>
											</form>
										</div>
									</div>
									
								</div>
								
							</section>
							
						</div>
					</article>
<script src="<?php echo base_url();?>asset/js/jquery.min.js"></script>
<link href="<?php echo base_url();?>asset/css/multi-select.css" media="screen" rel="stylesheet" type="text/css">
<script src="<?php echo base_url();?>asset/js/jquery.multi-select.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>asset/js/jquery.quicksearch.js" type="text/javascript"></script>
	<script>
		$('#searchable').multiSelect({
	  selectableHeader: "<input type='text' id='search' autocomplete='off' class='input-medium' placeholder='try \"kota\"'>"
	});

	$('#search').quicksearch($('.ms-elem-selectable', '#ms-searchable' )).on('keydown', function(e){
	  if (e.keyCode == 40){
		$(this).trigger('focusout');
		$('#searchable').focus();
		return false;
	  }
	});
	</script>