
							<header>
								<h2>Form Undangan Nota Dinas Pemilihan SEKDA</h2>
							</header>
							<section class="tab-content">
							
								<!-- Tab #basic -->
								<div class="tab-pane active" id="basic">
								
									<!-- Example horizontal forms -->
									<div class="row-fluid">
										<div class="span4">
											<p>Silahkan mengisi form berikut untuk membuat nota dinas undangan pemilihan sekertaris daerah yang akan di<i>generate</i> secara otomatis dalam format .doc</p>
										</div>
										<div class="span8">
											<form class="form-horizontal" action="<?php echo site_url("chome/add_undangansekda");?>" method="POST">
												<fieldset>
													<?php if($this->session->flashdata('success')){?>
													<div class="alert alert-success">
														<button class="close" data-dismiss="alert" type="button">&times;</button>
														<strong>Well done!</strong> <?php echo $this->session->flashdata('success');?>.
													</div>
													<?php } else if($this->session->flashdata('error')){?>
													<div class="alert alert-block">
														<button class="close" data-dismiss="alert" type="button">&times;</button>
														<strong>Warning!</strong>
														<p><?php echo $this->session->flashdata('error');?></p>
													</div>
													<?php }?>
													
													<div class="control-group">
														<label class="control-label" for="input">Tanggal Nota Dinas</label>
														<div class="controls">
															<input type="text"  class="input-xlarge disabled" id="disabledInput" value="<?php echo date("d-m-Y");?>" name="tglnota">
															<p class="help-block">Tanggal ini adalah tanggal yang akan dicantumkan pada tanggal surat nota dinas</p>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="input">Tanggal Rapat</label>
														<div class="controls">
															<div class="input-append">
																<input class="datepicker input-small" type="text" value="<?php echo date("d-m-Y");?>" name="tglrapat"><span class="add-on"><i class="awe-calendar"></i></span>
															</div>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="input">Jam Rapat</label>
															<div class="controls">
																<input type="text"  class="input-small" id="input" name="jamrapat" />
																<p class="help-block">contoh : 10.00 WIB</p>
															</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="input">No Surat Nota Dinas</label>
															<div class="controls">
																<input type="text"  class="input-xlarge" id="input" name="nosurat" />
																<p class="help-block">Nomor surat ini adalah nomor surat yang akan digunakan pada surat nota dinas</p>
															</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="select">Daerah (Kota / Kabupaten)</label>
															<select id='searchable' multiple='multiple' name="kota[]">
																<?php
																	foreach ($kota as $k){
																		echo "<option value='$k->id_kab_kota'>$k->nm_kab_kota</option>";
																	}
																?>
															
															</select>
														<!--div class="controls">
														</div-->
													</div>
													<div class="form-actions">
														<button class="btn btn-primary btn-large" type="submit">Save changes</button>
														
													</div>
												</fieldset>
											</form>
										</div>
									</div>
									
								</div>
								
							</section>
							
						</div>
					</article>
<script src="<?php echo base_url();?>asset/js/jquery.min.js"></script>
<link href="<?php echo base_url();?>asset/css/multi-select.css" media="screen" rel="stylesheet" type="text/css">
<script src="<?php echo base_url();?>asset/js/jquery.multi-select.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>asset/js/jquery.quicksearch.js" type="text/javascript"></script>
	<script>
		$('#searchable').multiSelect({
	  selectableHeader: "<input type='text' id='search' autocomplete='off' class='input-medium' placeholder='try \"kota\"'>"
	});

	$('#search').quicksearch($('.ms-elem-selectable', '#ms-searchable' )).on('keydown', function(e){
	  if (e.keyCode == 40){
		$(this).trigger('focusout');
		$('#searchable').focus();
		return false;
	  }
	});
	</script>