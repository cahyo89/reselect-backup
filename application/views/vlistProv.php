
							<header>
								<h2>Kelola Data Provinsi</h2>
								<ul class="data-header-actions">
													<li class="demoTabs active">
													<a href="#demoModal" class="btn btn-inverse" data-toggle="modal"><span class="awe-plus-sign"></span>Tambah Provinsi</a>
													</li>
													</ul>
							</header>
							<section class="tab-content">
							
								<!-- Tab #basic -->
								<div class="tab-pane active" id="basic">
								
									<!-- Example horizontal forms -->
									<div class="row-fluid">
										
										<div class="span12">
											
												<fieldset>
													<?php if($this->session->flashdata('success')){?>
													<div class="alert alert-success">
														<button class="close" data-dismiss="alert" type="button">&times;</button>
														<strong>Well done!</strong> <?php echo $this->session->flashdata('success');?>.
													</div>
													<?php } else if($this->session->flashdata('error')){?>
													<div class="alert alert-block">
														<button class="close" data-dismiss="alert" type="button">&times;</button>
														<strong>Warning!</strong>
														<p><?php echo $this->session->flashdata('error');?></p>
													</div>
													<?php }?>
													
														
									<div class="modal fade hide" id="demoModal">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">×</button>
										<h3>Tambah Provinsi Baru</h3>
									</div>
									<div class="modal-body">
										<form class="form-horizontal" action="<?php echo site_url("chome/add_prov");?>" method="POST">
											<fieldset>
												<div class="control-group">
														<label class="control-label" for="input">Nama Provinsi</label>
															<div class="controls">
																<input type="text"  class="input-xlarge" id="input" name="nm_prov" placeholder="Nama Provinsi ..."/>
																
															</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="input">Ibu Kota Provinsi</label>
															<div class="controls">
																<input type="text"  class="input-xlarge" id="input" name="ibukota_prov" placeholder="Ibukota Provinsi ..."/>
																
															</div>
													</div>
											</fieldset>
									</div>
									<div class="modal-footer">
										<a href="#" class="btn btn-alt" data-dismiss="modal">Cancle</a>
										<button class="btn btn-alt btn-large btn-primary" type="submit">Save changes</button>
									</div>
										</form>
								</div>
																										
													
															
									<h3>List Provinsi</h3>
									<table class="datatable table table-striped table-bordered table-hover" id="example">
										<thead>
											<tr>
												<th width="50px">No</th>
												<th>Nama Provinsi</th>
												<th>Ibu Kota Provinsi</th>
												<th width="100px">Aksi</th>
											</tr>
										</thead>
										<tbody>
										<?php $no=1; foreach ($provnya as $p){?>
											<tr class="odd gradeX">
												<td><?=$no?></td>
												<td><?=$p->nm_provinsi?></td>
												<td><?=$p->ibukota_prov?></td>
												<td>
													<div class="btn-group">
														<a class="btn" title="Edit Kategori <?=$p->nm_provinsi?>"  href="#demoModal2" data-toggle="modal"  onclick="document.kate.id_prov.value='<?=$p->id_provinsi?>';document.kate.nm_prov.value='<?=$p->nm_provinsi?>';document.kate.ibukota_prov.value='<?=$p->ibukota_prov?>';"><span class="awe-wrench"></span></a>
														<a onclick='return window.confirm("Anda yakin menghapus data ini ?");' href="<?php echo base_url();?>index.php/chome/delete_prov/<?=$p->id_provinsi?>" class="btn" title="Hapus Kategori <?=$p->nm_provinsi?>"><span class="awe-remove" ></span></a>
													</div>
												</td>
											</tr>
										<?php 
											$no++;
										} ?>
										</tbody>
									</table>
													
													
													</div>
												</fieldset>
										</div>
									</div>
									
								</div>
								
							</section>
							
						</div>
					</article>
					
					
					
								<div class="modal fade hide" id="demoModal2">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h3>Edit Nama Provinsi</h3>
									</div>
									<div class="modal-body">
									<form action="<?php echo site_url("chome/edit_prov");?>" method="POST" id="kate" name="kate">
										<fieldset>
											<div class="control-group">
												<label class="control-label" for="input">Nama Provinsi</label>
												<div class="controls">
													<input id="id_prov" class="input-xlarge" name="id_prov" type="hidden" value="">
													<input id="nm_prov" name="nm_prov" class="input-xlarge" type="text">
												</div>
											</div>
											<div class="control-group">
												<label class="control-label" for="input">Ibu Kota Provinsi</label>
												<div class="controls">
													<input id="ibukota_prov" name="ibukota_prov" class="input-xlarge" type="text">
												</div>
											</div>
										</fieldset>
									</div>
									<div class="modal-footer">
										<a href="#" class="btn " data-dismiss="modal">Cancle</a>
										<button onclick='return window.confirm("Anda yakin melakukan perubahan data ini ?");' class="btn btn-large btn-primary" type="submit">Save changes</button>
									</div>
								</form>
								</div>
							
<script src="<?php echo base_url();?>asset/js/jquery.min.js"></script>
<link href="<?php echo base_url();?>asset/css/multi-select.css" media="screen" rel="stylesheet" type="text/css">
<script src="<?php echo base_url();?>asset/js/jquery.multi-select.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>asset/js/jquery.quicksearch.js" type="text/javascript"></script>
	<script>
		$('#searchable').multiSelect({
	  selectableHeader: "<input type='text' id='search' autocomplete='off' class='input-medium' placeholder='try \"kota\"'>"
	});

	$('#search').quicksearch($('.ms-elem-selectable', '#ms-searchable' )).on('keydown', function(e){
	  if (e.keyCode == 40){
		$(this).trigger('focusout');
		$('#searchable').focus();
		return false;
	  }
	});
	</script>
	<script src="<?php echo base_url();?>asset/js/plugins/dataTables/jquery.datatables.min.js"></script>
	<script>
			/* Default class modification */
			$.extend( $.fn.dataTableExt.oStdClasses, {
				"sWrapper": "dataTables_wrapper form-inline"
			} );
			
			/* API method to get paging information */
			$.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
			{
				return {
					"iStart":         oSettings._iDisplayStart,
					"iEnd":           oSettings.fnDisplayEnd(),
					"iLength":        oSettings._iDisplayLength,
					"iTotal":         oSettings.fnRecordsTotal(),
					"iFilteredTotal": oSettings.fnRecordsDisplay(),
					"iPage":          Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
					"iTotalPages":    Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
				};
			}
			
			/* Bootstrap style pagination control */
			$.extend( $.fn.dataTableExt.oPagination, {
				"bootstrap": {
					"fnInit": function( oSettings, nPaging, fnDraw ) {
						var oLang = oSettings.oLanguage.oPaginate;
						var fnClickHandler = function ( e ) {
							e.preventDefault();
							if ( oSettings.oApi._fnPageChange(oSettings, e.data.action) ) {
								fnDraw( oSettings );
							}
						};
						
						$(nPaging).addClass('pagination').append(
							'<ul>'+
								'<li class="prev disabled"><a href="#">&larr; '+oLang.sPrevious+'</a></li>'+
								'<li class="next disabled"><a href="#">'+oLang.sNext+' &rarr; </a></li>'+
							'</ul>'
						);
						var els = $('a', nPaging);
						$(els[0]).bind( 'click.DT', { action: "previous" }, fnClickHandler );
						$(els[1]).bind( 'click.DT', { action: "next" }, fnClickHandler );
					},
					
					"fnUpdate": function ( oSettings, fnDraw ) {
						var iListLength = 5;
						var oPaging = oSettings.oInstance.fnPagingInfo();
						var an = oSettings.aanFeatures.p;
						var i, j, sClass, iStart, iEnd, iHalf=Math.floor(iListLength/2);
						
						if ( oPaging.iTotalPages < iListLength) {
							iStart = 1;
							iEnd = oPaging.iTotalPages;
						}
						else if ( oPaging.iPage <= iHalf ) {
							iStart = 1;
							iEnd = iListLength;
						} else if ( oPaging.iPage >= (oPaging.iTotalPages-iHalf) ) {
							iStart = oPaging.iTotalPages - iListLength + 1;
							iEnd = oPaging.iTotalPages;
						} else {
							iStart = oPaging.iPage - iHalf + 1;
							iEnd = iStart + iListLength - 1;
						}
						
						for ( i=0, iLen=an.length ; i<iLen ; i++ ) {
							// Remove the middle elements
							$('li:gt(0)', an[i]).filter(':not(:last)').remove();
							
							// Add the new list items and their event handlers
							for ( j=iStart ; j<=iEnd ; j++ ) {
								sClass = (j==oPaging.iPage+1) ? 'class="active"' : '';
								$('<li '+sClass+'><a href="#">'+j+'</a></li>')
									.insertBefore( $('li:last', an[i])[0] )
									.bind('click', function (e) {
										e.preventDefault();
										oSettings._iDisplayStart = (parseInt($('a', this).text(),10)-1) * oPaging.iLength;
										fnDraw( oSettings );
									} );
							}
							
							// Add / remove disabled classes from the static elements
							if ( oPaging.iPage === 0 ) {
								$('li:first', an[i]).addClass('disabled');
							} else {
								$('li:first', an[i]).removeClass('disabled');
							}
							
							if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {
								$('li:last', an[i]).addClass('disabled');
							} else {
								$('li:last', an[i]).removeClass('disabled');
							}
						}
					}
				}
			});
			
			/* Show/hide table column */
			function dtShowHideCol( iCol ) {
				var oTable = $('#example-2').dataTable();
				var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
				oTable.fnSetColumnVis( iCol, bVis ? false : true );
			};
			
			/* Table #example */
			$(document).ready(function() {
				$('.datatable').dataTable( {
					"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
					"sPaginationType": "bootstrap",
					"oLanguage": {
						"sLengthMenu": "_MENU_ records per page"
					}
				});
				$('.datatable-controls').on('click','li input',function(){
					dtShowHideCol( $(this).val() );
				})
			});
		</script>
		<script type="text/javascript" src="<?php echo base_url();?>asset/js/plugins/jGrowl/jquery.jgrowl.js"></script>
		<?php if (($this->session->flashdata('success')) || ($this->session->flashdata('error'))){?>
		<script>
			$(document).ready(function(){
				
				// This value can be true, false or a function to be used as a callback when the closer is clciked
				
				<?php if($this->session->flashdata('success')){?>
				$.jGrowl("<?=$this->session->flashdata('success')?>", { 
					theme: 'success'
				});
				<?php }else if($this->session->flashdata('error')){?>
				$.jGrowl("<?=$this->session->flashdata('error')?>", {
					life: 2500,
					theme: 'danger'
				});
				<?php }?>
				});
		</script>
				<?php }?>
		