<?php
foreach($undangan as $u){
	$tgl_nota=$u->tgl_nota;
	$tgl_rapat=$u->tgl_rapat;
	$jam_rapat=$u->jam_rapat;
	$no_notadinas=$u->no_notadinas;
	$jum=$this->m_home->getKabDetail($u->no_notadinas)->num_rows();
	$i=1;
	$kota="";
	foreach($this->m_home->getKabDetail($u->no_notadinas)->result() as $s){
		if($i==($jum-1)){
			$kota=$kota."".$s->nm_kab_kota;
		}elseif($i<$jum){
			$kota=$kota."".$s->nm_kab_kota.', ';
		}else{
			$kota=$kota." dan ".$s->nm_kab_kota;
		}
		$i++;
	}
 }
 function cariTanggal2($tglnya){
	$t=$tglnya;
	$namahari = date('l', strtotime($t));
	
	if ($namahari == "Sunday") $namahari = "Minggu";
	else if ($namahari == "Monday") $namahari = "Senin";
	else if ($namahari == "Tuesday") $namahari = "Selasa";
	else if ($namahari == "Wednesday") $namahari = "Rabu";
	else if ($namahari == "Thursday") $namahari = "Kamis";
	else if ($namahari == "Friday") $namahari = "Jumat";
	else if ($namahari == "Saturday") $namahari = "Sabtu";
	//echo $namahari;

	$tg=explode("-", $t);
	$a=$tg[1];

	switch ($a) {
		case 1:
			$bulan="Januari";
			break;
		case 2:
			$bulan="Februari";
			break;
		case 3:
			$bulan="Maret";
			break;
		case 4:
			$bulan="April";
			break;
		case 5:
			$bulan="Mei";
			break;
		case 6:
			$bulan="Juni";
			break;
		case 7:
			$bulan="Juli";
			break;
		case 8:
			$bulan="Agustus";
			break; 
		case 9:
			$bulan="September";
			break;
		case 10:
			$bulan="Oktober";
			break;
		case 11:
			$bulan="November";
			break;
		case 12:
			$bulan="Desember";
			break;
	}
	
	$tanggal2=$namahari." , ".$tg[0]." ".$bulan." ".$tg[2];
	return $tanggal2;
}
function cariTanggal($tglnya){
	$t=$tglnya;
	$namahari = date('l', strtotime($t));
	
	if ($namahari == "Sunday") $namahari = "Minggu";
	else if ($namahari == "Monday") $namahari = "Senin";
	else if ($namahari == "Tuesday") $namahari = "Selasa";
	else if ($namahari == "Wednesday") $namahari = "Rabu";
	else if ($namahari == "Thursday") $namahari = "Kamis";
	else if ($namahari == "Friday") $namahari = "Jumat";
	else if ($namahari == "Saturday") $namahari = "Sabtu";
	//echo $namahari;

	$tg=explode("-", $t);
	$a=$tg[1];

	switch ($a) {
		case 1:
			$bulan="Januari";
			break;
		case 2:
			$bulan="Februari";
			break;
		case 3:
			$bulan="Maret";
			break;
		case 4:
			$bulan="April";
			break;
		case 5:
			$bulan="Mei";
			break;
		case 6:
			$bulan="Juni";
			break;
		case 7:
			$bulan="Juli";
			break;
		case 8:
			$bulan="Agustus";
			break; 
		case 9:
			$bulan="September";
			break;
		case 10:
			$bulan="Oktober";
			break;
		case 11:
			$bulan="November";
			break;
		case 12:
			$bulan="Desember";
			break;
	}
	
	$tanggal2=$namahari." tanggal ".$tg[0]." ".$bulan." ".$tg[2];
	return $tanggal2;
}

//$tanggal=$tg[0]." ".$bulan." ".$tg[2];
$tanggal2=cariTanggal($tgl_rapat);

 
?>

<html>
	<head>
		<style>
				body{
				font-family:Tahoma, Geneva, sans-serif; 
				font-size:12pt; 
				/*margin: 3em;*/
				margin:5px;
			}
			div {
			/* background: #EDEDED;
			   border: solid 1px #CCC;
			  width: 650px;*/
			  width: 690px;
			
			}

			.break-word {
			  word-wrap: break-word;
			}
			hr.style-eight { 
				 color:#c00;background-color:#000;height:3px;border:none;
				
			} 
			
		</style>
	</head>
	<body>
		<table width="100%" cellspacing=0 cellpadding=0>
			<tr >
				<td width="66" ><img src="<?php echo base_url();?>asset/img/depdagri.png" width="100px"/></td>
				<td>
					<table width="100%" cellspacing=0 cellpadding=-1>
						<tr>
							<td style="font-size:14pt;  text-align: center;">KEMENTERIAN DALAM NEGERI</td>
						</tr>
							<tr>
							<td style="font-size:14pt;  text-align: center;">REPUBLIK INDONESIA</td>
						</tr>
						<tr>
							<td style="font-size:16pt;  text-align: center;font-weight:bold;">SEKRETARIAT JENDERAL</td>
						</tr>
						<tr>
							<td style="font-size:9pt;  text-align: center;">Jalan Medan Merdeka Utara Nomor 7 Jakarta 10110</td>
						</tr>
						<tr>
							<td style="font-size:9pt;  text-align: center;">Telepon (021) 3458542 Fax. (021) 3458542  Website : <u>www.kemendagri.go.id</u></td>
						</tr>
					</table>
				</td>
			</tr>
			
		</table>
		<hr class="style-eight"/>
		
		
		<table width="100%" style="margin-top:20px">
			<tr>
				<td colspan=2 style=" text-align: center;"><b><u>NOTA DINAS</u></b></td>
			</tr>
			<tr>
				<td colspan=2 style=" text-align: center;">&nbsp;</td>
			</tr>
			<tr>
				<td width="50">Kepada</td>
				<td>:   Yth. <?php echo ucwords(strtolower($this->m_home->getPenjabat4(2)));?> Sekretaris Jenderal</td>
			</tr>
			<tr>
				<td>Dari</td>
				<td>:   Kepala Biro Kepegawaian</td>
			</tr>
			<tr>
				<td>Tembusan</td>
				<td>:   &nbsp;</td>
			</tr>
			<tr>
				<td>Tanggal</td>
				<td>:   <?=$tgl_nota?>&nbsp;</td>
			</tr>
			<tr>
				<td>Nomor</td>
				<td>:   <?=$no_notadinas?>&nbsp;</td>
			</tr>
			<tr>
				<td>Sifat</td>
				<td>:   &nbsp;</td>
			</tr>
			<tr>
				<td>Lampiran</td>
				<td>:   &nbsp;</td>
			</tr>
			<tr>
				<td>Hal</td>
				<td>:   Undangan Penilaian Calon Sekretaris Daerah Kabupaten/Kota</td>
			</tr>
		</table>
		<hr width="100%" class="style-eight" style="height:1.5px; margin-top:10px;"  />
		<div class="break-word" style="text-align: justify;">
		<p><span style="margin-right:60px">&nbsp;</span>Dengan hormat kami laporkan kepada <?php echo ucwords(strtolower($this->m_home->getPenjabat4(2)));?> Sekretaris Jenderal perihal tersebut di atas, sebagai  berikut :
			<br/>
			<ol style="margin-left:-17px">
				<li style="margin-bottom:10px">Dalam rangka penilaian persetujuan pengangkatan calon Sekretaris Daerah <?=$kota?> akan diselenggarakan rapat penilaian calon Sekretaris Daerah Kabupaten/Kota pada hari <?=$tanggal2?> Pukul <?=$jam_rapat?> WIB bertempat di Ruang Rapat Kepala Biro Kepegawaian.</li>
				<li>Sehubungan dengan hal tersebut, terlampir disampaikan Net Surat Undangan penilaian calon Sekretaris Daerah Kabupaten/Kota, untuk mohon perkenan tanda tangan Ibu Sekretaris Jenderal.</li>
			</ol>
			<br/>
			<span style="margin-right:60px">&nbsp;</span>Demikian untuk menjadi periksa.
			<br/><br/><br/>
			<table align="right">
				<tr>
					<td style="text-align: center;"><?php echo $this->m_home->getPenjabat3(1); ?></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td style="text-align: center;"><b><?=$this->m_home->getPenjabat2(1)?></b></td>
				</tr>
			</table>
		</p>
		</div>
		<div style="page-break-before: always;">&nbsp;</div>
		<table width="100%">
			<tr >
				<td style="font-size:14pt;  text-align: center;"><img src="<?php echo base_url();?>asset/img/garuda.png" width="80px"/></td>
			</tr>
			<tr>
				<td style="font-size:10pt;  text-align: center;">KEMENTERIAN DALAM NEGERI</td>
			</tr>
			<tr>
				<td style="font-size:10pt;  text-align: center;"><b>REPUBLIK INDONESIA</b></td>
			</tr>
		</table>
		<table width="100%" style="font-size:12pt; margin-top:20px; margin-bottom:20px;">
			
			<tr>
				<td width="50">&nbsp;</td>
				<td width="190">&nbsp;</td>
				<td><span style="margin-right:150px">&nbsp;</span>Jakarta,</td>
			</tr>
			<tr>
				<td >&nbsp;</td>
				<td >&nbsp;</td>
				<td><span style="margin-right:150px">&nbsp;</span>Kepada</td>
			</tr>
			<tr>
				<td >Nomor</td>
				<td >:	&nbsp;</td>
				<td><span style="margin-right:120px">&nbsp;</span>Yth. Daftar terlampir</td>
			</tr>
			<tr>
				<td>Sifat</td>
				<td>: RAHASIA</td>
				<td><span style="margin-right:130px">&nbsp;</span>di</td>
			</tr>
			<tr>
				<td>Lampiran</td>
				<td>:&nbsp;</td>
				<td><span style="margin-right:165px">&nbsp;</span>tempat.</td>
			</tr>
		
			<tr>
				<td>Hal</td>
				<td>:   Undangan</td>
				<td>   &nbsp;</td>	
			</tr>
		</table>
		<div class="break-word" style="text-align: justify;margin-left:70px;width:610px;">
		<p><span style="margin-right:60px">&nbsp;</span>Dalam rangka penilaian calon Sekretaris Daerah Kabupaten/Kota sebagaimana diatur dalam Peraturan Menteri Dalam Negeri Nomor 5 Tahun 2005 tentang Pedoman Penilaian Calon Sekretaris Daerah Provinsi dan Kabupaten/Kota serta Pejabat Struktural Eselon II di lingkungan Kabupaten/Kota, dengan hormat diminta kehadiran Saudara atau dapat menugaskan Sekretaris komponen masing-masing untuk hadir pada :<br/>
		<table width="100%">
			<tr>
				<td width="60">Hari/Tanggal</td>
				<td width="10">: </td>
				<td><?=cariTanggal2($tgl_rapat)?></td>
			</tr>
			<tr>
				<td>Waktu</td>
				<td>: </td>
				<td><?=$jam_rapat?> WIB</td>
			</tr>
			<tr>
				<td valign="top">Tempat</td>
				<td valign="top">: </td>
				<td valign="top">Ruang Rapat Kepala Biro Kepegawaian Lantai II,  Jl. Medan Merdeka Utara No. 7 Jakarta Pusat</td>
			</tr>
			<tr>
				<td valign="top">Acara</td>
				<td valign="top">: </td>
				<td valign="top">Membahas penilaian pengangkatan calon Sekertaris Daerah <?=$kota?></td>
			</tr>
		</table>
		<br/>
		<br/>
		<span style="margin-right:60px">&nbsp;</span>Demikian dan atas kerjasama yang baik diucapkan terima kasih. 
		<br/><br/>
			<table align="right">
				<tr>
					<td style="text-align: center; font-size:12pt;">a.n. MENTERI DALAM NEGERI</td>
				</tr>
				<tr>
					<td style="text-align: center; font-size:12pt;"><?php echo $this->m_home->getPenjabat3(2); ?></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td style="text-align: center; font-size:12pt;"><b><?=$this->m_home->getPenjabat2(2)?></b></td>
				</tr>
			</p>
				</table>
				<br/><br/><br/><br/><br/><br/>
			<table width="100%" style="margin-left:-40px;">
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><b>Tembusan  :</b>
			<br/>Yth. Bapak Menteri Dalam Negeri, sebagai laporan.</td>
				</tr>
			</table>
			
			
			

		</div>
		<div style="page-break-before: always;">&nbsp;</div>
		<div class="break-word" style="text-align: justify;">
		<table width=100%>
		<tr>
			<td  style="text-align: center;"><b>DAFTAR PEJABAT YANG DIUNDANG</td>
		</tr>
		</table></b><br/><br/><br/>
		<ol>
			<li style="margin-bottom: 20px;">Sekretaris Jenderal Kemendagri;</li>
			<li style="margin-bottom: 20px;">Inspektur Jenderal Kemendagri;</li>
			<li style="margin-bottom: 20px;">Direktur Jenderal Kesatuan Bangsa dan Politik Kemendagri;</li>
			<li style="margin-bottom: 20px;">Direktur Jenderal Otonomi Daerah Kemendagri;</li>
			<li style="margin-bottom: 20px;">Kepala Badan Pendidikan dan Pelatihan Kemendagri;</li>
			<li style="margin-bottom: 20px;">Kepala Biro Kepegawaian Kemendagri;</li>
		</ol>
		</div>
	</body>
</html>