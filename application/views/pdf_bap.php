<?php
foreach($undangan as $u){
	$tgl_nota=$u->tgl_nota;
	$tgl_rapat=$u->tgl_rapat;
	$jam_rapat=$u->jam_rapat;
	$no_notadinas=$u->no_notadinas;
	$jum=$this->m_home->getKabDetail($u->no_notadinas)->num_rows();
	$i=1;
	$kota="";
	foreach($this->m_home->getKabDetail($u->no_notadinas)->result() as $s){
		if($i<$jum){
		$kota=$kota."".$s->nm_kab_kota.', ';
		}else{
		$kota=$kota." dan ".$s->nm_kab_kota;
		}
		$i++;
	}
	
	$jum=$this->m_home->getProvDetail($u->no_notadinas)->num_rows();
	$i=1;
	$prov="";
	foreach($this->m_home->getProvDetail($u->no_notadinas)->result() as $s){
		if($i<$jum){
		$prov=$prov."".ucwords(strtolower($s->nm_provinsi)).', ';
		}else{
		$prov=$prov." dan ".ucwords(strtolower($s->nm_provinsi));
		}
		$i++;
	}
 }
function cariTanggal($tglnya){
	$t=$tglnya;
	$namahari = date('l', strtotime($t));
	
	if ($namahari == "Sunday") $namahari = "Minggu";
	else if ($namahari == "Monday") $namahari = "Senin";
	else if ($namahari == "Tuesday") $namahari = "Selasa";
	else if ($namahari == "Wednesday") $namahari = "Rabu";
	else if ($namahari == "Thursday") $namahari = "Kamis";
	else if ($namahari == "Friday") $namahari = "Jumat";
	else if ($namahari == "Saturday") $namahari = "Sabtu";
	//echo $namahari;

	$tg=explode("-", $t);
	$a=$tg[1];

	switch ($a) {
		case 1:
			$bulan="Januari";
			break;
		case 2:
			$bulan="Februari";
			break;
		case 3:
			$bulan="Maret";
			break;
		case 4:
			$bulan="April";
			break;
		case 5:
			$bulan="Mei";
			break;
		case 6:
			$bulan="Juni";
			break;
		case 7:
			$bulan="Juli";
			break;
		case 8:
			$bulan="Agustus";
			break; 
		case 9:
			$bulan="September";
			break;
		case 10:
			$bulan="Oktober";
			break;
		case 11:
			$bulan="November";
			break;
		case 12:
			$bulan="Desember";
			break;
	}
	
	$tanggal2=$namahari." tanggal ".$tg[0]." ".$bulan." ".$tg[2];
	return $tanggal2;
}
function cariTanggal2($tglnya){
	$t=$tglnya;
	$tg=explode("-", $t);
	$a=$tg[1];

	switch ($a) {
		case 1:
			$bulan="Januari";
			break;
		case 2:
			$bulan="Februari";
			break;
		case 3:
			$bulan="Maret";
			break;
		case 4:
			$bulan="April";
			break;
		case 5:
			$bulan="Mei";
			break;
		case 6:
			$bulan="Juni";
			break;
		case 7:
			$bulan="Juli";
			break;
		case 8:
			$bulan="Agustus";
			break; 
		case 9:
			$bulan="September";
			break;
		case 10:
			$bulan="Oktober";
			break;
		case 11:
			$bulan="November";
			break;
		case 12:
			$bulan="Desember";
			break;
	}
	
	$tanggal2=$tg[0]." ".$bulan." ".$tg[2];
	return $tanggal2;
}

$tanggal=cariTanggal2($tgl_rapat);
$tanggal2=cariTanggal($tgl_rapat);

 
?>

<html>
	<head>
		<style>
				body{
				font-family:Tahoma, Geneva, sans-serif; 
				font-size:12pt; 
				/*margin: 3em;*/
				margin:5px;
			}
			div {
			/* background: #EDEDED;
			   border: solid 1px #CCC;
			  width: 650px;*/
			  width: 690px;
			
			}
		
			hr.style-eight { 
				 color:#c00;background-color:#000;height:3px;border:none;
				
			} 
			
						
			.break-word {
			  word-wrap: break-word;
			}
			
			ol.aneh {
			 // counter-reset: list;
			}
			ol.aneh li {
			  list-style: none;
			}
			ol.aneh li:before {
			  content: counter(list) ") ";
			  counter-increment: list;
			}

			.ul-dash {
			  margin: 0;
			}

			.ul-dash {
			  margin-left: 0em;
			  padding-left: 3.5em;
			}
			
			.cont .ul-dash {
			  list-style: none;
			}
			.cont .ul-dash > li:before {
			  content: "-";
			  text-indent: -9;
			  display: inline-block;
			  width: 0;
			  position: relative;
			  left: -1.5em;
			}
		</style>
	</head>
	<body>
		<table width="100%" cellspacing=0 cellpadding=0>
			<tr >
				<td width="66" ><img src="<?php echo base_url();?>asset/img/depdagri.png" width="100px"/></td>
				<td>
					<table width="100%" cellspacing=0 cellpadding=-1>
						<tr>
							<td style="font-size:14pt;  text-align: center;">KEMENTERIAN DALAM NEGERI</td>
						</tr>
							<tr>
							<td style="font-size:14pt;  text-align: center;">REPUBLIK INDONESIA</td>
						</tr>
						<tr>
							<td style="font-size:16pt;  text-align: center;font-weight:bold;">SEKRETARIAT JENDERAL</td>
						</tr>
						<tr>
							<td style="font-size:9pt;  text-align: center;">Jalan Medan Merdeka Utara Nomor 7 Jakarta 10110</td>
						</tr>
						<tr>
							<td style="font-size:9pt;  text-align: center;">Telepon (021) 3458542 Fax. (021) 3458542  Website : <u>www.kemendagri.go.id</u></td>
						</tr>
					</table>
				</td>
			</tr>
			
		</table>
		<hr class="style-eight"/>
		
		<table width="100%"  style="margin-top:25px">
			<tr>
				<td colspan=3 style=" text-align: center;"><b><u>NOTA DINAS</u></b></td>
			</tr>
			<tr>
				<td colspan=3 style=" text-align: center;">&nbsp;</td>
			</tr>
			<tr>
				<td width="50">Kepada</td>
				<td width="5">:</td>
				<td>Yth. <?php echo ucwords(strtolower($this->m_home->getPenjabat4(2)));?> Sekretaris Jenderal</td>
			</tr>
			<tr>
				<td>Dari</td>
				<td>:</td>
				<td>Kepala Biro Kepegawaian</td>
			</tr>
			<tr>
				<td>Tembusan</td>
				<td>:</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>Tanggal</td>
				<td>:</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>Nomor</td>
				<td>:</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>Sifat</td>
				<td>:</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>Lampiran</td>
				<td>:</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td valign="top">Hal</td>
				<td valign="top">:</td>
				<td valign="top">Penilaian Calon Sekretaris Daerah <?=$kota?></td>
			</tr>
		</table>
		<hr width="100%" class="style-eight" style="height:1.5px; margin-top:10px;"  />
		<span style="margin-bottom:25px">&nbsp;</span>
		<div class="break-word" style="text-align: justify;">
		<p><span style="margin-right:60px">&nbsp;</span>Dengan hormat kami laporkan kepada <?php echo ucwords(strtolower($this->m_home->getPenjabat4(2)));?> Sekretaris Jenderal, sebagai  berikut :
			<br/>
			<ol style="margin-left:-25px">
				<li style="margin-bottom:10px">Pada hari <?=$tanggal2?> sesuai surat undangan Sekretaris Jenderal Nomor <?=$no_notadinas?>  tanggal <?=$tanggal?>, telah dilakukan penilaian oleh Tim Penilai Kementerian Dalam Negeri terhadap usulan calon Sekretaris Daerah Kabupaten/Kota yaitu :<br/><br/>
				<?php 
				$jum=$this->m_home->getKabDetail($no_notadinas)->num_rows();
				$i=1;
				//$kota="";
				foreach($this->m_home->getKabDetail($no_notadinas)->result() as $s){
					if($i<$jum-1){
					echo "$i) ".$s->nm_kab_kota.";<br/>";
					}else if($i==$jum-1){
					echo "$i) ".$s->nm_kab_kota."; dan<br/>";
					}else if($i==$jum){
					echo "$i) ".$s->nm_kab_kota.".<br/><br/>";
					}
					$i++;
				}
				?>
				</li>
				<li>Sehubungan dengan hal tersebut di atas, terlampir bersama ini dihaturkan : <br/>
					<ol type="a" style="margin-left:-23px">
					  <li>Nota Dinas Ibu Sekretaris Jenderal kepada Bapak Menteri Dalam Negeri;</li>
					  <li>Berita Acara Penilaian Calon Sekretaris Daerah Kabupaten/Kota;</li>
					  <li>Net konsep Surat Menteri Dalam Negeri kepada Gubernur <?=$prov?>.</li>
					</ol>
				</li>
			</ol>
			untuk mohon perkenan tanda tangan dan paraf Ibu Sekretaris Jenderal.
			<br/>
			<br/>
			<span style="margin-right:60px">&nbsp;</span>Demikian untuk menjadi periksa.
			<br/><br/>
			<table align="right">
				<tr>
					<td style="text-align: center;"><?php echo ($this->m_home->getPenjabat3(1)); ?></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td style="text-align: center;"><b><?=$this->m_home->getPenjabat2(1)?></b></td>
				</tr>
			</table>
		</p>
		
		<!-- Page break-->
		<div style="page-break-before: always;">&nbsp;</div>
		
		<table width="100%" style="margin-top:-20px">
			<tr >
				<td style="font-size:14pt;  text-align: center;"><img src="<?php echo base_url();?>asset/img/garuda.png" width="100px"/></td>
			</tr>
			<tr>
				<td style="font-size:10pt;  text-align: center;">KEMENTERIAN DALAM NEGERI</td>
			</tr>
			<tr>
				<td style="font-size:10pt;  text-align: center;"><b>REPUBLIK INDONESIA</b></td>
			</tr>
		</table>
		
		
		<table width="100%"  style="margin-top:25px">
			<tr>
				<td colspan=3 style=" text-align: center;"><b><u>NOTA DINAS</u></b></td>
			</tr>
			<tr>
				<td colspan=3 style=" text-align: center;">&nbsp;</td>
			</tr>
			<tr>
				<td width="50">Kepada</td>
				<td width="5">:</td>
				<td>Yth. <?php echo ucwords(strtolower($this->m_home->getPenjabat4(3)));?> Menteri Dalam Negeri</td>
			</tr>
			<tr>
				<td>Dari</td>
				<td>:</td>
				<td>Sekretaris Jenderal</td>
			</tr>
			<tr>
				<td>Tembusan</td>
				<td>:</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>Tanggal</td>
				<td>:</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>Nomor</td>
				<td>:</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>Sifat</td>
				<td>:</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>Lampiran</td>
				<td>:</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td valign="top">Hal</td>
				<td valign="top">:</td>
				<td valign="top">Penilaian Calon Sekretaris Daerah <?=$kota?></td>
			</tr>
		</table>
		
		<hr width="100%" class="style-eight" style="height:1.5px; margin-top:10px;"  />
		<span style="margin-bottom:15px">&nbsp;</span>
		<p style="margin-left:1px"><span style="margin-right:60px">&nbsp;</span>Dengan hormat kami laporkan kepada Bapak Menteri Dalam Negeri, perihal tersebut di atas, sebagai berikut :
			<br/>
			<ol style="margin-left:-12px;margin-bottom:-4px;">
				<li class="cont"style="margin-bottom:10px">Bahwa berdasarkan ketentuan Pasal 122 ayat (1) dan (3) Undang-undang Nomor 32 Tahun 2004 Tentang Pemerintahan Daerah, dinyatakan bahwa : <br/>
					
					<ul style="margin-left:-40px" class="ul-dash">
						<li>Sekretaris Daerah diangkat dari pegawai negeri sipil yang memenuhi persyaratan;</li>
						<li>Sekretaris Daerah sebagaimana dimaksud untuk kabupaten/kota diangkat dan diberhentikan oleh Gubernur setelah dikonsultasikan kepada Menteri Dalam Negeri untuk mendapatkan persetujuan.</li>
					</ul>
					
				</li>
				<li style="margin-bottom:10px">Sesuai dengan Penjelasan Pasal 122 ayat (3) Undang-undang Nomor 32 Tahun 2004 Tentang Pemerintahan Daerah, bahwa dalam pengisian Sekretaris Daerah Kabupaten/Kota, Bupati/Walikota mengajukan 3 (tiga) orang calon yang memenuhi persyaratan kepada Gubernur. Atas dasar usul dari Bupati/Walikota tersebut, Gubernur melakukan penilaian terhadap 3 (tiga) orang calon Sekretaris Daerah melalui paparan di depan tim penilai tingkat provinsi, tentang rencana strategis seandainya calon dimaksud sebagai Sekretaris Daerah.<br/>
				Selanjutnya dari hasil penilaian tim penilai tingkat provinsi, oleh Gubernur disampaikan kepada Menteri Dalam Negeri untuk dimintakan penilaian persyaratan administrasi dan wawasan kebangsaan yang dimiliki oleh masing-masing calon, untuk dipilih salah satu calon yang paling memenuhi persyaratan.<br/>
				Untuk dapat menetapkan salah satu calon yang paling memenuhi persyaratan maka hasil nilai dari tim penilai provinsi digabung dengan nilai persyaratan administrasi dan wawasan kebangsaan dari tim penilai Kementerian Dalam Negeri. Dari gabungan nilai tersebut maka dapat ditetapkan salah satu calon yang paling memenuhi syarat untuk disetujui Menteri Dalam Negeri dan selanjutnya disampaikan kepada Gubernur untuk ditetapkan sebagai Sekretaris Daerah.
				</li>
				<li style="margin-bottom:10px">Sehubungan dengan hal tersebut agar penilaian Calon Sekretaris Daerah Kabupaten/Kota dapat memenuhi standar kompetensi Calon Sekretaris Daerah maka dikeluarkan  Peraturan Menteri Dalam Negeri Nomor 5 tahun 2005 Tentang Pedoman Penilaian Calon Sekretaris Daerah Provinsi dan Kabupaten/Kota serta Pejabat Struktural Eselon II di lingkungan Kabupaten/Kota, yang merupakan pedoman bagi seluruh Pimpinan Daerah dalam mengusulkan Calon Sekretaris Daerah.  </li>
				<li style="margin-bottom:10px">Mengacu kepada ketentuan tersebut, Gubernur <?=ucwords(strtolower($prov))?> menyampaikan permohonan persetujuan kepada Bapak Menteri Dalam Negeri, sebagaimana surat terlampir.</li>
				<div style="page-break-before: always;">&nbsp;</div>
				<?php $jumlah=0; foreach($this->m_home->getAllDapil2($no_notadinas)->result() as $p){
				
						foreach($this->m_home->getSuratGub($no_notadinas,$p->id_kab_kota)->result() as $g){
							$nosuratgub=$g->no_surat_gub;
							$tglsuratgub=cariTanggal2($g->tgl_surat_gub);
						}
				?>
				
				<li style="margin-bottom:10px">
				Dari Gubernur <?=ucwords(strtolower($p->nm_provinsi))?> dengan Surat Nomor <?=$nosuratgub?> tanggal <?=$tglsuratgub?> menyampaikan Penilaian Calon Sekda <?=$p->nm_kab_kota?> kepada Menteri Dalam Negeri, dengan Calon Sekretaris Daerah, sebagai berikut :
					<ol style="margin-left:-18px" >
					<?php $inc=1; $jm=$this->m_home->getDetailListSekda($no_notadinas,$p->id_kab_kota)->num_rows();
					foreach($this->m_home->getDetailListSekda($no_notadinas,$p->id_kab_kota)->result() as $c){ 
						if($inc <$jm){
					?>	
						<li value="<?=$inc?>" > Sdr.<b> <?=$c->nama?></b>, NIP.<?=$c->nip?>, <?=$c->pangkat?>, <?=$c->jabatan?>;</li>
					<? }else{?>
						<li value="<?=$inc?>" style="margin-bottom:10px"> Sdr.<b> <?=$c->nama?></b>, NIP.<?=$c->nip?>, <?=$c->pangkat?>, <?=$c->jabatan?>.</li>
					<?
					}
					$inc++;
					} ?>
					</ol>
				</li>
				
				<?php 
				}?>
				
				<li style="margin-bottom:10px">Sehubungan dengan hal tersebut di atas, terlampir bersama ini dihaturkan :<br/>
					<ol type="a" style="margin-left:-19px">
						<li>Berita Acara Penilaian Calon Sekretaris Daerah Kabupaten/Kota;</li>
						<li>Net/konsep surat Menteri Dalam Negeri kepada Gubernur <?=ucwords(strtolower($prov))?> </li>
					</ol>
				</li>
			</ol>
			untuk mohon perkenan tanda tangan Bapak Menteri Dalam Negeri.<br/>
			<span style="margin-right:60px">&nbsp;</span>Demikian untuk menjadi periksa dan arahan lanjut.
			<br/><br/><br/>
			<table align="right">
				<tr>
					<td style="text-align: center;"><?php echo strtoupper($this->m_home->getPenjabat3(2)); ?></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td style="text-align: center;"><b><?=$this->m_home->getPenjabat2(2)?></b></td>
				</tr>
			</table>
		</p>
		</div>
	</body>
</html>