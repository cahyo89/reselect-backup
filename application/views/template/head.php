<!DOCTYPE html>
<!--[if IE 8]>    <html class="no-js ie8 ie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9 ie" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<title>Dasbord | Reselect Apps Depdagri</title>

		
		<!-- jQuery TagsInput Styles -->
		<link rel='stylesheet' type='text/css' href='<?php echo base_url();?>asset/css/plugins/jquery.tagsinput.css'>
		
		<!-- jQuery jWYSIWYG Styles -->
		<link rel='stylesheet' type='text/css' href='<?php echo base_url();?>asset/css/plugins/jquery.jwysiwyg.css'>
		
		<!-- Bootstrap wysihtml5 Styles -->
		<link rel='stylesheet' type='text/css' href='<?php echo base_url();?>asset/css/plugins/bootstrap-wysihtml5.css'>
		
		
		<!-- jQuery Snippet Styles -->
		<link rel='stylesheet' type='text/css' href='<?php echo base_url();?>asset/css/plugins/jquery.snippet.css'>
		
		<!-- jQuery jGrowl Styles -->
		<link rel='stylesheet' type='text/css' href='<?php echo base_url();?>asset/css/plugins/jquery.jgrowl.css'>
		
		<!-- CSS styles -->
		<link rel='stylesheet' type='text/css' href='<?php echo base_url();?>asset/css/huraga-red.css'>
		
		<!-- Fav and touch icons -->
		<link rel="shortcut icon" href="<?php echo base_url();?>asset/img/icons/favicon.ico">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>asset/img/icons/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>asset/img/icons/apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>asset/img/icons/apple-touch-icon-57-precomposed.png">
		
		<!-- JS Libs -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="<?php echo base_url();?>asset/js/libs/jquery.js"><\/script>')</script>
		<script src="<?php echo base_url();?>asset/js/libs/modernizr.js"></script>
		<script src="<?php echo base_url();?>asset/js/libs/selectivizr.js"></script>
		
		<script>
			$(document).ready(function(){
				
				// Tooltips
				$('[title]').tooltip({
					placement: 'top'
				});
				
				// Dropdowns
				$('.dropdown-toggle').dropdown();
				
				// Tabs
				$('.demoTabs a').click(function (e) {
					e.preventDefault();
					$(this).tab('show');
				})
				
			});
		</script>
	</head>
	<body>
		
		<!-- Main page header -->
		<header class="container">
		
			<!-- Main page logo -->
			<h1><a href="#" class="brand">RESELECT</a></h1>
			
			<!-- Main page headline -->
			<p>Regional Secretary Election Apps | Kementrian Dalam Negeri RI</p>
			
		
		</header>
		<!-- /Main page header -->
		
		<!-- Main page container -->
		<section class="container" role="main">
		
			<!-- Left (navigation) side -->
			<div class="navigation-block">
			
				<!-- User profile -->
				<section class="user-profile">
					<figure>
						<img alt="<?php echo $_SESSION['username'];?> avatar" src="<?php echo base_url();?>asset/img/anon.png">
						<figcaption>
							<strong><a href="#" class=""><?php echo $_SESSION['username'];?></a></strong>
							<!--em>marketing manager</em-->
							<ul>
								<li><a class="btn btn-primary btn-flat" href="<?php echo base_url();?>index.php/chome/settingUser" title="Account Settings">settings</a></li>
								<li><a class="btn btn-primary btn-flat" href="<?php echo base_url();?>index.php/chome/logout" title="Logout Account">logout</a></li>
							</ul>
						</figcaption>
					</figure>
				</section>
				<!-- /User profile -->
				
				<!-- Main navigation -->
				<nav class="main-navigation" role="navigation">
					<ul>
						<li><a href="<?php echo base_url();?>index.php/chome/home" class="no-submenu"><span class="awe-home"></span>Dashboard</a></li>
						<li>
							<a href=""><span class="awe-copy"></span>Pembuatan Nota Dinas</a>
							<ul>
								<li><a href="<?php echo base_url();?>index.php/chome/undangansekda">Undangan Pemilu SEKDA</a></li>								
							</ul>
						</li>
						<li><a href=""><span class="awe-file"></span>List Nota Dinas</a>
							<ul>
								<li><a href="<?php echo base_url();?>index.php/chome/listundangan">List Undangan SEKDA</a></li>
								<li><a href="<?php echo base_url();?>index.php/chome/listbap">List Berita Acara Penilaian calon</a></li>
								<li><a href="<?php echo base_url();?>index.php/chome/listhasilpenilaian">List Dokumen Hasil Penilaian</a></li>
								<li><a href="<?php echo base_url();?>index.php/chome/listsuratmendagri">List Surat Mendagri</a></li>
							</ul>
						</li>
						<li><a href=""><span class="awe-file"></span>Pemilihan</a>
							<ul>
								<li><a href="<?php echo base_url();?>index.php/chome/listdapil">List Daerah Pemilihan</a></li>
								<li><a href="<?php echo base_url();?>index.php/chome/listpenilaian">List Penilaian</a></li>
							</ul>
						</li>
						<li><a href="<?php echo base_url();?>index.php/chome/listpenjabat" class="no-submenu"><span class="awe-user-md"></span>List Penjabat</a></li>
						<li><a href=""><span class="awe-flag"></span>Kelola Daerah Pemilihan</a>
							<ul>
								<li><a href="<?php echo base_url();?>index.php/chome/listProv">Kelola Provinsi</a></li>
								<li><a href="<?php echo base_url();?>index.php/chome/listKabKota">Kelola Kab atau Kota</a></li>
							</ul>
						</li>
						<li><a href="<?php echo base_url();?>index.php/chome/log" class="no-submenu"><span class="awe-book"></span>Log Histori</a></li>
					</ul>
				</nav>
				<!-- /Main navigation -->
				
				<!-- Sample side note -->
				<section class="side-note">
					<div class="side-note-container">
						<h2>PERHATIAN!</h2><br/>
						<p>Silahkan membaca petunjuk penggunaan aplikasi sebelum menggunakannya.</p>
					</div>
					<div class="side-note-bottom"></div>
				</section>
				<!-- /Sample side note -->
				
			</div>
			<!-- Left (navigation) side -->
			
			<!-- Right (content) side -->
			<div class="content-block" role="main">
			
				<!-- Page header>
				<article class="page-header">
					<h1>Form elements</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam blandit, dolor mollis adipiscing elementum, ipsum turpis euismod tellus, vitae mollis velit leo id nisi. Morbi non lectus turpis, eu interdum orci. In at rutrum nisi. Donec sit amet urna purus, at eleifend ipsum. Sed magna enim, tempor eu tincidunt vitae, dictum tristique arcu.</p>
				</article  -->
				<!-- /Page header -->
				
				<!-- Grid row -->
				<div class="row">
				
					<!-- Data block -->
					<article class="span12 data-block">
						<div class="data-container">