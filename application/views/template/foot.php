</div>
					</article>
					<!-- /Data block -->
					
				</div>
				<!-- /Grid row -->
				
				
				
			</div>
			<!-- /Right (content) side -->
			
		</section>
		<!-- /Main page container -->
		
		<!-- Main page footer>
		<footer class="container">
			<p>Built with love by <a href="https://www.facebook.com/cayo.ts">Cahyo Al-Hazim</a>.</p>
			<ul>
				<li><a href="#" class="">Support</a></li>
				<li><a href="#" class="">Documentation</a></li>
				<li><a href="#" class="">API</a></li>
			</ul>
			<a href="#top" class="btn btn-primary btn-flat pull-right">Top &uarr;</a>
		</footer -->
		<!-- /Main page footer -->
		
		<!-- Scripts -->
		
		<script src="<?php echo base_url();?>asset/js/navigation.js"></script>
		<script src="<?php echo base_url();?>asset/js/bootstrap/bootstrap-affix.js"></script>
		<script src="<?php echo base_url();?>asset/js/bootstrap/bootstrap-alert.js"></script>
		<script src="<?php echo base_url();?>asset/js/bootstrap/bootstrap-tooltip.js"></script>
		<script src="<?php echo base_url();?>asset/js/bootstrap/bootstrap-dropdown.js"></script>
		<script src="<?php echo base_url();?>asset/js/bootstrap/bootstrap-tab.js"></script>
		<script src="<?php echo base_url();?>asset/js/bootstrap/bootstrap-collapse.js"></script>		
		<script src="<?php echo base_url();?>asset/js/bootstrap/bootstrap-button.js"></script>
		<script src="<?php echo base_url();?>asset/js/bootstrap/bootstrap-alert.js"></script>
		<script src="<?php echo base_url();?>asset/js/bootstrap/bootstrap-popover.js"></script>
		<script src="<?php echo base_url();?>asset/js/bootstrap/bootstrap-modal.js"></script>
		<script src="<?php echo base_url();?>asset/js/bootstrap/bootstrap-transition.js"></script>
		
		<script src="<?php echo base_url();?>asset/js/bootstrap/bootstrap-fileupload.js"></script>
		<script src="<?php echo base_url();?>asset/js/bootstrap/bootstrap-inputmask.js"></script>

	
		<script src="<?php echo base_url();?>asset/js/plugins/datepicker/bootstrap-datepicker.js"></script>
		
		<script>
			$(document).ready(function() {
				
				var dp1 = $('.datepicker').datepicker({format: 'dd-mm-yyyy'}).on('changeDate', function(ev) {
dp1.hide();
}).data('datepicker');
				var dp2 = $('.datepicker2').datepicker({format: 'dd-mm-yyyy'}).on('changeDate', function(ev) {
dp2.hide();
}).data('datepicker');
				
			});
			
		</script>
		
	</body>
</html>
