<!-- Grid row -->
<form id="f2" name="f2"action="<?php echo site_url("chome/inputpenilaian/".$this->uri->segment(3)."/".$this->uri->segment(4));?>" method="POST">
	<header >
	<h2 style="margin-left:20px; margin-top:5px;">Penilaian calon ( <?=$nama?> )</h2>
	</header>
				<div class="row">
					<article class="span4 data-block" style="margin-left:20px; ">
						<div class="data-container">
							<header>
								<h2>Persyaratan Administrasi</h2>
							</header>
							<section>
								<table class="table table-striped table-hover">
									<tr>
										<td><b>Kepangkatan </b></td>
										<td><select style="width:45px" id="pangkat" name="pangkat" onchange="getData()">
											<option value=0>0</option>
											<option value=1>1</option>
											<option value=2>2</option>
											<option value=3>3</option>
											<option value=4>4</option>
										</select></td>
									</tr>
									<tr>
										<td><b>Diklat Pim</b></td>
										<td><select style="width:45px" id="pim" name="pim" onchange="getData()">
											<option value=0>0</option>
											<option value=1>1</option>
											<option value=2>2</option>
											<option value=3>3</option>
											<option value=4>4</option>
										</select></td>
									</tr>
									<tr>
										<td><b>Pendidikan</b></td>
										<td><select style="width:45px" id="pendidikan" name="pendidikan" onchange="getData()">
											<option value=0>0</option>
											<option value=1>1</option>
											<option value=2>2</option>
											<option value=3>3</option>
											<option value=4>4</option>
										</select></td>
									</tr>
									<tr>
										<td><b>Riwayat Jabatan</b></td>
										<td><select style="width:45px" id="jabatan" name="jabatan" onchange="getData()">
											<option value=0>0</option>
											<option value=1>1</option>
											<option value=2>2</option>
											<option value=3>3</option>
											<option value=4>4</option>
										</select></td>
									</tr>
									<tr>
										<td><b>Diklat Teknis</b></td>
										<td><select style="width:45px" id="teknis" name="teknis" onchange="getData()">
											<option value=0>0</option>
											<option value=1>1</option>
											<option value=2>2</option>
											<option value=3>3</option>
											<option value=4>4</option>
										</select></td>
									</tr>
									<tr>
										<td><b>Diklat Fungsional</b></td>
										<td><select style="width:45px" id="fungsional" name="fungsional" onchange="getData()">
											<option value=0>0</option>
											<option value=1>1</option>
											<option value=2>2</option>
											<option value=3>3</option>
											<option value=4>4</option>
										</select></td>
									</tr>
									<tr>
										<td><b>D U K</b></td>
										<td><select style="width:45px" id="duk" name="duk" onchange="getData()" >
											<option value=0>0</option>
											<option value=1>1</option>
											<option value=2>2</option>
											<option value=3>3</option>
											<option value=4>4</option>
										</select></td>
									</tr>
									<tr>
										<td><b>DP3</b></td>
										<td><select style="width:45px" id="dp3" name="dp3" onchange="getData()">
											<option value=0>0</option>
											<option value=1>1</option>
											<option value=2>2</option>
											<option value=3>3</option>
											<option value=4>4</option>
										</select></td>
									</tr>
									<tr>
										<td><b>Disiplin</b></td>
										<td><select style="width:45px" id="disiplin" name="disiplin" onchange="getData()">
											<option value=0>0</option>
											<option value=1>1</option>
											<option value=2>2</option>
											<option value=3>3</option>
											<option value=4>4</option>
										</select></td>
									</tr>
								</table>
							</section>
						</div>
					</article>
					
					<article class="span3 data-block">
						<div class="data-container">
							<header>
								<h2>Wawasan Kebangsaan</h2>
							</header>
							<section>
								<table class="table table-striped table-hover">
									<tr>
										<td><b>Perjalanan Karir </b></td>
										<td><select style="width:45px" id="karir" name="karir" onchange="getData()">
											<option value=0>0</option>
											<option value=1>1</option>
											<option value=2>2</option>
											<option value=3>3</option>
											<option value=4>4</option>
										</select></td>
									</tr>
									<tr>
										<td><b>Keragaman Diklat</b></td>
										<td><select style="width:45px" id="diklat" name="diklat" onchange="getData()">
											<option value=0>0</option>
											<option value=1>1</option>
											<option value=2>2</option>
											<option value=3>3</option>
											<option value=4>4</option>
										</select></td>
									</tr>
									<tr>
										<td><b>Seminar</b></td>
										<td><select style="width:45px" id="seminar" name="seminar" onchange="getData()">
											<option value=0>0</option>
											<option value=1>1</option>
											<option value=2>2</option>
											<option value=3>3</option>
											<option value=4>4</option>
										</select></td>
									</tr>
									<tr>
										<td><b>Pokir Strategis</b></td>
										<td><select style="width:45px" id="pokir" name="pokir" onchange="getData()">
											<option value=0>0</option>
											<option value=1>1</option>
											<option value=2>2</option>
											<option value=3>3</option>
											<option value=4>4</option>
										</select></td>
									</tr>
								</table>
							</section>
						</div>
					</article>
					
					<article class="span4 data-block">
						<div class="data-container">
							<header>
								<h2>Rencana Strategis</h2>
							</header>
							<section>
								<table class="table table-striped table-hover">
									<tr>
										<td><b>Kemampuan menyikapi kondisi teritorial & NKRI</b></td>
										<td><select style="width:45px" id="nkri" name="nkri" onchange="getData()">
											<option value=0>0</option>
											<option value=1>1</option>
											<option value=2>2</option>
											<option value=3>3</option>
											<option value=4>4</option>
										</select></td>
									</tr>
									<tr>
										<td><b>Pandangan terhadap Otda</b></td>
										<td><select style="width:45px" id="otda" name="otda" onchange="getData()">
											<option value=0>0</option>
											<option value=1>1</option>
											<option value=2>2</option>
											<option value=3>3</option>
											<option value=4>4</option>
										</select></td>
									</tr>
									<tr>
										<td><b>Visi & misi Calon Sekda</b></td>
										<td><select style="width:45px" id="visimisi" name="visimisi" onchange="getData()">
											<option value=0>0</option>
											<option value=1>1</option>
											<option value=2>2</option>
											<option value=3>3</option>
											<option value=4>4</option>
										</select></td>
									</tr>
									<tr>
										<td><b>Koordinasi & Komunikasi</b></td>
										<td><select style="width:45px" id="komunikasi" name="komunikasi" onchange="getData()">
											<option value=0>0</option>
											<option value=1>1</option>
											<option value=2>2</option>
											<option value=3>3</option>
											<option value=4>4</option>
										</select></td>
									</tr>
								</table>
							</section>
						</div>
					</article>
					
					<article class="span7 data-block">
						<div class="data-container">
							<header>
								<h2>Skor Sementara</h2>
							</header>
							<section>
								<table class="table table-striped table-hover">
									<tr>
										<td><b><h1>Total Score</h1></b></td>
										<td><b><h1 id="totalSekor" style="color:#E74949;"> 0 </h1></b>
											<input id="totalSekor2" class="input-xlarge" id="totalSekor2" name="totalSekor2" type="hidden">
										</td>
									</tr>
									<tr>
										
										<td colspan=2><input type="submit" class="btn btn-info" value="Save Data"/></td>
									</tr>
								
								</table>
							</section>
						</div>
					</article>
				</div>
			</form>
			<script>
				function getData()
				{
					var a=parseInt(document.getElementById('pangkat').value);
					var b=parseInt(document.getElementById('pim').value);
					var c=parseInt(document.getElementById('pendidikan').value);
					var d=parseInt(document.getElementById('jabatan').value);
					var e=parseInt(document.getElementById('teknis').value);
					var f=parseInt(document.getElementById('fungsional').value);
					var g=parseInt(document.getElementById('duk').value);
					var h=parseInt(document.getElementById('dp3').value);
					var i=parseInt(document.getElementById('disiplin').value);
					var j=parseInt(document.getElementById('karir').value);
					var k=parseInt(document.getElementById('diklat').value);
					var l=parseInt(document.getElementById('pokir').value);
					var m=parseInt(document.getElementById('nkri').value);
					var n=parseInt(document.getElementById('otda').value);
					var o=parseInt(document.getElementById('visimisi').value);
					var p=parseInt(document.getElementById('komunikasi').value);
					var q=parseInt(document.getElementById('seminar').value);
					 var total_sekor=a+b+c+d+e+f+g+h+i+j+k+l+m+n+o+p+q;
					document.getElementById('totalSekor').innerHTML = total_sekor;
					document.f2.totalSekor2.value=total_sekor; 
				}
			</script>