<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"><head>
<title></title>

<style type="text/css">

div {
  background: #EDEDED;
  border: solid 1px #CCC;
  padding: 10px;
  width: 600px;
  margin: 10px;
}

.break-word {
  word-wrap: break-word;
}
</style>
</head>
<body>

<h3>break-word</h3>
<div class="break-word">
	<ol>
		<li style="text-align: justify;" >Dalam rangka penilaian persetujuan pengangkatan calon Sekretaris Daerah Kab. Teluk Wondama, Kab. Tambrauw, Kota Sungai Penuh dan Kab. Solok Selatan akan diselenggarakan rapat penilaian calon Sekretaris Daerah Kabupaten/Kota pada hari Senin tanggal 12 Maret 2012 Pukul 10.00 WIB bertempat di Ruang Rapat Kepala Biro Kepegawaian. </li>
		<li style="text-align: justify;"		>Sehubungan dengan hal tersebut, terlampir disampaikan Net Surat Undangan penilaian calon Sekretaris Daerah Kabupaten/Kota, untuk mohon perkenan tanda tangan Ibu Sekretaris Jenderal.</li>
	</ol>

</div>

<h3>normal</h3>
<div class="normal">
  <p>I'm a veeeeeeeerryyyyyy loooooooonggggggg teeeeexxxtttt</p>
  <p>http://www.w3.org/TR/2011/WD-css3-text-20110412/</p>
</div>




<h1>Font Selection</h1>

<p>Available font-family:</p>
<ul>
<li>serif (default) (Aliases: times, times-roman)</li>
<li>sans-serif (Aliases: helvetica)</li>
<li>monospace (Aliases: fixed, courier)</li>
</ul>
<p>Available font-style:</p>
<ul>
<li>normal (default)</li>
<li>italic</li>
</ul>
<p>Available  font-weight:</p>
<ul>
<li>normal (default)</li>
<li>bold</li>
</ul>
<p>Other variations are falling back to a combination of the above</p>
<p>Special fonts</p>
<ul>
<li>symbol</li>
<li>zapfdingbats</li>
</ul>

<h2>Font selection</h2>

<p style="font-family:sans-serif;">abcdefghijk ABCDEFGHIJK - (Helvetica) - (sans-serif) - sans-serif</p>
<p style="font-family:helvetica;">abcdefghijk ABCDEFGHIJK - (Helvetica) - (sans-serif) - helvetica</p>
<p style="font-family:serif;">abcdefghijk ABCDEFGHIJK - (Times-Roman) - (serif) - serif</p>
<p style="font-family:times;">abcdefghijk ABCDEFGHIJK - (Times-Roman) - (serif) - times</p>
<p style="font-family:times-roman;">abcdefghijk ABCDEFGHIJK - (Times-Roman) - (serif) - times-roman</p>
<p style="font-family:monospace;">abcdefghijk ABCDEFGHIJK - (Courier)- (monospace) - mononospace</p>
<p style="font-family:fixed;">abcdefghijk ABCDEFGHIJK - (Courier)- (monospace) - fixed</p>
<p style="font-family:courier;">abcdefghijk ABCDEFGHIJK - (Courier)- (monospace) - courier</p>

<h2>Font search path</h2>

<p style="font-family:dummy1,dummy2;">abcdefghijk ABCDEFGHIJK - serif - "font-family:dummy1,dummy2;"</p>
<p style="font-family:dummy1,dummy2,sans-serif;">abcdefghijk ABCDEFGHIJK - sans-serif - "font-family:dummy1,dummy2,sans-serif;"</p>
<p style="font-family:sans-serif,dummy1,dummy2;">abcdefghijk ABCDEFGHIJK - sans-serif - "font-family:sans-serif,dummy1,dummy2;"</p>
<p style="font-family:sans-serif,courier;">abcdefghijk ABCDEFGHIJK - sans-serif - "font-family:sans-serif,courier;"</p>

<h2>Font variations</h2>

<p style="font-style:normal; font-weight:normal;">abcdefghijk ABCDEFGHIJK - serif - normal - "font-style:normal; font-weight:normal;"</p>
<p style="font-style:normal; font-weight:lighter;">abcdefghijk ABCDEFGHIJK - serif - normal - "font-style:normal; font-weight:lighter;"</p>
<p style="font-style:normal; font-weight:100;">abcdefghijk ABCDEFGHIJK - serif - normal - "font-style:normal; font-weight:100;"</p>
<p style="font-style:normal; font-weight:200;">abcdefghijk ABCDEFGHIJK - serif - normal - "font-style:normal; font-weight:200;"</p>
<p style="font-style:normal; font-weight:300;">abcdefghijk ABCDEFGHIJK - serif - normal - "font-style:normal; font-weight:300;"</p>
<p style="font-style:normal; font-weight:400;">abcdefghijk ABCDEFGHIJK - serif - normal - "font-style:normal; font-weight:400;"</p>
<p style="font-style:normal; font-weight:500;">abcdefghijk ABCDEFGHIJK - serif - normal - "font-style:normal; font-weight:500;"</p>
<p style="font-style:normal; font-weight:600;">abcdefghijk ABCDEFGHIJK - serif - bold - "font-style:normal; font-weight:600;"</p>
<p style="font-style:normal; font-weight:700;">abcdefghijk ABCDEFGHIJK - serif - bold - "font-style:normal; font-weight:700;"</p>
<p style="font-style:normal; font-weight:800;">abcdefghijk ABCDEFGHIJK - serif - bold - "font-style:normal; font-weight:800;"</p>
<p style="font-style:normal; font-weight:900;">abcdefghijk ABCDEFGHIJK - serif - bold - "font-style:normal; font-weight:900;"</p>
<p style="font-style:normal; font-weight:bold;">abcdefghijk ABCDEFGHIJK - serif - bold - "font-style:normal; font-weight:bold;"</p>
<p style="font-style:normal; font-weight:bolder;">abcdefghijk ABCDEFGHIJK - serif - bold - "font-style:normal; font-weight:bolder;"</p>
<p style="font-style:italic; font-weight:normal;">abcdefghijk ABCDEFGHIJK - serif - italic - "font-style:italic; font-weight:normal;"</p>
<p style="font-style:oblique; font-weight:normal;">abcdefghijk ABCDEFGHIJK - serif - italic - "font-style:oblique; font-weight:normal;"</p>
<p style="font-style:italic; font-weight:bold;">abcdefghijk ABCDEFGHIJK - serif - bold_italic - "font-style:italic; font-weight:bold;"</p>
<p style="font-variant:small-caps; font-style:normal; font-weight:normal;">abcdefghijk ABCDEFGHIJK - serif - normal - "font-variant:small-caps; font-style:normal; font-weight:normal;"</p>

<h2>Font size</h2>
<p style="font-size:xx-small">abcdefghijk ABCDEFGHIJK - xx-small</p>
<p style="font-size:x-small">abcdefghijk ABCDEFGHIJK - x-small</p>
<p style="font-size:small">abcdefghijk ABCDEFGHIJK - small</p>
<p style="font-size:medium">abcdefghijk ABCDEFGHIJK - medium</p>
<p style="font-size:large">abcdefghijk ABCDEFGHIJK - large</p>
<p style="font-size:x-large">abcdefghijk ABCDEFGHIJK - x-large</p>
<p style="font-size:xx-large">abcdefghijk ABCDEFGHIJK - xx-large</p>
<p style="font-size:10pt">abcdefghijk ABCDEFGHIJK - 10pt</p>
<p style="font-size:12pt">abcdefghijk ABCDEFGHIJK - 12pt</p>
<p style="font-size:14pt">abcdefghijk ABCDEFGHIJK - l4pt</p>
<p style="font-size:smaller">abcdefghijk ABCDEFGHIJK - smaller</p>
<p style="font-size:larger">abcdefghijk ABCDEFGHIJK - larger</p>

<h2>Line height</h2>
<p style="line-height:100%">abcdefghijk ABCDEFGHIJK<br>abcdefghijk ABCDEFGHIJK 100%</p>
<p style="line-height:120%">abcdefghijk ABCDEFGHIJK<br>abcdefghijk ABCDEFGHIJK 120%</p>
<p style="line-height:140%">abcdefghijk ABCDEFGHIJK<br>abcdefghijk ABCDEFGHIJK 140%</p>
<p style="font-size:xx-large;line-height:100%">abcdefghijk ABCDEFGHIJK<br>abcdefghijk ABCDEFGHIJK 100%</p>
<p style="font-size:xx-large;line-height:120%">abcdefghijk ABCDEFGHIJK<br>abcdefghijk ABCDEFGHIJK 120%</p>
<p style="font-size:xx-large;line-height:140%">abcdefghijk ABCDEFGHIJK<br>abcdefghijk ABCDEFGHIJK 140%</p>

<h2>Font combined setting</h2>
<p style="font:italic small-caps bold 14pt/160% sans-serif;">style="font:italic small-caps bold 14pt/160% sans-serif;"<br>(all attributes)</p>
<p style="font:normal 10pt/160% sans-serif;">style="font:normal 10pt/160% sans-serif;"<br>(partial attributes)</p>
<p style="font:700 10pt/160% sans-serif;">style="font:700 10pt/160% sans-serif;"<br>(partial attributes)</p>
<p style="font:small sans-serif;">style="font:small sans-serif;"<br>(partial attributes)</p>
<div style="font:italic small-caps bold 14pt/160% sans-serif;">

<p>inherit style="font:italic small-caps bold 14pt/160% sans-serif;" :</p>

<p style="font:small sans-serif;">
style="font:small sans-serif;"<br>
(partial attributes - reset inherited)<br>
<span style="font-weight:bold;">style="font-weight:bold;"<br>
(partial overwrite)</span><br>
(resume partial attributes)
</p>

<p>continue inherited</p>

</div>
</body>
</html>
