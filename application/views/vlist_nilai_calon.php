<script src="<?php echo base_url();?>asset/js/jquery18.min"></script>
	<header>
	<h2>Input Penilaian Sekertaris Daerah</h2>
	</header>
		<section class="tab-content">
							
		<!-- Tab #basic -->
			<div class="tab-pane active" id="basic" style="min-height:430px">
				
				<?php if($this->session->flashdata('error')){?>
				<div class="alert alert-error">
					<button class="close" data-dismiss="alert">&times;</button >
					<strong>Error!</strong>
					<?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } else if($this->session->flashdata('success')){?>
				<div class="alert alert-success">
					<button class="close" data-dismiss="alert">&times;</button >
					<strong>Success!</strong>
					<?php echo $this->session->flashdata('success'); ?>
				</div>	
				<?php } ?>
				<table class="table table-striped table-hover" id="example">
					<thead>
						<tr>
							<th>No</th>
							<th>NIP</th>
							<th>Nama Calon</th>
							<th>Total Score</th>
							<th>Aksi</th>
							
						</tr>
					</thead>
					<tbody>
						<?php $p=0;$i=1; $s;
						foreach($datanya as $d){
							$skor=$this->m_home->cekSkor($d->nip);
							$flag=$this->m_home->getFlagNilai($d->nip,$nosurat,$iddaerah);
							//$jmlCalon=$this->m_home->cekJmlCalon2($d->no_notadinas,$d->id_kab_kota)->num_rows();
							$opo=$this->uri->segment(3);
						?>
						<tr class="gradeA">
							<td><?=$i?></td>
							<td><?=$d->nip;?></td>
							<td><?=$d->nama;?></td>
							<td><?=$skor;?></td>
							<td><?php 
						
							if($skor < 1){?><a href="<?php echo base_url();?>index.php/chome/inputpenilaian/<?=$opo?>/<?=$d->nip?>"><span class="btn"><i class="awe-edit"></i> Input Penilaian</span></a><?php }elseif($flag==1) {?><span class="label label-info">Complete</span><?}else{?><a href="<?php echo base_url();?>index.php/chome/editpenilaian/<?=$opo?>/<?=$d->nip?>" title="Edit Penilaian"><span class="btn btn-info"><i class="awe-edit"></i> Edit</span></a> <a href="<?php echo base_url();?>index.php/chome/deletepenilaian/<?=$opo?>/<?=$d->nip?>" title="Hapus Penilaian" onclick='return window.confirm("Anda yakin menghapus data ini ?");' ><span class="btn btn-danger"><i class="awe-edit"></i> Hapus</span></a><?} ?></td>
						</tr>
						<?php 	$s[$p]=$skor; $p++;$i++;}?>
					</tbody>
				</table>
				<? if(($s[0]>=1) && ($s[1]>=1) && ($s[2]>=1)){?>
				<header>
					
						<a href="<?php echo base_url();?>index.php/chome/reviewpenilaian/<?=$opo?>"><span class="btn btn-inverse"><i class="awe-list-alt"></i> Review Penilaian </span>
						
					
					
						<ul class="data-header-actions">
							<li> <a href="<?php echo base_url();?>index.php/chome/finishpenilaian/<?=$opo?>"><span class="btn btn-success"><i class="awe-list-alt"></i> Finish Penilaian </span> </li>
						</ul>
				</header>
				<?};?>
			</div>
		
		</section>
	