<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pdf {
    function pdf_create($html, $filename, $stream=TRUE)
    {
		require_once("dompdf/dompdf_config.inc.php");
		spl_autoload_register('DOMPDF_autoload');

		$dompdf = new DOMPDF();
		$dompdf->load_html($html);
		$dompdf->set_paper('legal', 'portrait');
		
		$dompdf->render();
		if($filename=="ND_BAP"){
		$canvas = $dompdf->get_canvas();
		$font = Font_Metrics::get_font("helvetica", "normal");

		// the same call as in my previous example
		
		$canvas->page_text(270, 790, "- {PAGE_NUM} -",
						   $font, 10, array(0,0,0));
						   
		}
		if ($stream) {
			$dompdf->stream($filename.".pdf");
		} else {
			$CI =& get_instance();
			$CI->load->helper('file');
			write_file($filename, $dompdf->output());
		}
	}
}